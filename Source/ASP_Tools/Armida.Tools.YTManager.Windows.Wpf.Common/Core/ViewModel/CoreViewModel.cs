﻿using System.ComponentModel;

namespace Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel
{
    public class CoreViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
