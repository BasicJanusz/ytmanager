﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.View;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.ViewModel;

namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service
{
    public class DialogService : IDialogService
    {
        public DialogResultInfo ShowDecisionDialog(string dialogHeader, string message)
        {
            var dialogWindow = new DialogWindow(DialogType.Decision, dialogHeader, message, string.Empty);
            dialogWindow.ShowDialog();

            var viewModel = dialogWindow.DataContext as DialogViewModel;
            if (viewModel == null)
            {
                return DialogResultInfo.Empty;
            }

            return viewModel.GetDialogResult();
        }

        public DialogResultInfo ShowDecisionDialog(string dialogHeader, string message, params AdditionalOption[] additionalOptions)
        {
            var dialogWindow = new DialogWindow(DialogType.Decision, dialogHeader, message, string.Empty, additionalOptions);
            dialogWindow.ShowDialog();

            var viewModel = dialogWindow.DataContext as DialogViewModel;
            if (viewModel == null)
            {
                return DialogResultInfo.Empty;
            }

            return viewModel.GetDialogResult();
        }

        public DialogResultInfo ShowDialog(string dialogHeader, string message)
        {
            var dialogWindow = new DialogWindow(DialogType.Information, dialogHeader, message, string.Empty);
            dialogWindow.ShowDialog();

            var viewModel = dialogWindow.DataContext as DialogViewModel;
            if(viewModel == null)
            {
                return DialogResultInfo.Empty;
            }

            return viewModel.GetDialogResult();
        }

        public DialogResultInfo ShowDialog(string dialogHeader, string message, params AdditionalOption[] additionalOptions)
        {
            var dialogWindow = new DialogWindow(DialogType.Information, dialogHeader, message, string.Empty, additionalOptions);
            dialogWindow.ShowDialog();

            var viewModel = dialogWindow.DataContext as DialogViewModel;
            if (viewModel == null)
            {
                return DialogResultInfo.Empty;
            }

            return viewModel.GetDialogResult();
        }

        public DialogResultInfo ShowDirectoryDialog(string dialogHeader, string message, string startDirectory)
        {
            var dialogWindow = new DialogWindow(DialogType.Directory, dialogHeader, message, startDirectory);
            dialogWindow.ShowDialog();

            var viewModel = dialogWindow.DataContext as DialogViewModel;
            if (viewModel == null)
            {
                return DialogResultInfo.Empty;
            }

            return viewModel.GetDialogResult();
        }

        public DialogResultInfo ShowFileDialog(string dialogHeader, string message, string startDirectory)
        {
            throw new NotImplementedException();
        }
    }
}
