﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model;

namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service
{
    public interface IDialogService
    {
        public DialogResultInfo ShowDialog(string dialogHeader, string message);
        public DialogResultInfo ShowDialog(string dialogHeader, string message, params AdditionalOption[] additionalOptions);
        public DialogResultInfo ShowDecisionDialog(string dialogHeader, string message);
        public DialogResultInfo ShowDecisionDialog(string dialogHeader, string message, params AdditionalOption[] additionalOptions);
        public DialogResultInfo ShowDirectoryDialog(string dialogHeader, string message, string startDirectory);
        public DialogResultInfo ShowFileDialog(string dialogHeader, string message, string startDirectory);
    }
}
