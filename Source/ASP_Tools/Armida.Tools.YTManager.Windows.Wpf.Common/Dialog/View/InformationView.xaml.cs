﻿using System.Windows.Controls;

namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.View
{
    /// <summary>
    /// Interaction logic for InformationView.xaml
    /// </summary>
    public partial class InformationView : UserControl
    {
        public InformationView()
        {
            InitializeComponent();
        }
    }
}
