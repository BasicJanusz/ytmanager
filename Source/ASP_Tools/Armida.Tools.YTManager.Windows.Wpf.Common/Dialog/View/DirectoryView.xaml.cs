﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.ViewModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.View
{
    /// <summary>
    /// Interaction logic for DirectoryView.xaml
    /// </summary>
    public partial class DirectoryView : UserControl
    {
        public DirectoryView()
        {
            InitializeComponent();
        }

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(DataContext is DialogViewModel viewModel &&
              e.OriginalSource is FrameworkElement element &&
              element.DataContext is string directory &&
              Directory.Exists(directory))
            {
                viewModel.CurrentDirectory = directory;
            }
        }
    }
}
