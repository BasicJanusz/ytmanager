﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.ViewModel;
using System.Windows;

namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.View
{
    /// <summary>
    /// Interaction logic for DialogWindow.xaml
    /// </summary>
    public partial class DialogWindow : Window
    {
        private readonly DialogViewModel _viewModel;

        public DialogWindow(DialogType dialogType, string header, string message, string startDirectory, params AdditionalOption[] additionalOptions)
        {
            _viewModel = new DialogViewModel(dialogType, header, message, startDirectory, additionalOptions);
            _viewModel.CloseWindow += Close;

            DataContext = _viewModel;

            InitializeComponent();
        }
    }
}
