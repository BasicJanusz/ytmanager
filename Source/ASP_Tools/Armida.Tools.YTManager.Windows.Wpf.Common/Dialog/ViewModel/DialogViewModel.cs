﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model;
using CommunityToolkit.Mvvm.Input;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.ViewModel
{
    public class DialogViewModel : CoreViewModel
    {
        public event Action CloseWindow;

        private ObservableCollection<string> _currentDirectories;
        private ObservableCollection<string> _drives;
        private ObservableCollection<AdditionalOption> _additionalOptions;
        private ObservableCollection<AdditionalOption> _selectedAdditionalOptions;
        private string _currentDirectory;
        private string _currentDrive;

        public ICommand OkCommand { get; }
        public ICommand CloseCommand { get; }
        public ICommand YesCommand { get; }
        public ICommand NoCommand { get; }
        public ICommand CreateDirectoryCommand { get; }
        public ICommand SelectDirectoryCommand { get; }
        public ICommand SelectFileCommand { get; }

        public ObservableCollection<AdditionalOption> AdditionalOptions
        {
            get => _additionalOptions;
            set
            {
                _additionalOptions = value;
                RaisePropertyChanged(nameof(AdditionalOptions));
            }
        }
        public ObservableCollection<AdditionalOption> SelectedAdditionalOptions
        {
            get => _selectedAdditionalOptions;
            set
            {
                _selectedAdditionalOptions = value;
                RaisePropertyChanged(nameof(SelectedAdditionalOptions));
            }
        }

        public ObservableCollection<string> CurrentDirectories
        {
            get => _currentDirectories;
            set
            {
                _currentDirectories = value;
                RaisePropertyChanged(nameof(CurrentDirectories));
            }
        }

        public ObservableCollection<string> Drives
        {
            get => _drives;
            set
            {
                _drives = value;
                RaisePropertyChanged(nameof(Drives));
            }
        }

        public string DialogHeader { get; }
        public string DialogMessage { get; }

        public string CurrentDirectory
        {
            get => _currentDirectory;
            set
            {
                if (value == "..")
                {
                    var newDirectory = Directory.GetParent(_currentDirectory);
                    value = newDirectory.FullName;
                }

                _currentDirectory = value;
                RaisePropertyChanged(nameof(CurrentDirectory));
                RaisePropertyChanged(nameof(DirectoryExists));
                ReloadDirectories();
            }
        }

        public string CurrentDrive
        {
            get => _currentDrive;
            set
            {
                _currentDrive = value;
                RaisePropertyChanged(nameof(CurrentDrive));
                CurrentDirectory = _currentDrive;
            }
        }

        public DialogResult DialogResult { get; set; }
        public DialogType DialogType { get; }
        public bool IsInformationDialog => DialogType == DialogType.Information;
        public bool IsDecisionDialog => DialogType == DialogType.Decision;
        public bool IsDirectoryDialog => DialogType == DialogType.Directory;
        public bool IsFileDialog => DialogType == DialogType.File;
        public bool DirectoryExists => Directory.Exists(CurrentDirectory);

        public DialogViewModel(DialogType dialogType, string header, string message, string startDirectory, params AdditionalOption[] additionalOptions)
        {
            OkCommand = new RelayCommand(Ok);
            CloseCommand = new RelayCommand(Close);
            YesCommand = new RelayCommand(Yes);
            NoCommand = new RelayCommand(No);
            CreateDirectoryCommand = new RelayCommand(CreateDirectory);
            SelectDirectoryCommand = new RelayCommand(SelectDirectory);
            SelectFileCommand = new RelayCommand(SelectFile);

            DialogType = dialogType;
            DialogHeader = header;
            DialogMessage = message;
            CurrentDirectory = startDirectory;

            DialogResult = DialogResult.None;
            CurrentDirectories = new ObservableCollection<string>();
            Drives = new ObservableCollection<string>();
            AdditionalOptions = ProcessAdditionalOptions(additionalOptions);
            SelectedAdditionalOptions = new ObservableCollection<AdditionalOption>();

            if (DialogType != DialogType.Directory)
                return;

            LoadDrives();
            ReloadDirectories();
            CurrentDirectory = startDirectory;
        }

        private ObservableCollection<AdditionalOption> ProcessAdditionalOptions(AdditionalOption[] additionalOptions)
        {
            var newOptions = new ObservableCollection<AdditionalOption>();

            if (additionalOptions == null ||
                additionalOptions.Length == 0)
            {
                return newOptions;
            }

            foreach(var option in additionalOptions)
            {
                var newOption = option;
                newOption.OptionValueChanged += OnAdditionalOptionChanged;
                newOptions.Add(newOption);
            }

            return newOptions;
        }

        private void OnAdditionalOptionChanged(AdditionalOption changedOption)
        {
            if(!changedOption.Checked && SelectedAdditionalOptions.Contains(changedOption))
            {
                SelectedAdditionalOptions.Remove(changedOption);
            }

            if (changedOption.Checked && !SelectedAdditionalOptions.Contains(changedOption))
            {
                SelectedAdditionalOptions.Add(changedOption);
            }
        }

        public DialogResultInfo GetDialogResult() =>
            new()
            {
                DialogResult = DialogResult,
                Data = DialogResult switch
                {
                    DialogResult.OK => string.Empty,
                    DialogResult.Close => string.Empty,
                    DialogResult.Yes => string.Empty,
                    DialogResult.No => string.Empty,
                    DialogResult.SelectedDirectory => CurrentDirectory,
                    DialogResult.SelectedFile => string.Empty,
                    _ => string.Empty,
                },
                AdditionalOptions = SelectedAdditionalOptions,
            };

        private void LoadDrives()
        {
            var drives = DriveInfo.GetDrives();

            foreach (var drive in drives)
            {
                if (drive.DriveType == DriveType.Fixed ||
                   drive.DriveType == DriveType.Removable)
                {
                    Drives.Add(drive.Name);
                }
            }

            CurrentDrive = Path.GetPathRoot(CurrentDirectory);
        }

        private void ReloadDirectories()
        {
            if (CurrentDirectories == null)
                return;

            CurrentDirectories.Clear();

            var newDirectory = Directory.GetParent(_currentDirectory);
            if (newDirectory != null && newDirectory.Exists)
            {
                CurrentDirectories.Add("..");
            }

            if (Directory.Exists(CurrentDirectory))
            {
                var innerDirectories = Directory.GetDirectories(CurrentDirectory);
                foreach (var directory in innerDirectories)
                {
                    CurrentDirectories.Add(directory);
                }
            }


            RaisePropertyChanged(nameof(CurrentDirectories));
        }

        private void Ok()
        {
            DialogResult = DialogResult.OK;
            CloseWindow?.Invoke();
        }

        private void Close()
        {
            DialogResult = DialogResult.Close;
            CloseWindow?.Invoke();
        }

        private void Yes()
        {
            DialogResult = DialogResult.Yes;
            CloseWindow?.Invoke();
        }

        private void No()
        {
            DialogResult = DialogResult.No;
            CloseWindow?.Invoke();
        }

        private void CreateDirectory()
        {
            Directory.CreateDirectory(CurrentDirectory);

            RaisePropertyChanged(nameof(CurrentDirectory));
            RaisePropertyChanged(nameof(DirectoryExists));
            ReloadDirectories();
        }

        private void SelectDirectory()
        {
            DialogResult = DialogResult.SelectedDirectory;
            CloseWindow?.Invoke();
        }

        private void SelectFile()
        {
            DialogResult = DialogResult.SelectedFile;
            CloseWindow?.Invoke();
        }
    }
}
