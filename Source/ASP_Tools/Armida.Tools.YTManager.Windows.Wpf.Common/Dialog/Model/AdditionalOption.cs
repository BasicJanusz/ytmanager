﻿namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model
{
    public class AdditionalOption
    {
        private string _name;
        private bool _checked;

        public event Action<AdditionalOption> OptionValueChanged;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OptionValueChanged?.Invoke(this);
            }
        }

        public bool Checked
        {
            get => _checked;
            set
            {
                _checked = value;
                OptionValueChanged?.Invoke(this);
            }
        }
    }
}
