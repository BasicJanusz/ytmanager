﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum;
using System.Collections.ObjectModel;

namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model
{
    public class DialogResultInfo
    {
        public DialogResult DialogResult { get; set; }
        public string Data { get; set; }
        public ObservableCollection<AdditionalOption> AdditionalOptions { get; set; }

        public DialogResultInfo()
        {
        }

        private static DialogResultInfo _empty;
        public static DialogResultInfo Empty => _empty ??= 
            new DialogResultInfo()
            {
                DialogResult = DialogResult.None,
                Data = string.Empty,
            };
    }
}
