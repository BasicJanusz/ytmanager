﻿namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum
{
    public enum DialogType
    {
        Information,
        Decision,
        Directory,
        File,
    }
}
