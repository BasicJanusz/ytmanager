﻿namespace Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum
{
    public enum DialogResult
    {
        // Information
        Close,
        OK,

        // Decision
        Yes,
        No,

        // Directory
        SelectedDirectory,

        // File
        SelectedFile,

        // Default
        None,
    }
}
