﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Workspace;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Utils;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using YoutubeExplode;
using YoutubeExplode.Common;
using YoutubeExplode.Playlists;
using YoutubeExplode.Videos.Streams;

namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Service
{
    public class YoutubeService : IYoutubeService, IDisposable
    {
        public event Action<string> FileConverted;

        private readonly Dispatcher _dispatcher;
        private object _processLock = new object();
        private object _queueLock = new object();
        private bool _mainLoop;
        private Queue<(ProcessType process, YTVideo video)> _processQueue;
        private Thread _thread;

        private readonly IWorkspaceManager _workspaceManager;
        private readonly IConversionService _conversionService;
        private readonly IApplicationSettings _applicationSettings;
        private readonly IOptionsProvider _optionsProvider;
        private readonly IConversionContextProvider _conversionContextProvider;
        private readonly IFFmpegQueryFactory _ffmpegQueryFactory;
        private readonly YoutubeClient _youtube;

        public RemoveOptionType RemoveOption => _applicationSettings.SelectedRemoveOption;

        public YoutubeService(IWorkspaceManager workspaceManager, IConversionService conversionService, Dispatcher dispatcher, IApplicationSettings applicationSettings, IOptionsProvider optionsProvider, IConversionContextProvider conversionContextProvider, IFFmpegQueryFactory ffmpegQueryFactory)
        {
            _dispatcher = dispatcher;
            _processQueue = new Queue<(ProcessType process, YTVideo video)>();
            _thread = new Thread(new ThreadStart(ProcessLoop));

            _workspaceManager = workspaceManager;
            _conversionService = conversionService;
            _applicationSettings = applicationSettings;
            _optionsProvider = optionsProvider;
            _conversionContextProvider = conversionContextProvider;
            _ffmpegQueryFactory = ffmpegQueryFactory;

            var cookies = _applicationSettings.ExtractCookies();
            _youtube = new YoutubeClient(cookies);
        }

        public void StartProcessThread()
        {
            if (_thread == null)
                return;

            _mainLoop = true;
            _thread?.Start();
        }

        public void EnqueueProcess(ProcessType process, YTVideo video)
        {
            lock (_queueLock)
            {
                Debug.WriteLine($"EnqueueProcess : {process}");
                _processQueue.Enqueue((process, video));
            }
        }

        public async Task<List<YTVideo>> GetVideosFromPLaylistAsync(YTPlaylist playlist)
        {
            var id = PlaylistId.Parse(playlist.Url);
            var youtubeVideos = await _youtube.Playlists.GetVideosAsync(id);

            var videos = new List<YTVideo>();
            ConversionProfileOption conversionProfile = ConversionProfileOption.Empty;
            foreach(var profile in _optionsProvider.ConversionProfileOptions)
            {
                if(_applicationSettings.SelectedConversionProfile == profile.Guid)
                {
                    conversionProfile = profile;
                }    
            }
            
            foreach (var youtubeVideo in youtubeVideos)
            {
                var video = new YTVideo()
                {
                    Url = youtubeVideo.Url,
                    Id = youtubeVideo.Id.ToString(),
                    State = VideoState.Created,
                    AutoLoad = _applicationSettings.AutoLoad,
                    AutoDownload = _applicationSettings.AutoDownload,
                    AutoConvert = _applicationSettings.AutoConvert,
                    Format = _applicationSettings.DownloadFormat,
                    DownloadMode = _applicationSettings.DownloadMode,
                    Quality = _applicationSettings.Quality,
                    ConversionProfile = conversionProfile,
                };
                videos.Add(video);
            }

            return videos;
        }

        public List<YTVideo> GetVideosFromRadio(YTRadio radio)
        {
            var cookies = _applicationSettings.ExtractCookies();
            var body = HttpGet(radio.Url, cookies);
            byte[] bytes = Encoding.Default.GetBytes(body);
            var encodedBody = Encoding.UTF8.GetString(bytes);

            var splitBody = encodedBody.Split("/watch?v=");
            var videoIds = new List<string>();
            var videos = new List<YTVideo>();

            for (int i = 1; i < splitBody.Length; i++)
            {
                var videoId = splitBody[i].Substring(0, 11);

                if (!videoIds.Contains(videoId))
                {
                    videoIds.Add(videoId);
                }
            }

            ConversionProfileOption conversionProfile = ConversionProfileOption.Empty;
            foreach (var profile in _optionsProvider.ConversionProfileOptions)
            {
                if (_applicationSettings.SelectedConversionProfile == profile.Guid)
                {
                    conversionProfile = profile;
                }
            }

            foreach (var id in videoIds)
            {
                var video = new YTVideo()
                {
                    Url = YTVideo.CreateUrlFromId(id),
                    Id = id,
                    State = VideoState.Created,
                    AutoLoad = _applicationSettings.AutoLoad,
                    AutoDownload = _applicationSettings.AutoDownload,
                    AutoConvert = _applicationSettings.AutoConvert,
                    Format = _applicationSettings.DownloadFormat,
                    DownloadMode = _applicationSettings.DownloadMode,
                    Quality = _applicationSettings.Quality,
                    ConversionProfile = conversionProfile,
                };

                videos.Add(video);
            }

            return videos;
        }

        private string HttpGet(string url, List<Cookie> cookies)
        {
            var responseAsString = string.Empty;

            using (HttpClient client = new HttpClient())
            {
                var cookieContainer = new CookieContainer();
                var handler = new HttpClientHandler() { CookieContainer = cookieContainer };

                foreach(var cookie in cookies)
                {
                    cookieContainer.Add(cookie);
                }

                HttpResponseMessage response = client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                {
                    responseAsString = response.Content.ReadAsStringAsync().Result;
                }
            }

            return responseAsString;
        }

        private async void ProcessLoop()
        {
            YTVideo video = new YTVideo();
            ProcessType process = ProcessType.Empty;
            bool dequeued = false;

            while (_mainLoop)
            {
                lock (_queueLock)
                {
                    if (_processQueue.Count > 0)
                    {
                        var currentProcess = _processQueue.Dequeue();
                        process = currentProcess.process;
                        video = currentProcess.video;
                        dequeued = true;
                    }
                }

                if (dequeued)
                {
                    lock (_processLock)
                    {
                        Debug.WriteLine($"DeququqProcess : {process}");
                        switch (process)
                        {
                            case ProcessType.Load:
                                _ = LoadVideoAsync(video);
                                break;

                            case ProcessType.Download:
                                switch (video.DownloadMode)
                                {
                                    case DownloadMode.MuxedHighestVideoQuality:
                                        _ = DownloadMuxedVideoAsync(video, true);
                                        break;

                                    case DownloadMode.MuxedHighestBitrateQuality:
                                        _ = DownloadMuxedVideoAsync(video, false);
                                        break;

                                    case DownloadMode.HighestPossibleQuality:
                                        _ = DownloadHighestQualityVideoAsync(video);
                                        break;

                                    default:
                                        _ = DownloadMuxedVideoAsync(video, true);
                                        break;
                                }
                                break;

                            case ProcessType.Convert:
                                ConvertVideoAsync(video);
                                break;

                            case ProcessType.Remove:
                                RemoveVideoAsync(video);
                                break;

                            default:
                                break;
                        }
                    }

                    dequeued = false;
                    continue;
                }

                await Task.Delay(500);
            }
        }

        private async Task<int> LoadVideoAsync(YTVideo video)
        {
            var youtubeVideo = await _youtube.Videos.GetAsync(video.Url);

            video.Title = youtubeVideo.Title;
            video.Id = youtubeVideo.Id;
            video.Quality = _applicationSettings.Quality;
            video.State = VideoState.Loaded;

            return 0;
        }

        private async Task<YTVideo> DownloadMuxedVideoAsync(YTVideo video, bool highestVideoQuality)
        {
            var streamManifest = await _youtube.Videos.Streams.GetManifestAsync(video.Url);
            var streamInfo = highestVideoQuality
                ? streamManifest.GetMuxedStreams().GetWithHighestVideoQuality()
                : streamManifest.GetMuxedStreams().GetWithHighestBitrate();

            var filePath = Path.Combine(_workspaceManager.DownloadPath, video.FullName);
            await _youtube.Videos.Streams.DownloadAsync(streamInfo, filePath);

            video.DownloadedFilePath = filePath;
            video.State = VideoState.Downloaded;

            return video;
        }

        private async Task<YTVideo> DownloadHighestQualityVideoAsync(YTVideo video)
        {
            var streamManifest = await _youtube.Videos.Streams.GetManifestAsync(video.Url);
            
            // Directory for video and audio
            var guid = Guid.NewGuid();
            var innerDirectoryPath = Path.Combine(_workspaceManager.DownloadPath, guid.ToString());
            Directory.CreateDirectory(innerDirectoryPath);

            // Video
            var videoOnlyStreamInfo = streamManifest.GetVideoOnlyStreams().GetWithHighestVideoQuality();
            var videoFilePath = Path.Combine(innerDirectoryPath, $"videoOnly.{video.GetFormatFileExtension()}");
            await _youtube.Videos.Streams.DownloadAsync(videoOnlyStreamInfo, videoFilePath);
            video.VideoOnlyPath = videoFilePath;

            // Audio
            var audioOnlyStreamInfo = streamManifest.GetAudioOnlyStreams().GetWithHighestBitrate();
            var audioFilePath = Path.Combine(innerDirectoryPath, $"videoOnly.mp3");
            await _youtube.Videos.Streams.DownloadAsync(audioOnlyStreamInfo, audioFilePath);
            video.AudioOnlyPath = audioFilePath;

            // Video & Audio
            var finalFilePath = Path.Combine(_workspaceManager.DownloadPath, video.FullName);
            MergeVideoWithAudio(videoFilePath, audioFilePath, finalFilePath);
            Directory.Delete(innerDirectoryPath, true);

            video.DownloadedFilePath = finalFilePath;
            video.State = VideoState.Downloaded;

            return video;
        }

        private void MergeVideoWithAudio(string videoOnlyPath, string audioOnlyPath, string outputPath)
        {
            string arguments = $"-i \"{videoOnlyPath}\" -i \"{audioOnlyPath}\" -c:v copy -c:a aac -strict experimental \"{outputPath}\"";
            var ffmpegPath = Path.Combine(_workspaceManager.ApplicationPath, "Ffmpeg\\ffmpeg.exe");

            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = ffmpegPath,
                Arguments = arguments,
                UseShellExecute = true,
                CreateNoWindow = true,
            };

            using (Process process = Process.Start(startInfo))
            {
                process.WaitForExit();
            }
        }

        private void ConvertVideoAsync(YTVideo video)
        {
            ConversionProfile conversionProfile = _conversionContextProvider.GetConversionProfileByOption(video.ConversionProfile);

            _ffmpegQueryFactory.SetMainApplicationPath(_workspaceManager.ApplicationPath);
            _ffmpegQueryFactory.SetConversionDirectory(_workspaceManager.ConvertPath);
            _ffmpegQueryFactory.SetVideo(video);
            _ffmpegQueryFactory.SetConversionProfile(conversionProfile);

            var query = _ffmpegQueryFactory.Build();

            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = query.FFmpegPath,
                Arguments = query.Query,
                UseShellExecute = true,
                CreateNoWindow = true,
            };

            using (Process process = Process.Start(startInfo))
            {
                process.WaitForExit();
            }

            video.ConvertedFilePath = query.ConvertedFilePath;
            video.State = VideoState.Converted;

            _dispatcher.Invoke(() =>
            {
                FileConverted?.Invoke(query.ConvertedFilePath);
            });
        }

        private void RemoveVideoAsync(YTVideo video)
        {
            var shouldRemoveDownloadedFile = 
                RemoveOption == RemoveOptionType.DownloadedFile || 
                RemoveOption == RemoveOptionType.All;

            var shouldRemoveConvertedFile =
                RemoveOption == RemoveOptionType.ConvertedFile ||
                RemoveOption == RemoveOptionType.All;

            if (shouldRemoveDownloadedFile && 
                File.Exists(video.DownloadedFilePath))
                File.Delete(video.DownloadedFilePath);

            if (shouldRemoveConvertedFile && 
                File.Exists(video.ConvertedFilePath))
                File.Delete(video.ConvertedFilePath);
        }

        public void Dispose()
        {
            _mainLoop = false;
        }
    }

    public interface IYoutubeService
    {
        public event Action<string> FileConverted;
        public Task<List<YTVideo>> GetVideosFromPLaylistAsync(YTPlaylist playlist);
        public List<YTVideo> GetVideosFromRadio(YTRadio radio);

        public void StartProcessThread();
        public void EnqueueProcess(ProcessType process, YTVideo video);
    }
}
