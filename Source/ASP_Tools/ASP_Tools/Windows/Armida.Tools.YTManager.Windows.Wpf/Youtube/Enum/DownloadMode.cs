﻿namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum
{
    public enum DownloadMode
    {
        MuxedHighestVideoQuality,
        MuxedHighestBitrateQuality,
        HighestPossibleQuality,
    }
}
