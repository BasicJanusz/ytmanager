﻿namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum
{
    public enum DownloadFormat
    {
        WebMediaFile,
        MPEG4,
        ThirdGenerationPartnershipProject,
    }
}
