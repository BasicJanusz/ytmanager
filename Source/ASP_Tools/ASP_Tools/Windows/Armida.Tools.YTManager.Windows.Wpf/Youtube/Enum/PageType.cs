﻿namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum
{
    public enum PageType
    {
        Basic = 0,
        Video = 1,
        Playlist = 2,
        Radio = 3,
    }
}
