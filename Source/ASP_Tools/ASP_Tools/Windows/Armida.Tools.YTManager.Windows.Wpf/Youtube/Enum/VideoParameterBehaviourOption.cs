﻿namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum
{
    public enum VideoParameterBehaviour
    {
        ChangeForAdded,
        ChangeForSelectedAndAdded,
        ChangeForAll,
    }
}
