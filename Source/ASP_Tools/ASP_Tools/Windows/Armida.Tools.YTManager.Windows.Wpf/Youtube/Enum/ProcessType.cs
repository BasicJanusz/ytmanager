﻿namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum
{
    public enum ProcessType
    {
        Load,
        Download,
        Convert,
        Remove,
        Empty,
    }
}
