﻿namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum
{
    public enum VideoState
    {
        Created,
        Loaded,
        Downloaded,
        Converted,
        Empty,
    }
}
