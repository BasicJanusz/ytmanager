﻿namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Model
{
    public class YTPlaylist
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
