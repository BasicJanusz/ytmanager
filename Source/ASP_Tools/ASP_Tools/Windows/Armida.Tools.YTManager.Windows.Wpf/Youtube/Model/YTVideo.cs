﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using System;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;

namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Model
{
    public class YTVideo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        public event Action StateChanged;

        private ConversionProfileOption _conversionProfile;
        private string _id;
        private string _title;
        private string _url;
        private string _fullName;
        private VideoState _state;
        private Quality _quality;
        private DownloadFormat _format;
        private DownloadMode _downloadMode;
        private bool _autoLoad;
        private bool _autoDownload;
        private bool _autoConvert;

        public VideoState State
        {
            get => _state;
            set
            {
                _state = value;
                StateChanged?.Invoke();
            }
        }

        public string Id
        {
            get => _id;
            set
            {
                _id = value;
                RaisePropertyChanged(nameof(Id));
            }
        }
        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                RaisePropertyChanged(nameof(Title));
                UpdateFullName();
            }
        }

        public string FullName 
        { 
            get => _fullName; 
            set
            {
                if(Title == null)
                {
                    return;
                }

                _fullName = value;
                RaisePropertyChanged(nameof(FullName));
            }
        }

        public string Url
        {
            get => _url;
            set
            {
                _url = value;
                RaisePropertyChanged(nameof(Url));
            }
        }

        public string DownloadedFilePath { get; set; }
        public string ConvertedFilePath { get; set; }
        public string VideoOnlyPath { get; set; }
        public string AudioOnlyPath { get; set; }

        public bool AutoLoad
        {
            get => _autoLoad;
            set
            {
                _autoLoad = value;
                RaisePropertyChanged(nameof(AutoLoad));
            }
        }
        public bool AutoDownload
        {
            get => _autoDownload;
            set
            {
                _autoDownload = value;
                RaisePropertyChanged(nameof(AutoDownload));
            }
        }
        public bool AutoConvert
        {
            get => _autoConvert;
            set
            {
                _autoConvert = value;
                RaisePropertyChanged(nameof(AutoConvert));
            }
        }
        public ConversionProfileOption ConversionProfile
        {
            get => _conversionProfile;
            set
            {
                _conversionProfile = value;
                RaisePropertyChanged(nameof(ConversionProfile));
            }
        }
        public Quality Quality
        {
            get => _quality;
            set
            {
                _quality = value;
                RaisePropertyChanged(nameof(Quality));
            }
        }

        public DownloadFormat Format
        {
            get => _format;
            set
            {
                _format = value;
                RaisePropertyChanged(nameof(Format));
                UpdateFullName();
            }
        }

        public DownloadMode DownloadMode
        {
            get => _downloadMode;
            set
            {
                _downloadMode = value;
                RaisePropertyChanged(nameof(DownloadMode));
            }
        }

        public YTVideo()
        {
            State = VideoState.Empty;
            _format = DownloadFormat.MPEG4;

            Id = string.Empty; 
            Title = string.Empty; 
            FullName = string.Empty; 
            Url = string.Empty; 
            DownloadedFilePath = string.Empty; 
            ConvertedFilePath = string.Empty;
            VideoOnlyPath = string.Empty;
            AudioOnlyPath = string.Empty;
            ConversionProfile = null;
            Quality = Quality.High;
            Format = DownloadFormat.MPEG4;
            DownloadMode = DownloadMode.MuxedHighestVideoQuality;
        }

        public void UpdateFullName()
        {
            FullName = MakeValidFileName($"{Title}.{GetFormatFileExtension()}");
        }

        private void RaisePropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return Regex.Replace(name, invalidRegStr, "_");
        }

        public static string CreateUrlFromId(string id) => $"https://www.youtube.com/watch?v={id}";

        public string GetFormatFileExtension() =>
            Format switch
            {
                DownloadFormat.WebMediaFile => "webm",
                DownloadFormat.MPEG4 => "mp4",
                DownloadFormat.ThirdGenerationPartnershipProject => "3gp",
                _ => "mp4"
            };
    }
}
