﻿using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;

namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Model
{
    public class YTPage
    {
        public string Url { get; }
        public string Id { get; }
        public PageType PageType { get; }

        private YTPage(string url, string id, PageType pageType)
        {
            Url = url;
            Id = id;
            PageType = pageType;
        }

        public static YTPage Parse(string url)
        {
            if (string.IsNullOrWhiteSpace(url) || (!url.Contains("https://www.youtube.com/") && !url.Contains("https://youtu.be/")))
            {
                return null;
            }

            YTPage page = null;

            if (url.Contains("&start_radio=1") && url.Contains("&list="))
            {
                int idFrom = url.IndexOf("&list=") + 6;
                int idTo = url.IndexOf("&start_radio=1");

                var id = url.Substring(idFrom, url.Length - idTo - 1);

                page = new YTPage(url, id, PageType.Radio);
                return page;
            }

            if (url.Contains("watch?v="))
            {
                var extendedId = url.Split("watch?v=")[1];
                var id = extendedId.Split("&t=")[0];

                page = new YTPage(url, id, PageType.Video);
            }

            if (url.Contains("playlist?list="))
            {
                var id = url.Split("playlist?list=")[1];

                page = new YTPage(url, id, PageType.Playlist);
            }

            if(url.Contains("youtu.be/"))
            {
                var id = url.Split("youtu.be/")[1];

                page = new YTPage(url, id, PageType.Video);
            }

            if (page == null)
                page = new YTPage(url, null, PageType.Basic);

            return page;
        }

        public static bool IsYtPage(string url) => Parse(url) != null;
    }
}
