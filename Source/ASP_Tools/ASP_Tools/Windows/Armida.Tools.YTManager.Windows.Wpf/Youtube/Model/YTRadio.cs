﻿namespace Armida.Tools.YTManager.Windows.Wpf.Youtube.Model
{
    public class YTRadio
    {
        public string Url { get; set; }
        public string Id { get; set; }
    }
}
