﻿using System.Collections.Generic;

namespace Armida.Tools.YTManager.Windows.Wpf.Mocks
{
    public static class Links
    {
        public static List<string> All => new List<string>()
            {
                "https://4programmers.net/Forum/C_i_.NET/293392-wyjatek_ten_typ_collectionview_nie_obsluguje_zmian_wlasnej_kolekcji_sourcecollection_z_watku_innego_niz_dispatcher",
                "https://stackoverflow.com/questions/9464001/drag-and-drop-file-into-textbox",
                "https://www.youtube.com/watch?v=QnlLJepox2M",
                "https://www.youtube.com/watch?v=mMszpUL2rG8&t=120s",
                "https://www.youtube.com/watch?v=abuvv5BEplg",
                "https://www.youtube.com/playlist?list=PLUNCoaYEmT8iUVOU-Fmnuvf-3zLxm7G8S",
            };

}
}
