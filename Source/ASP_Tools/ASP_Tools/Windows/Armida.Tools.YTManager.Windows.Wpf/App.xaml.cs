﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Workspace;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.View;
using System.Windows;

namespace Armida.Tools.YTManager.Windows.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        IWorkspaceManager workspaceManager;
        IApplicationSettings _settings;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            SetupWorkspace();
            SetupLogging();
            SetupLocalization();
            SetupDependencyInjection();

            OpenMainWondow();
        }

        private void OpenMainWondow()
        {
            var mainWindow = new MainWindow(workspaceManager, _settings, new DialogService());
            mainWindow.Show();
        }

        private void SetupWorkspace()
        {
            // TODO : Resolve workspace manager via DI
            workspaceManager = new WorkspaceManager();

            // Read settings from Json
            _settings = new ApplicationSettings(workspaceManager);
            _settings.ReadFromJson();

            workspaceManager.SetupWorkspace();
        }

        private void SetupLogging()
        {
            // TODO :
            // Implement Loggging (Serilog)
        }

        private void SetupLocalization()
        {
            // TODO :
            // Implement Text Localization
        }

        private void SetupDependencyInjection()
        {
            //TODO :
            // Implement DI (Castle Core)
        }
    }
}
