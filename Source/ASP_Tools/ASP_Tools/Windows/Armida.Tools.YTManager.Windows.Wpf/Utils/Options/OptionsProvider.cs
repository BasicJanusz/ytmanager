﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using System.Collections.Generic;
using System.Windows;

namespace Armida.Tools.YTManager.Windows.Wpf.Utils.Options
{
    public interface IOptionsProvider
    {
        public List<RemoveOption> RemoveOptions { get; }
        public List<WindowStateOption> WindowStateOptions { get; }
        public List<DownloadFormatOption> DownloadFormatOptions { get; }
        public List<DownloadModeOption> DownloadModeOptions { get; }
        public List<PlayNonExistantVideoOption> PlayNonExistantVideoOptions { get; }
        public List<VideoParameterBehaviourOption> VideoParameterBehaviourOptions { get; }
        public List<ConversionProfileOption> ConversionProfileOptions { get; }

        public void LoadOptionsFromSettings();
        public void LoadCenversionProfileOptions();
    }

    public class OptionsProvider : IOptionsProvider
    {
        private readonly IApplicationSettings _applicationSettings;

        public List<RemoveOption> RemoveOptions { get; }
        public List<WindowStateOption> WindowStateOptions { get; }
        public List<DownloadFormatOption> DownloadFormatOptions { get; }
        public List<DownloadModeOption> DownloadModeOptions { get; }
        public List<PlayNonExistantVideoOption> PlayNonExistantVideoOptions { get; }
        public List<VideoParameterBehaviourOption> VideoParameterBehaviourOptions { get; }
        public List<ConversionProfileOption> ConversionProfileOptions { get; private set; }

        public OptionsProvider(IApplicationSettings applicationSettings)
        {
            _applicationSettings = applicationSettings;
            
            RemoveOptions = new()
            {
                new() { Type = RemoveOptionType.Nothing, Description = "Don't delete anything", }, // Description = GetOptionDescription(OptionConsts.RemoveOptions_Nothing);
                new() { Type = RemoveOptionType.DownloadedFile, Description = "Delete the downloaded file", },
                new() { Type = RemoveOptionType.ConvertedFile, Description = "Delete the converted file", },
                new() { Type = RemoveOptionType.All, Description = "Delete both files", },
            };

            WindowStateOptions = new()
            {
                new() { State = WindowState.Normal, Description = "Normal (floating, non-minimized or maximized", },
                new() { State = WindowState.Minimized, Description = "Minimized", },
                new() { State = WindowState.Maximized, Description = "Maximized (with window header)", },
            };

            DownloadFormatOptions = new()
            {
                new() { Format = DownloadFormat.WebMediaFile, Description = "Web Media File (Webm)", },
                new() { Format = DownloadFormat.MPEG4, Description = "MPEG4 (Mp4)", },
                new() { Format = DownloadFormat.ThirdGenerationPartnershipProject,  Description = "Third Generation Partnership Project (3gp)", },
            };

            DownloadModeOptions = new()
            {
                new() { Mode = DownloadMode.MuxedHighestBitrateQuality, Description = "Fast Download (mid video, low audio quality)", },
                new() { Mode = DownloadMode.MuxedHighestVideoQuality, Description = "Fast Download (low video, mid audio quality)", },
                new() { Mode = DownloadMode.HighestPossibleQuality, Description = "Highest Possible Quality", },
            };

            PlayNonExistantVideoOptions = new()
            {
                new() { Behaviour = PlayNonExistantVideoBehaviour.Ask, Description = "Ask what to do", },
                new() { Behaviour = PlayNonExistantVideoBehaviour.Clear, Description = "Clear media file reference and revert to previous state", },
                new() { Behaviour = PlayNonExistantVideoBehaviour.DoNothing, Description = "Do nothing", },
            };

            VideoParameterBehaviourOptions = new()
            {
                new() { Behaviour = VideoParameterBehaviour.ChangeForAdded, Description = "Change parameters only for added ones", },
                new() { Behaviour = VideoParameterBehaviour.ChangeForSelectedAndAdded, Description = "Change parameters for selected and added ones", },
                new() { Behaviour = VideoParameterBehaviour.ChangeForAll, Description = "Change parameters for all", },
            };
            
            ConversionProfileOptions = new List<ConversionProfileOption>();
        }

        // TODO : Implement localization
        //private string GetOptionDescription(string key) =>
        //    _localizationService.GetValue(key);
        public void LoadOptionsFromSettings()
        {
            LoadCenversionProfileOptions();
        }

        public void LoadCenversionProfileOptions()
        {
            ConversionProfileOptions.Clear();

            for (int i = 0; i < _applicationSettings.ConversionProfiles.Count; i++)
            {
                var option = new ConversionProfileOption()
                {
                    Guid = _applicationSettings.ConversionProfiles[i].Guid,
                    Name = _applicationSettings.ConversionProfiles[i].Name,
                };

                ConversionProfileOptions.Add(option);
            }
        }
    }

}
