﻿using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;

namespace Armida.Tools.YTManager.Windows.Wpf.Utils.Options.Conversion
{
    public class ConversionProfileDialogResult
    {
        public ConversionProfileDialogDecision Decision { get; set; }
        public ConversionProfile Data { get; set; }
    }

    public enum ConversionProfileDialogDecision
    {
        Finish,
        Cancel,
    }

}
