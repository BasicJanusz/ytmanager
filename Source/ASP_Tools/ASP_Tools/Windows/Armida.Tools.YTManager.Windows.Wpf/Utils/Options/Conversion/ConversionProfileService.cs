﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using Armida.Tools.YTManager.Windows.Wpf.View.Options.Conversion;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion;
using System;
using System.Collections.ObjectModel;

namespace Armida.Tools.YTManager.Windows.Wpf.Utils.Options.Conversion
{
    public interface IConversionProfileService
    {
        public ConversionProfileDialogResult ShowEditDialog(ConversionProfile selectedConversionProfile, ObservableCollection<ConversionProfile> conversionProfiles);
        public ConversionProfileDialogResult ShowAddDialog(ConversionProfile selectedConversionProfile, ObservableCollection<ConversionProfile> conversionProfiles);
    }

    public class ConvertionProfileService : IConversionProfileService
    {
        private readonly IDialogService _dialogService;
        private readonly IConversionValidationService _validationService;
        private readonly IMediaFileDataProvider _mediaFileDataProvider;

        public ConvertionProfileService(IDialogService dialogService, IConversionValidationService validationService, IMediaFileDataProvider mediaFileDataProvider)
        {
            _dialogService = dialogService;
            _validationService = validationService;
            _mediaFileDataProvider = mediaFileDataProvider;
        }

        public ConversionProfileDialogResult ShowAddDialog(ConversionProfile selectedConversionProfile, ObservableCollection<ConversionProfile> conversionProfiles)
        {
            var profile = new ConversionProfile();

            if (selectedConversionProfile != null)
            {
                profile.Guid = Guid.NewGuid();
                profile.Name = selectedConversionProfile.Name;
                profile.Target = selectedConversionProfile.Target;
                profile.Source = selectedConversionProfile.Source;
                profile.Filters = selectedConversionProfile.Filters;
                profile.Codecs = selectedConversionProfile.Codecs;
            }

            var dialogViewModel = new ConversionProfileDialogViewModel(_dialogService, _validationService, _mediaFileDataProvider, profile, false);
            var dialogWindow = new ConversionProfileDialogWindow(dialogViewModel);
            dialogWindow.ShowDialog();

            return dialogViewModel.Result;
        }

        public ConversionProfileDialogResult ShowEditDialog(ConversionProfile selectedConversionProfile, ObservableCollection<ConversionProfile> conversionProfiles)
        {
            var profile = new ConversionProfile();
            profile.Target = _mediaFileDataProvider.ProvideDefaultMediaFileFormatEntry();

            if (selectedConversionProfile != null)
            {
                profile.Guid = selectedConversionProfile.Guid;
                profile.Name = selectedConversionProfile.Name;
                profile.Target = selectedConversionProfile.Target;
                profile.Source = selectedConversionProfile.Source;
                profile.Filters = selectedConversionProfile.Filters;
                profile.DisableAudio = selectedConversionProfile.DisableAudio;
                profile.DisableVideo = selectedConversionProfile.DisableVideo;
                profile.Codecs = selectedConversionProfile.Codecs;
            }

            var dialogViewModel = new ConversionProfileDialogViewModel(_dialogService, _validationService, _mediaFileDataProvider, profile, true);
            var dialogWindow = new ConversionProfileDialogWindow(dialogViewModel);
            dialogWindow.ShowDialog();

            return dialogViewModel.Result;
        }
    }
}
