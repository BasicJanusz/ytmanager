﻿using System.Windows.Controls;

namespace Armida.Tools.YTManager.Windows.Wpf.View.Options.Conversion
{
    /// <summary>
    /// Interaction logic for FilterListViewItem.xaml
    /// </summary>
    public partial class FilterListViewItem : UserControl
    {
        public FilterListViewItem()
        {
            InitializeComponent();
        }
    }
}
