﻿using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion;
using System;
using System.Windows;

namespace Armida.Tools.YTManager.Windows.Wpf.View.Options.Conversion
{
    /// <summary>
    /// Interaction logic for ConversionProfileDialogWindow.xaml
    /// </summary>
    public partial class ConversionProfileDialogWindow : Window, IDisposable
    {
        ConversionProfileDialogViewModel _viewModel;

        public ConversionProfileDialogWindow(ConversionProfileDialogViewModel viewModel)
        {
            _viewModel = viewModel;
            DataContext = _viewModel;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.CloseWindow += Close;
            }    
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_viewModel != null)
            {
                _viewModel.CloseWindow -= Close;
            }
        }
    }
}
