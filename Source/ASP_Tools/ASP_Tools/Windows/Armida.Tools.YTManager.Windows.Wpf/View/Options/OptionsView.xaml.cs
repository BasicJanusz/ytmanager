﻿using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options;
using System.Windows.Controls;

namespace Armida.Tools.YTManager.Windows.Wpf.View.Options
{
    /// <summary>
    /// Interaction logic for OptionsView.xaml
    /// </summary>
    public partial class OptionsView : UserControl
    {
        public OptionsView()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if(DataContext is OptionsViewModel viewModel)
            {
                viewModel.LoadSelectedOptionsFromSettings();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is OptionsViewModel viewModel)
            {
                viewModel.AreOptionsChanged = true;
            }
        }
    }
}
