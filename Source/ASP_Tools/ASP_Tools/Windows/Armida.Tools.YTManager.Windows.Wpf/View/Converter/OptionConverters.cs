﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Armida.Tools.YTManager.Windows.Wpf.View.Converter
{
    public class QualityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null &&
                value is Quality option)
            {
                return QualityHelper.GetOption(option);
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public static class QualityHelper
    {
        public static string GetOption(Quality quality) =>
            quality switch
            {
                Quality.Lowest => "96k",
                Quality.Low => "128k",
                Quality.Medium => "256k",
                Quality.High => "330k",
                Quality.Higher => "660k",
                Quality.Ultra => "990k",
                _ => "330k",
            };
    }
}
