﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Workspace;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Main;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Converter;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Downloader;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Service;
using System.Windows;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Utils;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options.Conversion;

namespace Armida.Tools.YTManager.Windows.Wpf.View
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IApplicationSettings _settings;
        private IDialogService _dialogService;
        private IYoutubeService _youtubeService;
        private IConversionService _conversionService;
        private DownloaderViewModel _downloaderViewModel;
        private OptionsViewModel _optionsViewModel;
        private VideoListViewModel _videoListViewModel;

        private bool _isReadingSettings;

        public MainWindow(IWorkspaceManager workspaceManager, IApplicationSettings settings, IDialogService dialogService)
        {
            _settings = settings;
            _dialogService = dialogService;

            var conversionContextProvider = new ConversionContextProvider();
            var optionsProvider = new OptionsProvider(_settings);
            optionsProvider.LoadOptionsFromSettings();
            var mediaFileDataProvider = new MediaFileDataProvider();
            mediaFileDataProvider.LoadData();

            var ffmpegQueryFactory = new FFmpegQueryFactory(mediaFileDataProvider);
            var validationService = new ConversionValidationService(conversionContextProvider);
            var conversionProfileService = new ConvertionProfileService(_dialogService, validationService, mediaFileDataProvider);

            _conversionService = new ConversionService(workspaceManager, _settings);
            _youtubeService = new YoutubeService(workspaceManager, _conversionService, Dispatcher, _settings, optionsProvider, conversionContextProvider, ffmpegQueryFactory);
            var conversionProfileListViewModel = new ConversionProfileListViewModel(_settings, dialogService, conversionProfileService, conversionContextProvider, mediaFileDataProvider, optionsProvider);

            _videoListViewModel = new VideoListViewModel(_conversionService, _settings, _dialogService, optionsProvider, conversionProfileListViewModel);
            _downloaderViewModel = new DownloaderViewModel(_videoListViewModel, _youtubeService, _conversionService, _settings, _dialogService, optionsProvider);
            var converterViewmodel = new ConverterViewModel(_conversionService, Dispatcher, _settings);
            _optionsViewModel = new OptionsViewModel(_settings, _dialogService, optionsProvider, conversionProfileListViewModel);
            var toolsViewModel = new ToolsViewModel(_downloaderViewModel, converterViewmodel, _optionsViewModel, _settings, _dialogService, optionsProvider);

            _videoListViewModel.HandleYoutubePage += toolsViewModel.HandleYoutubePage;

            _conversionService.FileConverted += (path) => converterViewmodel.CreateFile(path);
            _youtubeService.FileConverted += (path) => converterViewmodel.CreateFile(path);

            DataContext = new MainViewModel(toolsViewModel, _videoListViewModel, _optionsViewModel);

            InitializeComponent();
        }

        private void Window_PreviewDrop(object sender, DragEventArgs e)
        {
            var viewmodel = (MainViewModel)DataContext;

            if (viewmodel == null)
                return;

            string text = (string)e.Data.GetData(DataFormats.Text, false);
            if (text != null)
            {
                viewmodel.HandleText(text);
                return;
            }

            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files)
                viewmodel.HandleText(file);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ReadWindowSettings();

            _youtubeService?.StartProcessThread();
            _conversionService?.StartProcessThread();
        }

        private void ReadWindowSettings()
        {
            _isReadingSettings = true;

            if (_settings.UseWindowParametersFromPreviousSession)
            {
                Height = _settings.RecentWindowHeight;
                Width = _settings.RecentWindowWidth;
                WindowState = _settings.RecentWindowState;
            }
            else
            {
                Height = _settings.WindowHeight;
                Width = _settings.WindowWidth;
                WindowState = _settings.WindowState;
            }

            _isReadingSettings = false;
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            _downloaderViewModel.SaveVideosToSettings();
            _settings?.WriteToJson();
        }

        private void Window_StateChanged(object sender, System.EventArgs e)
        {
            if(_isReadingSettings ||
               _settings == null)
            {
                return;
            }

            _settings.RecentWindowState = WindowState;
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_isReadingSettings ||
               _settings == null ||
               double.Equals(double.NaN, Width) ||
               double.Equals(double.NaN, Height))
            {
                return;
            }

            _settings.RecentWindowWidth = Width;
            _settings.RecentWindowHeight = Height;
        }
    }
}
