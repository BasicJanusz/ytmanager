﻿using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Downloader;
using Gma.System.MouseKeyHook;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Armida.Tools.YTManager.Windows.Wpf.View.Tools.Downloader
{
    /// <summary>
    /// Logika interakcji dla klasy VideoListView.xaml
    /// </summary>
    public partial class VideoListView : UserControl, IDisposable
    {
        private readonly Window _mainWindow;

        private ScrollViewer _scrollViewer;
        private VideoListViewModel _viewModel; 
        private IKeyboardMouseEvents m_GlobalHook;
        private bool _isDraggingOptions;
        private System.Drawing.Point _mouseDownPosition;
        private System.Drawing.Point _mouseUpPosition;

        public VideoListView()
        {
            InitializeComponent();

            _mainWindow = Window.GetWindow(this);
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(_viewModel != null)
            {
                foreach (var video in e.RemovedItems)
                {
                    if (video is VideoItemViewModel videoItemViewModel)
                        _viewModel.DeselectVideo(videoItemViewModel);
                }

                foreach (var video in e.AddedItems)
                {
                    if (video is VideoItemViewModel videoItemViewModel)
                        _viewModel.SelectVideo(videoItemViewModel);
                }
            }
        }

        private void ScrollViewer_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e != null)
            {
                if (_scrollViewer == null)
                {
                    return;
                }

                var currentScrollSpeed = _viewModel.ScrollSpeed;
                var currentIteration = 0;

                if (e.Delta <= 0)
                {
                    while (currentIteration < currentScrollSpeed)
                    {
                        _scrollViewer.LineRight();
                        currentIteration++;
                    }
                    return;
                }

                while (currentIteration < currentScrollSpeed)
                {
                    _scrollViewer.LineLeft();
                    currentIteration++;
                }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            TrySubscribeToMainWindow();
            TrySubscribeToGlobalHooks();

            if (DataContext is VideoListViewModel viewModel)
            {
                _viewModel = viewModel;
            }

            DependencyObject senderAsDependencyObject = sender as DependencyObject;

            if(senderAsDependencyObject == null) 
            {
                return;
            }

            var child = FindChild(senderAsDependencyObject, "OptionsScrollViewer", true);

            if (child == null)
            {
                return;
            }

            _scrollViewer = (ScrollViewer)child;
        }

        private DependencyObject FindChild(DependencyObject parent, string name, bool useRecurrency = false)
        {
            var childCount = VisualTreeHelper.GetChildrenCount(parent);

            for (int i = 0; i < childCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                if (child is FrameworkElement childAsFrameworkElement)
                {
                    if (name == childAsFrameworkElement.Name)
                    {
                        return child as DependencyObject;
                    }

                    if (!useRecurrency)
                    {
                        continue;
                    }

                    var innerChild = FindChild(child, name, useRecurrency);
                    if (innerChild is FrameworkElement innerChildAsFE)
                    {
                        if (innerChildAsFE.Name == name)
                        {
                            return innerChild;
                        }
                    }
                }
            }

            return null;
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsText() &&
                DataContext is VideoListViewModel viewModel)
            {
                var text = Clipboard.GetText(TextDataFormat.Text);
                viewModel.OnGotFocus(text);
            }
        }

        private void TrySubscribeToMainWindow()
        {
            if(_mainWindow != null)
            {
                _mainWindow.Closed += OnMainWindowClosed;
            }
        }

        private void UnsubscribeFromMainWindow()
        {
            if (_mainWindow != null)
            {
                _mainWindow.Closed -= OnMainWindowClosed;
            }
        }

        private void OnMainWindowClosed(object? sender, EventArgs e)
        {
            Dispose();
        }

        private void TrySubscribeToGlobalHooks()
        {
            m_GlobalHook = Hook.GlobalEvents();

            m_GlobalHook.MouseUpExt += MouseUpExt;
            m_GlobalHook.MouseMoveExt += MouseMoveExt;
        }

        private void UnsubscribeToGlobalHooks()
        {
            m_GlobalHook.MouseUpExt -= MouseUpExt;
            m_GlobalHook.MouseMoveExt -= MouseMoveExt;

            m_GlobalHook.Dispose();
        }

        private void MouseUpExt(object? sender, MouseEventExtArgs e)
        {
            if(e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                _isDraggingOptions = false;
            }
        }

        private void MouseMoveExt(object? sender, MouseEventExtArgs e)
        {
            if(!_isDraggingOptions)
            {
                _mouseDownPosition = e.Location;
                return;
            }

            _mouseUpPosition = e.Location;

            var delta = (_mouseDownPosition.X - _mouseUpPosition.X) / 15;
            if (delta > 0)
            {
                _scrollViewer.LineRight();
                _mouseDownPosition = _mouseUpPosition;

                return;
            }

            if (delta < 0)
            {
                _scrollViewer.LineLeft();
                _mouseDownPosition = _mouseUpPosition;
            }
        }

        private void OptionsScrollViewer_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _isDraggingOptions = true;
        }

        public void Dispose()
        {
            UnsubscribeFromMainWindow();
            UnsubscribeToGlobalHooks();
        }
    }
}
