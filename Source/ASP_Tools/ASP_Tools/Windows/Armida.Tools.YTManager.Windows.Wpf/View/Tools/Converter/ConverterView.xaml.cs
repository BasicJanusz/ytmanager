﻿using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Converter;
using System.Windows.Controls;

namespace Armida.Tools.YTManager.Windows.Wpf.View.Tools.Converter
{
    /// <summary>
    /// Logika interakcji dla klasy ConverterView.xaml
    /// </summary>
    public partial class ConverterView : UserControl
    {
        public ConverterView()
        {
            InitializeComponent();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is ConverterViewModel viewModel)
            {
                foreach (var file in e.RemovedItems)
                {
                    viewModel.DeselectFile((MediaFileViewModel)file);
                }

                foreach (var file in e.AddedItems)
                {
                    viewModel.SelectFile((MediaFileViewModel)file);
                }
            }
        }
    }
}
