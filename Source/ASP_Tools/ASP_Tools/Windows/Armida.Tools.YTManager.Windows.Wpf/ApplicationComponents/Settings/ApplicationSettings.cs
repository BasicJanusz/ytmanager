﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Workspace;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;

namespace Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings
{
    public class ApplicationSettings : IApplicationSettings
    {
        private const string DownloadFolderName = "Download";
        private const string ConvertFolderName = "Convert";
        private const string AuthenticationFolderName = "Authentication";
        private const string JsonFileName = "ApplicationSession.json";

        public event Action DeleteDownloadedFileWhenRemovingVideoChanged;
        public event Action DeleteConvertedFileWhenRemovingVideoChanged;
        public event Action RecentDownloadRootFolderChanged;
        public event Action RecentConvertRootFolderChanged;
        public event Action DownloadDirectoryChanged;
        public event Action ConvertDirectoryChanged;
        public event Action AuthenticationDirectoryChanged;
        public event Action AddVideosFromPreviousSessionChanged;
        public event Action DontShowSessionLoadResultsChanged;
        public event Action AddVideosFromDownloadFolderChanged;
        public event Action AddVideosFromConvertFolderChanged;
        public event Action SelectedRemoveOptionChanged;
        public event Action SelectedConversionFormatChanged;
        public event Action SelectedPlayNonExistantVideoBehaviourChanged;
        public event Action UseWindowParametersFromPreviousSessionChanged;
        public event Action RecentWindowWidthChanged;
        public event Action RecentWindowHeightChanged;
        public event Action RecentWindowStateChanged;
        public event Action WindowWidthChanged;
        public event Action WindowHeightChanged;
        public event Action WindowStateChanged;
        public event Action ScrollSpeedChanged;
        public event Action WarnAboutConversionProfileRemovalChanged;
        public event Action QualityChanged;
        public event Action ConversionProfilesChanged;
        public event Action SelectedConversionProfileChanged;
        public event Action AllowPlaylistsChanged;
        public event Action AllowRadioChanged;
        public event Action WarnAboutNonExistingMediaChanged;
        public event Action SelectedVideoParameterBehaviourChanged;
        public event Action AutoLoadChanged;
        public event Action AutoDownloadChanged;
        public event Action AutoConvertChanged;
        public event Action DownloadModeChanged;
        public event Action DownloadFormatChanged;

        private readonly IWorkspaceManager _workspaceManager;
        private ApplicationSettingsData _data;

        public string SelectedConversionFormat
        {
            get => _data.SelectedConversionFormat;
            set
            {
                _data.SelectedConversionFormat = value;
                SelectedConversionFormatChanged?.Invoke();
            }
        }

        public string RecentDownloadRootFolder 
        { 
            get => _data.RecentDownloadRootFolder;
            set
            {
                _data.RecentDownloadRootFolder = value;
                RecentDownloadRootFolderChanged?.Invoke();
            }
        }
        
        public string RecentConvertRootFolder
        {
            get => _data.RecentConvertRootFolder;
            set
            {
                _data.RecentConvertRootFolder = value;
                RecentConvertRootFolderChanged?.Invoke();
            }
        }

        public string DownloadDirectory
        {
            get => _data.DownloadDirectory;
            set
            {
                _data.DownloadDirectory = value;
                DownloadDirectoryChanged?.Invoke();
            }
        }

        public string ConvertDirectory
        {
            get => _data.ConvertDirectory;
            set
            {
                _data.ConvertDirectory = value;
                ConvertDirectoryChanged?.Invoke();
            }
        }

        public string AuthenticationDirectory
        {
            get => _data.AuthenticationDirectory;
            set
            {
                _data.AuthenticationDirectory = value;
                AuthenticationDirectoryChanged?.Invoke();
            }
        }

        public bool AddVideosFromPreviousSession
        {
            get => _data.AddVideosFromPreviousSession;
            set
            {
                _data.AddVideosFromPreviousSession = value;
                AddVideosFromPreviousSessionChanged?.Invoke();
            }
        }

        public bool DontShowSessionLoadResults
        {
            get => _data.DontShowSessionLoadResults;
            set
            {
                _data.DontShowSessionLoadResults = value;
                DontShowSessionLoadResultsChanged?.Invoke();
            }
        }

        public bool AddVideosFromDownloadFolder
        {
            get => _data.AddVideosFromDownloadFolder;
            set
            {
                _data.AddVideosFromDownloadFolder = value;
                AddVideosFromDownloadFolderChanged?.Invoke();
            }
        }

        public bool AddVideosFromConvertFolder
        {
            get => _data.AddVideosFromConvertFolder;
            set
            {
                _data.AddVideosFromConvertFolder = value;
                AddVideosFromConvertFolderChanged?.Invoke();
            }
        }

        public RemoveOptionType SelectedRemoveOption
        {
            get => _data.SelectedRemoveOption;
            set
            {
                _data.SelectedRemoveOption = value;
                SelectedRemoveOptionChanged?.Invoke();
            }
        }

        public PlayNonExistantVideoBehaviour SelectedPlayNonExistantVideoBehaviour
        {
            get => _data.PlayNonExistantVideoBehaviour;
            set
            {
                _data.PlayNonExistantVideoBehaviour = value;
                SelectedPlayNonExistantVideoBehaviourChanged?.Invoke();
            }
        }

        public List<YTVideo> Videos
        {
            get => _data.Videos;
            set
            {
                _data.Videos = value;
            }
        }

        public bool UseWindowParametersFromPreviousSession
        {
            get => _data.UseWindowParametersFromPreviousSession;
            set
            {
                _data.UseWindowParametersFromPreviousSession = value;
                UseWindowParametersFromPreviousSessionChanged?.Invoke();
            }
        }

        public double RecentWindowWidth
        {
            get => _data.RecentWindowWidth;
            set
            {
                _data.RecentWindowWidth = value;
                RecentWindowWidthChanged?.Invoke();
            }
        }

        public double RecentWindowHeight
        {
            get => _data.RecentWindowHeight;
            set
            {
                _data.RecentWindowHeight = value;
                RecentWindowHeightChanged?.Invoke();
            }
        }

        public WindowState RecentWindowState
        {
            get => _data.RecentWindowState;
            set
            {
                _data.RecentWindowState = value;
                RecentWindowStateChanged?.Invoke();
            }
        }

        public double WindowWidth 
        { 
            get => _data.WindowWidth;
            set
            {
                _data.WindowWidth = value;
                WindowWidthChanged?.Invoke();
            }
        }

        public double WindowHeight
        {
            get => _data.WindowHeight;
            set
            {
                _data.WindowHeight = value;
                WindowHeightChanged?.Invoke();
            }
        }

        public WindowState WindowState
        {
            get => _data.WindowState;
            set
            {
                _data.WindowState = value;
                WindowStateChanged?.Invoke();
            }
        }

        public int ScrollSpeed
        {
            get => _data.ScrollSpeed;
            set
            {
                _data.ScrollSpeed = value;
                ScrollSpeedChanged?.Invoke();
            }
        }

        public bool WarnAboutConversionProfileRemoval
        {
            get => _data.WarnAboutConversionProfileRemoval;
            set
            {
                _data.WarnAboutConversionProfileRemoval = value;
                WarnAboutConversionProfileRemovalChanged?.Invoke();
            }
        }

        public Quality Quality
        {
            get => _data.Quality;
            set
            {
                _data.Quality = value;
                QualityChanged?.Invoke();
            }
        }

        public List<ConversionProfileData> ConversionProfiles
        {
            get => _data.ConversionProfiles;
            set
            {
                _data.ConversionProfiles = value;
                ConversionProfilesChanged?.Invoke();
            }
        }

        public Guid SelectedConversionProfile
        {
            get => _data.SelectedConversionProfile;
            set
            {
                _data.SelectedConversionProfile = value;
                SelectedConversionProfileChanged?.Invoke();
            }
        }

        public bool AllowPlaylists
        {
            get => _data.AllowPlaylists;
            set
            {
                _data.AllowPlaylists = value;
                AllowPlaylistsChanged?.Invoke();
            }
        }

        public bool AllowRadio
        {
            get => _data.AllowRadio;
            set
            {
                _data.AllowRadio = value;
                AllowRadioChanged?.Invoke();
            }
        }

        public bool WarnAboutNonExistingMedia
        {
            get => _data.WarnAboutNonExistingMedia;
            set
            {
                _data.WarnAboutNonExistingMedia = value;
                WarnAboutNonExistingMediaChanged?.Invoke();
            }
        }

        public VideoParameterBehaviour SelectedVideoParameterBehaviour
        { 
            get => _data.VideoParameterBehaviour;
            set
            {
                _data.VideoParameterBehaviour = value;
                SelectedVideoParameterBehaviourChanged?.Invoke();
            }
        }

        public bool AutoLoad
        {
            get => _data.AutoLoad;
            set
            {
                _data.AutoLoad = value;
                AutoLoadChanged?.Invoke();
            }
        }

        public bool AutoDownload
        {
            get => _data.AutoDownload;
            set
            {
                _data.AutoDownload = value;
                AutoDownloadChanged?.Invoke();
            }
        }

        public bool AutoConvert
        {
            get => _data.AutoConvert;
            set
            {
                _data.AutoConvert = value;
                AutoConvertChanged?.Invoke();
            }
        }

        public DownloadMode DownloadMode
        {
            get => _data.DownloadMode;
            set
            {
                _data.DownloadMode = value;
                DownloadModeChanged?.Invoke();
            }
        }

        public DownloadFormat DownloadFormat
        {
            get => _data.DownloadFormat;
            set
            {
                _data.DownloadFormat = value;
                DownloadFormatChanged?.Invoke();
            }
        }

        public ApplicationSettings(IWorkspaceManager workspaceManager)
        {
            _workspaceManager = workspaceManager;
            _workspaceManager.SetSettings(this);

            _data = new ApplicationSettingsData();
        }

        public void ReadFromJson()
        {
            var jsonPath = Path.Combine(_workspaceManager.ApplicationPath, JsonFileName);
            if (!File.Exists(jsonPath))
            {
                // TODO : prompt, file does not exist, created new
                _data = new ApplicationSettingsData();
                SetDefaultSettings();
                WriteToJson();
                return;
            }

            var jsonContent = File.ReadAllText(jsonPath);
            var applicationSettingsData = JsonConvert.DeserializeObject<ApplicationSettingsData>(jsonContent);

            if(applicationSettingsData == null)
            {
                // TODO : prompt, could not deserialize, created new
                _data = new ApplicationSettingsData();
                SetDefaultSettings();
                WriteToJson();
                return;
            }

            _data = applicationSettingsData;
        }

        public void WriteToJson()
        {
            var jsonPath = Path.Combine(_workspaceManager.ApplicationPath, JsonFileName);

            var dataAsJson = JsonConvert.SerializeObject(_data, Formatting.Indented);
            File.WriteAllText(jsonPath, dataAsJson);
        }

        public void SetDefaultSettings()
        {
            // TODO : Default settings
            RecentDownloadRootFolder = "C:\\";
            RecentConvertRootFolder = "C:\\";

            DownloadDirectory = Path.Combine(_workspaceManager.ApplicationPath, DownloadFolderName);
            ConvertDirectory = Path.Combine(_workspaceManager.ApplicationPath, ConvertFolderName);
            AuthenticationDirectory = Path.Combine(_workspaceManager.ApplicationPath, AuthenticationFolderName);
            
            AddVideosFromPreviousSession = true;
            DontShowSessionLoadResults = false;
            AddVideosFromDownloadFolder = false;
            AddVideosFromConvertFolder = false;
            
            SelectedRemoveOption = RemoveOptionType.Nothing;
            SelectedPlayNonExistantVideoBehaviour = PlayNonExistantVideoBehaviour.Ask;

            Videos = new List<YTVideo>();

            UseWindowParametersFromPreviousSession = true;
            RecentWindowWidth = 800d;
            RecentWindowHeight = 600d;
            RecentWindowState = WindowState.Normal;
            WindowWidth = 800d;
            WindowHeight = 600d;
            WindowState = WindowState.Normal;
            ScrollSpeed = 5;

            WarnAboutConversionProfileRemoval = true;
            Quality = Quality.High;
            ConversionProfiles = new List<ConversionProfileData>()
            {
                new()
                {
                    Guid = new Guid("886b2eb6-2e76-41ca-a0e6-0a57b5c1506c"),
                    Name = "To Mp3 (300 Kb/s)",
                    SourceFormat = MediaFileFormat.MPEG4,
                    TargetFormat = MediaFileFormat.MP3,
                    Filters = new List<ConversionFilterOption> { },
                    Codecs = new List<CodecOption>
                    {
                        new() { Codec = Codec.libmp3lame, Parameter="-b:a 320k", }
                    },
                },
                new()
                {
                    Guid = new Guid("b3427ae1-aca3-4aaa-aba4-9ff41cd1dcbd"),
                    Name = "To Mp3 (Bass Boost)",
                    SourceFormat = MediaFileFormat.MPEG4,
                    TargetFormat = MediaFileFormat.MP3,
                    Filters = new List<ConversionFilterOption> 
                    {
                        new() { Filter = Filter.firequalizer, Parameter = "=gain_entry='entry(0,10);entry(250,5)'" },
                    },
                    Codecs = new List<CodecOption>
                    {
                        new() { Codec = Codec.libmp3lame, Parameter = "-b:a 320k", }
                    },
                },
                new()
                {
                    Guid = new Guid("fc0ee7df-8b75-4dad-8e2b-2c7effcf4dd5"),
                    Name = "To Mp3 (Speed x1.25 + Bass Boost)",
                    SourceFormat = MediaFileFormat.MPEG4,
                    TargetFormat = MediaFileFormat.MP3,
                    Filters = new List<ConversionFilterOption>
                    {
                        new() { Filter = Filter.firequalizer, Parameter = "=gain_entry='entry(0,10);entry(250,5)'" },
                        new() { Filter = Filter.atempo, Parameter = "=1.25" },
                    },
                    Codecs = new List<CodecOption>
                    {
                        new() { Codec = Codec.libmp3lame, Parameter = "-b:a 320k", }
                    },
                },
                new()
                {
                    Guid = new Guid("d85c06cd-97fc-4d65-b26c-a9a8a48082a1"),
                    Name = "To Mp3 (Speed x1.5 + Bass Boost)",
                    SourceFormat = MediaFileFormat.MPEG4,
                    TargetFormat = MediaFileFormat.MP3,
                    Filters = new List<ConversionFilterOption>
                    {
                        new() { Filter = Filter.firequalizer, Parameter = "=gain_entry='entry(0,10);entry(250,5)'" },
                        new() { Filter = Filter.atempo, Parameter = "=1.5" },
                    },
                    Codecs = new List<CodecOption>
                    {
                        new() { Codec = Codec.libmp3lame, Parameter = "-b:a 320k", }
                    },
                },
                new()
                {
                    Guid = new Guid("7570309c-0c96-4ef3-ae6c-f6179dbd20f8"),
                    Name = "To Mp3 (Speed x2 + Bass Boost)",
                    SourceFormat = MediaFileFormat.MPEG4,
                    TargetFormat = MediaFileFormat.MP3,
                    Filters = new List<ConversionFilterOption>
                    {
                        new() { Filter = Filter.atempo, Parameter = "=2" },
                        new() { Filter = Filter.firequalizer, Parameter = "=gain_entry='entry(0,10);entry(250,5)'" },
                    },
                    Codecs = new List<CodecOption>
                    {
                        new() { Codec = Codec.libmp3lame, Parameter = "-b:a 320k", }
                    },
                },
            };
            SelectedConversionProfile = Guid.Empty;

            AllowPlaylists = true;
            AllowRadio = true;
            WarnAboutNonExistingMedia = true;
            SelectedVideoParameterBehaviour = VideoParameterBehaviour.ChangeForAdded;
            AutoLoad = true;
            AutoDownload = false;
            AutoConvert = false;

            DownloadMode = DownloadMode.MuxedHighestVideoQuality;
            DownloadFormat = DownloadFormat.MPEG4;
        }

        public List<Cookie> ExtractCookies()
        {
            var cookies = new List<Cookie>();

            if(!Directory.Exists(AuthenticationDirectory))
            {
                return cookies;
            }

            var files = Directory.GetFiles(AuthenticationDirectory);
            foreach(var file in files)
            {
                if(string.IsNullOrWhiteSpace(file))
                {
                    continue;
                }

                var auth = JsonConvert.DeserializeObject<Authentication>(File.ReadAllText(file));
                cookies.AddRange(auth.GetCookies());
            }

            return cookies;
        }
    }

    public class Authentication
    {
        private const string Domain = ".youtube.com";
        private const string Path = "/";
        public AuthObject web { get; set; }

        public List<Cookie> GetCookies()
        {
            var cookies = new List<Cookie>()
            {
                new(nameof(web.client_id), web.client_id, Path, Domain),
                new(nameof(web.project_id), web.project_id, Path, Domain),
                new(nameof(web.auth_uri), web.auth_uri, Path, Domain),
                new(nameof(web.token_uri), web.token_uri, Path, Domain),
                new(nameof(web.auth_provider_x509_cert_url), web.auth_provider_x509_cert_url, Path, Domain),
                new(nameof(web.client_secret), web.client_secret, Path, Domain),
            };

            return cookies;
        }
    }

    public class AuthObject
    {
        public string client_id { get; set; }
        public string project_id { get; set; }
        public string auth_uri { get; set; }
        public string token_uri { get; set; }
        public string auth_provider_x509_cert_url { get; set; }
        public string client_secret { get; set; }
        public string[] redirect_uris { get; set; }
    }

    public class ApplicationSettingsData
    {
        public string RecentDownloadRootFolder { get; set; }
        public string RecentConvertRootFolder { get; set; }
        public string SelectedConversionFormat { get; set; }

        public string DownloadDirectory { get; set; }
        public string ConvertDirectory { get; set; }
        public string AuthenticationDirectory { get; set; }
        public bool AddVideosFromPreviousSession { get; set; }
        public bool DontShowSessionLoadResults { get; set; }
        public bool AddVideosFromConvertFolder { get; set; }
        public bool AddVideosFromDownloadFolder { get; set; }
        public RemoveOptionType SelectedRemoveOption { get; set; }
        public PlayNonExistantVideoBehaviour PlayNonExistantVideoBehaviour { get; set; }

        public List<YTVideo> Videos { get; set; }

        public bool UseWindowParametersFromPreviousSession { get; set; }
        public double RecentWindowWidth { get; set; }
        public double RecentWindowHeight { get; set; }
        public WindowState RecentWindowState { get; set; }
        public double WindowWidth { get; set; }
        public double WindowHeight { get; set; }
        public WindowState WindowState { get; set; }
        public int ScrollSpeed { get; set; }

        public bool WarnAboutConversionProfileRemoval { get; set; }
        public List<ConversionProfileData> ConversionProfiles { get; set; }
        public Guid SelectedConversionProfile { get; set; }
        public Quality Quality { get; set; }

        public bool AllowPlaylists { get; set; }
        public bool AllowRadio { get; set; }
        public bool WarnAboutNonExistingMedia { get; set; }
        public VideoParameterBehaviour VideoParameterBehaviour { get; set; }
        public bool AutoLoad { get; set; }
        public bool AutoDownload { get; set; }
        public bool AutoConvert { get; set; }

        public DownloadMode DownloadMode { get; set; }
        public DownloadFormat DownloadFormat { get; set; }
    }

    public interface IApplicationSettings
    {
        public event Action DeleteDownloadedFileWhenRemovingVideoChanged;
        public event Action DeleteConvertedFileWhenRemovingVideoChanged;
        public event Action RecentDownloadRootFolderChanged;
        public event Action RecentConvertRootFolderChanged;
        public event Action DownloadDirectoryChanged;
        public event Action ConvertDirectoryChanged;
        public event Action AuthenticationDirectoryChanged;
        public event Action AddVideosFromPreviousSessionChanged;
        public event Action DontShowSessionLoadResultsChanged;
        public event Action AddVideosFromDownloadFolderChanged;
        public event Action AddVideosFromConvertFolderChanged;
        public event Action SelectedRemoveOptionChanged;
        public event Action SelectedConversionFormatChanged;
        public event Action SelectedPlayNonExistantVideoBehaviourChanged;
        public event Action UseWindowParametersFromPreviousSessionChanged;
        public event Action RecentWindowWidthChanged;
        public event Action RecentWindowHeightChanged;
        public event Action RecentWindowStateChanged;
        public event Action WindowWidthChanged;
        public event Action WindowHeightChanged;
        public event Action WindowStateChanged;
        public event Action ScrollSpeedChanged;
        public event Action WarnAboutConversionProfileRemovalChanged;
        public event Action QualityChanged;
        public event Action ConversionProfilesChanged;
        public event Action SelectedConversionProfileChanged;
        public event Action AllowPlaylistsChanged;
        public event Action AllowRadioChanged;
        public event Action WarnAboutNonExistingMediaChanged;
        public event Action SelectedVideoParameterBehaviourChanged;
        public event Action AutoLoadChanged;
        public event Action AutoDownloadChanged;
        public event Action AutoConvertChanged;
        public event Action DownloadModeChanged;
        public event Action DownloadFormatChanged;

        public string RecentDownloadRootFolder { get; set; }
        public string RecentConvertRootFolder { get; set; }

        public string DownloadDirectory { get; set; }
        public string ConvertDirectory { get; set; }
        public string AuthenticationDirectory { get; set; }
        public bool AddVideosFromPreviousSession { get; set; }
        public bool DontShowSessionLoadResults { get; set; }
        public bool AddVideosFromConvertFolder { get; set; }
        public bool AddVideosFromDownloadFolder { get; set; }
        public RemoveOptionType SelectedRemoveOption { get; set; } 
        public string SelectedConversionFormat { get; set; }
        public PlayNonExistantVideoBehaviour SelectedPlayNonExistantVideoBehaviour { get; set; }

        public List<YTVideo> Videos { get; set; }   

        public bool UseWindowParametersFromPreviousSession { get; set; }
        public double RecentWindowWidth { get; set; }
        public double RecentWindowHeight { get; set; }
        public WindowState RecentWindowState { get; set; }
        public double WindowWidth { get; set; }
        public double WindowHeight { get; set; }
        public WindowState WindowState { get; set; }
        public int ScrollSpeed { get; set; }

        public bool WarnAboutConversionProfileRemoval { get; set; }
        public Quality Quality { get; set; }
        public List<ConversionProfileData> ConversionProfiles { get; set; }
        public Guid SelectedConversionProfile { get; set; }

        public bool AllowPlaylists { get; set; }
        public bool AllowRadio { get; set; }
        public bool WarnAboutNonExistingMedia { get; set; }
        public VideoParameterBehaviour SelectedVideoParameterBehaviour { get; set; }
        public bool AutoLoad { get; set; }
        public bool AutoDownload { get; set; }
        public bool AutoConvert { get; set; }

        public DownloadMode DownloadMode { get; set; }
        public DownloadFormat DownloadFormat { get; set; }

        public void ReadFromJson();
        public void WriteToJson();
        public void SetDefaultSettings();
        
        public List<Cookie> ExtractCookies();
    }

    public enum RemoveOptionType
    {
        Nothing = 0,
        DownloadedFile = 1,
        ConvertedFile = 2,
        All = 3,
    }

    public class RemoveOption
    {
        public RemoveOptionType Type { get; set; }
        public string Description { get; set; }

        public RemoveOption() 
        {
            Type = RemoveOptionType.Nothing;
            Description = string.Empty;
        }

        public override string ToString() => Description;
    }

    public class WindowStateOption
    {
        public WindowState State { get; set; }
        public string Description { get; set; }

        public WindowStateOption()
        {
            State = WindowState.Normal;
            Description = string.Empty;
        }

        public override string ToString() => Description;
    }

    public class DownloadFormatOption
    {
        public DownloadFormat Format { get; set; }
        public string Description { get; set; }

        public DownloadFormatOption()
        {
            Format = DownloadFormat.MPEG4;
            Description = string.Empty;
        }

        public override string ToString() => Description;
    }

    public class DownloadModeOption
    {
        public DownloadMode Mode { get; set; }
        public string Description { get; set; }

        public DownloadModeOption()
        {
            Mode = DownloadMode.MuxedHighestVideoQuality;
            Description = string.Empty;
        }

        public override string ToString() => Description;
    }

    public class PlayNonExistantVideoOption
    {
        public PlayNonExistantVideoBehaviour Behaviour { get; set; }
        public string Description { get; set; }

        public PlayNonExistantVideoOption()
        {
            Behaviour = PlayNonExistantVideoBehaviour.Ask;
            Description = string.Empty;
        }

        public override string ToString() => Description;
    }

    public class VideoParameterBehaviourOption
    {
        public VideoParameterBehaviour Behaviour { get; set; }
        public string Description { get; set; }

        public VideoParameterBehaviourOption()
        {
            Behaviour = VideoParameterBehaviour.ChangeForAdded;
            Description = string.Empty;
        }

        public override string ToString() => Description;
    }

    public class ConversionProfileOption
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }

        public ConversionProfileOption()
        {
            Guid = Guid.Empty;
            Name = string.Empty;
        }

        public override string ToString() => Name;

        private static ConversionProfileOption _empty;
        public static ConversionProfileOption Empty => 
            _empty ??= new ConversionProfileOption()
            {
                Name = "No Profile Selected",
            };
    }

    public class ConversionProfileData
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public MediaFileFormat SourceFormat { get; set; }
        public MediaFileFormat TargetFormat { get; set; }
        public List<ConversionFilterOption> Filters { get; set; }
        public List<CodecOption> Codecs { get; set; }
    }

    public class ConversionFilterOption
    {
        public Filter Filter { get; set; }
        public string Parameter { get; set; }
    }

    public class CodecOption
    {
        public Codec Codec { get; set; }
        public string Parameter { get; set; }
    }
}
