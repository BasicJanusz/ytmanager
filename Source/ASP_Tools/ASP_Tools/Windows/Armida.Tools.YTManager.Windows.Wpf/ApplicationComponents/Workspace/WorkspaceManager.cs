﻿using System;
using System.IO;
using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;

namespace Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Workspace
{
    public class WorkspaceManager : IWorkspaceManager
    {
        private IApplicationSettings _settings;

        private string _mainAppPath;

        public bool IsSetup { get; private set; }

        public string DownloadPath => _settings.DownloadDirectory;
        public string ConvertPath => _settings.ConvertDirectory;
        public string AuthenticationPath => _settings.AuthenticationDirectory;
        public string ApplicationPath => _mainAppPath;

        public WorkspaceManager()
        {
            var assemblyLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if (assemblyLocation == null)
            {
                throw new Exception("Could not setup the workspace. Could not get the executing assembly location.");
            }

            _mainAppPath = assemblyLocation;
        }

        public void SetSettings(IApplicationSettings applicationSettings)
        {
            _settings = applicationSettings;

            _settings.ConvertDirectoryChanged += SetupWorkspace;
            _settings.DownloadDirectoryChanged += SetupWorkspace;
        }

        public void SetupWorkspace()
        {
            IsSetup = false;

            if (!TryCreateDirectory(DownloadPath))
            {
                return;
                //throw new Exception($"Could not setup the donwload Directory at '{DownloadPath}'");
            }

            if (!TryCreateDirectory(ConvertPath))
            {
                return;
                //throw new Exception($"Could not setup the convert Directory at '{ConvertPath}'");
            }

            if (!TryCreateDirectory(AuthenticationPath))
            {
                return;
                //throw new Exception($"Could not setup the authentication Directory at '{AuthenticationPath}'");
            }

            IsSetup = true;
        }

        private static bool TryCreateDirectory(string targetPath)
        {
            if(string.IsNullOrWhiteSpace(targetPath))
            {
                return false;
            }

            if(Directory.Exists(targetPath)) 
            {
                return true; 
            }

            var directory = Directory.CreateDirectory(targetPath);

            return directory.Exists;
        }
    }

    public interface IWorkspaceManager
    {
        public bool IsSetup { get; }

        public string DownloadPath { get; }
        public string ConvertPath { get; }
        public string AuthenticationPath { get; }
        public string ApplicationPath { get; }

        public void SetSettings(IApplicationSettings applicationSettings);
        public void SetupWorkspace();
    }
}
