﻿using AngleSharp.Dom;
using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options.Conversion;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion
{
    public class ConversionProfileDialogViewModel : CoreViewModel
    {
        public event Action CloseWindow;

        private readonly IDialogService _dialogService;
        private readonly IConversionValidationService _validationService;

        private ObservableCollection<MediaFileFilterEntry> _supportedBitstreamFilters { get; }
        private ObservableCollection<MediaFileFilterEntry> _supportedFilters { get; }
        private ObservableCollection<CodecEntry> _supportedDecoders { get; }
        private ObservableCollection<CodecEntry> _supportedEncoders { get; }

        private MediaFileFormatEntry _selectedSourceFormat;
        private MediaFileFilterEntry _selectedBitstreamFilter;
        private MediaFileFormatEntry _selectedTargetFormat;
        private MediaFileFilterEntry _selectedFilter;
        private CodecEntry _selectedDecoder;
        private CodecEntry _selectedEncoder;
        private string _name;
        private bool _isBitstreamFilterSettingEnabled;
        private bool _isDisableAudioAvailable;
        private bool _disableAudio;
        private bool _isDisableVideoAvailable;
        private bool _disableVideo;
        private bool _isFilterSettingEnabled;
        private bool _isDecoderSettingEnabled;
        private bool _isEncoderSettingEnabled;
        private bool _isLoaded;
        private bool _isEditing;

        public ConversionProfile Data { get; set; }
        public ConversionProfileDialogResult Result { get; set; }

        public ObservableCollection<MediaFileFormatEntry> AvailableSourceFormats { get; }
        public ObservableCollection<MediaFileFormatEntry> AvailableTargetFormats { get; }
        public ObservableCollection<MediaFileFilterEntry> CompatibleBitstreamFilters { get; }
        public ObservableCollection<MediaFileFilterEntry> CompatibleFilters { get; }
        public ObservableCollection<CodecEntry> CompatibleDecoders { get; }
        public ObservableCollection<CodecEntry> CompatibleEncoders { get; }
        public ObservableCollection<FilterViewModel> ChosenBitstreamFilters { get; }
        public ObservableCollection<FilterViewModel> ChosenFilters { get; }
        public ObservableCollection<CodecViewModel> ChosenDecoders { get; }
        public ObservableCollection<CodecViewModel> ChosenEncoders { get; }

        public bool AreBitstreamFiltersEnabled => SelectedSourceFormat != null;
        public bool AreFiltersEnabled => SelectedSourceFormat != null && SelectedTargetFormat != null;
        public bool IsTargetFormatEnabled => SelectedSourceFormat != null;
        public bool AreDecodersAvailable => SelectedSourceFormat != null;
        public bool AreEncodersAvailable => SelectedSourceFormat != null && SelectedTargetFormat != null;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }

        public MediaFileFormatEntry SelectedSourceFormat
        {
            get => _selectedSourceFormat;
            set
            {
                _selectedSourceFormat = value;
                SourceFormatChanged();
                RaisePropertyChanged(nameof(AreFiltersEnabled));
                RaisePropertyChanged(nameof(AreDecodersAvailable));
                RaisePropertyChanged(nameof(AreEncodersAvailable));
                RaisePropertyChanged(nameof(IsTargetFormatEnabled));
                RaisePropertyChanged(nameof(SelectedSourceFormat));
            }
        }

        public MediaFileFormatEntry SelectedTargetFormat
        {
            get => _selectedTargetFormat;
            set
            {
                _selectedTargetFormat = value;
                TargetFormatChanged();
                RaisePropertyChanged(nameof(AreFiltersEnabled));
                RaisePropertyChanged(nameof(SelectedTargetFormat));
                RaisePropertyChanged(nameof(AreDecodersAvailable));
                RaisePropertyChanged(nameof(AreEncodersAvailable));
            }
        }

        public MediaFileFilterEntry SelectedBitstreamFilter
        {
            get => _selectedBitstreamFilter;
            set
            {
                _selectedBitstreamFilter = value;
                SelectedBitstreamFilterChanged();
                RaisePropertyChanged(nameof(CompatibleBitstreamFilters));
                RaisePropertyChanged(nameof(SelectedBitstreamFilter));
                if (SelectedBitstreamFilter != null)
                {
                    RaisePropertyChanged(nameof(SelectedBitstreamFilter.Description));
                }
            }
        }

        public MediaFileFilterEntry SelectedFilter
        {
            get => _selectedFilter;
            set
            {
                _selectedFilter = value;
                SelectedFilterChanged();
                RaisePropertyChanged(nameof(CompatibleFilters));
                RaisePropertyChanged(nameof(SelectedFilter));
                if (SelectedFilter != null)
                {
                    RaisePropertyChanged(nameof(SelectedFilter.Description));
                }
            }
        }

        public CodecEntry SelectedDecoder
        {
            get => _selectedDecoder;
            set
            {
                _selectedDecoder = value;
                SelectedDecoderChanged();
                RaisePropertyChanged(nameof(CompatibleDecoders));
                RaisePropertyChanged(nameof(SelectedDecoder));
                if (SelectedDecoder != null)
                {
                    RaisePropertyChanged(nameof(SelectedDecoder.Description));
                }
            }
        }

        public CodecEntry SelectedEncoder
        {
            get => _selectedEncoder;
            set
            {
                _selectedEncoder = value;
                SelectedEncoderChanged();
                RaisePropertyChanged(nameof(CompatibleEncoders));
                RaisePropertyChanged(nameof(SelectedEncoder));
                if (SelectedEncoder != null)
                {
                    RaisePropertyChanged(nameof(SelectedEncoder.Description));
                }
            }
        }

        public bool IsBitstreamFilterSettingEnabled
        {
            get => _isBitstreamFilterSettingEnabled;
            set
            {
                _isBitstreamFilterSettingEnabled = value;
                RaisePropertyChanged(nameof(IsBitstreamFilterSettingEnabled));
            }
        }

        public bool IsDisableAudioAvailable
        {
            get => _isDisableAudioAvailable;
            set
            {
                _isDisableAudioAvailable = value;
                RaisePropertyChanged(nameof(IsDisableAudioAvailable));
            }
        }
        public bool DisableAudio
        {
            get => _disableAudio;
            set
            {
                _disableAudio = value;

                if(_isLoaded)
                {
                    ReloadBitstreamFilters();
                    ReloadFilters();
                }

                RaisePropertyChanged(nameof(DisableAudio));
            }
        }

        public bool IsDisableVideoAvailable
        {
            get => _isDisableVideoAvailable;
            set
            {
                _isDisableVideoAvailable = value;
                RaisePropertyChanged(nameof(IsDisableVideoAvailable));
            }
        }
        public bool DisableVideo
        {
            get => _disableVideo;
            set
            {
                _disableVideo = value;

                if (_isLoaded)
                {
                    ReloadBitstreamFilters();
                    ReloadFilters();
                }

                RaisePropertyChanged(nameof(DisableVideo));
            }
        }

        public bool IsFilterSettingEnabled
        {
            get => _isFilterSettingEnabled;
            set
            {
                _isFilterSettingEnabled = value;
                RaisePropertyChanged(nameof(IsFilterSettingEnabled));
            }
        }

        public bool IsDecoderSettingEnabled 
        { 
            get => _isDecoderSettingEnabled;
            set
            {
                _isDecoderSettingEnabled = value;
                RaisePropertyChanged(nameof(IsDecoderSettingEnabled));
            }
        }

        public bool IsEncoderSettingEnabled
        {
            get => _isEncoderSettingEnabled;
            set
            {
                _isEncoderSettingEnabled = value;
                RaisePropertyChanged(nameof(IsEncoderSettingEnabled));
            }
        }

        public ICommand CancelCommand { get; }
        public ICommand FinishCommand { get; }
        public ICommand SetSelectedBitstreamFilterCommand { get; }
        public ICommand SetSelectedFilterCommand { get; }
        public ICommand SetSelectedDecoderCommand { get; }
        public ICommand SetSelectedEncoderCommand { get; }

        public ConversionProfileDialogViewModel(IDialogService dialogService, IConversionValidationService validationService, IMediaFileDataProvider mediaFileDataProvider, ConversionProfile profile, bool isEditing)
        {
            _dialogService = dialogService;
            _validationService = validationService;

            Data = null == profile ? new ConversionProfile() : profile;

            Result = new ConversionProfileDialogResult()
            {
                Decision = ConversionProfileDialogDecision.Cancel,
            };

            _supportedBitstreamFilters = mediaFileDataProvider.ProvideAvailableBitstreamFilters();
            CompatibleBitstreamFilters = new ObservableCollection<MediaFileFilterEntry>();
            CompatibleBitstreamFilters.CollectionChanged += (s, e) => RaisePropertyChanged(nameof(CompatibleBitstreamFilters));
            ChosenBitstreamFilters = new ObservableCollection<FilterViewModel>();
            ChosenBitstreamFilters.CollectionChanged += (s, e) => RaisePropertyChanged(nameof(ChosenBitstreamFilters));

            _supportedFilters = mediaFileDataProvider.ProvideAvailableFilters();
            CompatibleFilters = new ObservableCollection<MediaFileFilterEntry>();
            CompatibleFilters.CollectionChanged += (s, e) => RaisePropertyChanged(nameof(CompatibleFilters));
            ChosenFilters = new ObservableCollection<FilterViewModel>();
            ChosenFilters.CollectionChanged += (s, e) => RaisePropertyChanged(nameof(ChosenFilters));

            _supportedDecoders = mediaFileDataProvider.ProvideAvailableDecoders();
            CompatibleDecoders = new ObservableCollection<CodecEntry>();
            CompatibleDecoders.CollectionChanged += (s, e) => RaisePropertyChanged(nameof(CompatibleDecoders));
            ChosenDecoders = new ObservableCollection<CodecViewModel>();
            ChosenDecoders.CollectionChanged += (s, e) => RaisePropertyChanged(nameof(ChosenDecoders));

            _supportedEncoders = mediaFileDataProvider.ProvideAvailableEncoders();
            CompatibleEncoders = new ObservableCollection<CodecEntry>();
            CompatibleEncoders.CollectionChanged += (s, e) => RaisePropertyChanged(nameof(CompatibleEncoders));
            ChosenEncoders = new ObservableCollection<CodecViewModel>();
            ChosenEncoders.CollectionChanged += (s, e) => RaisePropertyChanged(nameof(ChosenEncoders));

            AvailableSourceFormats = mediaFileDataProvider.ProvideAvailableFormats();
            AvailableTargetFormats = mediaFileDataProvider.ProvideAvailableFormats();

            LoadInitialValues();

            SetSelectedBitstreamFilterCommand = new RelayCommand(SetSelectedBitstreamFilter);
            SetSelectedFilterCommand = new RelayCommand(SetSelectedFilter);
            CancelCommand = new RelayCommand(Cancel);
            FinishCommand = new RelayCommand(Finish);
            SetSelectedDecoderCommand = new RelayCommand(SetSelectedDecoder);
            SetSelectedEncoderCommand = new RelayCommand(SetSelectedEncoder);

            _isEditing = isEditing;
        }

        private void LoadInitialValues()
        {
            SelectedSourceFormat = Data.Source;
            SelectedTargetFormat = Data.Target;
            Name = Data.Name;
            DisableAudio = Data.DisableAudio;
            DisableVideo = Data.DisableVideo;

            foreach(var filter in Data.Filters)
            {
                switch (filter.Type)
                {
                    case FilterType.Audio:
                    case FilterType.Video:
                        var filterViewModel = new FilterViewModel(filter);
                        filterViewModel.RemoveBehaviour += RemoveFilter;
                        ChosenFilters.Add(filterViewModel);
                        break;

                    case FilterType.BitstreamAudio: 
                    case FilterType.BitstreamVideo: 
                    case FilterType.BitstreamSubtitle:
                        var bitstreamFilterViewModel = new FilterViewModel(filter);
                        bitstreamFilterViewModel.RemoveBehaviour += RemoveBitstreamFilter;
                        ChosenBitstreamFilters.Add(bitstreamFilterViewModel);
                        break;
                }
            }

            foreach(var codec in Data.Codecs)
            {
                switch(codec.Type)
                {
                    case CodecType.Decoder:
                        var decoderViewModel = new CodecViewModel(codec);
                        decoderViewModel.RemoveBehaviour += RemoveDecoder;
                        ChosenDecoders.Add(decoderViewModel);
                        break;

                    case CodecType.Encoder:
                        var encoderViewModel = new CodecViewModel(codec);
                        encoderViewModel.RemoveBehaviour += RemoveDecoder;
                        ChosenEncoders.Add(encoderViewModel);
                        break;

                    case CodecType.DecoderEncoder: // TODO : Investigate and reimplement
                        var encoderDecoderViewModel = new CodecViewModel(codec);
                        encoderDecoderViewModel.RemoveBehaviour += RemoveDecoder;
                        ChosenDecoders.Add(encoderDecoderViewModel);
                        ChosenEncoders.Add(encoderDecoderViewModel);
                        break;
                }
            }
        }

        private void SourceFormatChanged()
        {
            if ((SelectedSourceFormat == null))
            {
                IsDisableAudioAvailable = false;
                DisableAudio = true;
                IsDisableVideoAvailable = false;
                DisableVideo = true;
            }
            else
            {
                RaisePropertyChanged(nameof(AreBitstreamFiltersEnabled));

                switch (SelectedSourceFormat.MediaType)
                {
                    case MediaType.AudioVideo:
                        IsDisableAudioAvailable = true;
                        DisableAudio = false;
                        IsDisableVideoAvailable = true;
                        DisableVideo = false;
                        break;
                    case MediaType.AudioOnly:
                        IsDisableAudioAvailable = true;
                        DisableAudio = false;
                        IsDisableVideoAvailable = false;
                        DisableVideo = true;
                        break;
                    case MediaType.VideoOnly:
                        IsDisableAudioAvailable = false;
                        DisableAudio = true;
                        IsDisableVideoAvailable = true;
                        DisableVideo = false;
                        break;

                    default:
                        break;
                }
            }

            ReloadBitstreamFilters();
            ReloadFilters();
            ReloadDecoders();

            _isLoaded = true;
        }

        private void ReloadBitstreamFilters()
        {
            SelectedBitstreamFilter = null;
            
            CompatibleBitstreamFilters.Clear();
            ChosenBitstreamFilters.Clear();

            if(SelectedSourceFormat == null)
            {
                RaisePropertyChanged(nameof(AreBitstreamFiltersEnabled));
                return;
            }

            foreach (var filter in _supportedBitstreamFilters)
            {
                if (_selectedSourceFormat.MediaType == MediaType.AudioVideo && ((filter.Type == FilterType.BitstreamAudio && !DisableAudio) || (filter.Type == FilterType.BitstreamVideo && !DisableVideo)))
                {
                    CompatibleBitstreamFilters.Add(filter);
                    continue;
                }

                if(_selectedSourceFormat.MediaType == MediaType.AudioOnly && filter.Type == FilterType.BitstreamAudio && !DisableAudio)
                {
                    CompatibleBitstreamFilters.Add(filter);
                    continue;
                }

                if(_selectedSourceFormat.MediaType == MediaType.VideoOnly && filter.Type == FilterType.BitstreamVideo && !DisableVideo)
                {
                    CompatibleBitstreamFilters.Add(filter);
                }
            }

            RaisePropertyChanged(nameof(AreBitstreamFiltersEnabled));
        }

        private void ReloadFilters()
        {
            SelectedFilter = null;

            CompatibleFilters.Clear();
            ChosenFilters.Clear();
            
            if (_selectedTargetFormat == null)
            {
                RaisePropertyChanged(nameof(AreFiltersEnabled));
                return;
            }

            foreach (var filter in _supportedFilters)
            {
                if (_selectedTargetFormat.MediaType == MediaType.AudioVideo && ((filter.Type == FilterType.Audio && !DisableAudio) || (filter.Type == FilterType.Video && !DisableVideo)))
                {
                    CompatibleFilters.Add(filter);
                    continue;
                }

                if (_selectedTargetFormat.MediaType == MediaType.AudioOnly && filter.Type == FilterType.Audio && !DisableAudio)
                {
                    CompatibleFilters.Add(filter);
                    continue;
                }

                if (_selectedTargetFormat.MediaType == MediaType.VideoOnly && filter.Type == FilterType.Video && !DisableVideo)
                {
                    CompatibleFilters.Add(filter);
                }
            }

            RaisePropertyChanged(nameof(AreFiltersEnabled));
        }

        private void ReloadDecoders()
        {
            SelectedDecoder = null;

            CompatibleDecoders.Clear();
            ChosenDecoders.Clear();

            if (_selectedSourceFormat == null)
            {
                RaisePropertyChanged(nameof(AreDecodersAvailable));
                return;
            }

            foreach (var decoder in _supportedDecoders)
            {
                CompatibleDecoders.Add(decoder);
            }
        }

        private void ReloadEncoders()
        {
            SelectedEncoder = null;

            CompatibleEncoders.Clear();
            ChosenEncoders.Clear();

            if (_selectedTargetFormat == null)
            {
                RaisePropertyChanged(nameof(AreEncodersAvailable));
                return;
            }

            foreach (var encoder in _supportedEncoders)
            {
                CompatibleEncoders.Add(encoder);
            }
        }

        private void SelectedBitstreamFilterChanged()
        {
            IsBitstreamFilterSettingEnabled = SelectedBitstreamFilter != null;
        }

        private void SelectedFilterChanged()
        {
            IsFilterSettingEnabled = SelectedFilter != null;
        }

        public void SelectedDecoderChanged()
        {
            IsDecoderSettingEnabled = SelectedDecoder != null;
        }

        public void SelectedEncoderChanged()
        {
            IsEncoderSettingEnabled = SelectedEncoder != null;
        }

        private void TargetFormatChanged()
        {
            ReloadFilters();
            ReloadEncoders();
        }

        private void SetSelectedBitstreamFilter()
        {
            if (ChosenBitstreamFilters == null)
            {
                return;
            }

            if (ContainsBitstreamFilter(SelectedBitstreamFilter.Filter))
            {
                return;
            }

            // Check for existing and update
            if(ChosenBitstreamFilters.Count > 0)
            {
                for(int i=0; i<ChosenBitstreamFilters.Count; i++)
                {
                    if (ChosenBitstreamFilters[i].Type == SelectedBitstreamFilter.Type)
                    {
                        var updatedEntry = MediaFileFilterEntry.Copy(SelectedBitstreamFilter);
                        var viewModel = new FilterViewModel(updatedEntry);
                        viewModel.RemoveBehaviour += RemoveBitstreamFilter;

                        ChosenBitstreamFilters[i] = viewModel;
                        SelectedBitstreamFilter = null;
                        return;
                    }
                }
            }

            // create new
            var entry = MediaFileFilterEntry.Copy(SelectedBitstreamFilter);
            var filterViewModel = new FilterViewModel(entry);
            ChosenBitstreamFilters.Add(filterViewModel);

            entry.Guid = Guid.NewGuid();
            filterViewModel.RemoveBehaviour += RemoveBitstreamFilter;

            SelectedBitstreamFilter = null;
        }

        private bool ContainsBitstreamFilter(Filter filter)
        {
            foreach(var alreadySetBitstreamFilter in ChosenBitstreamFilters)
            {
                if (filter == alreadySetBitstreamFilter.Filter)
                {
                    return true;
                }
            }

            return false;
        }

        private void SetSelectedFilter()
        {
            if (ChosenFilters == null)
            {
                return;
            }

            if (ContainsFilter(SelectedFilter.Filter))
            {
                return;
            }

            var entry = MediaFileFilterEntry.Copy(SelectedFilter);
            var filterViewModel = new FilterViewModel(entry);
            ChosenFilters.Add(filterViewModel);

            entry.Guid = Guid.NewGuid();
            filterViewModel.RemoveBehaviour += RemoveFilter;

            SelectedFilter = null;
        }

        private bool ContainsFilter(Filter filter)
        {
            foreach (var alreadySetFilter in ChosenFilters)
            {
                if (filter == alreadySetFilter.Filter)
                {
                    return true;
                }
            }

            return false;
        }

        private void SetSelectedDecoder()
        {
            if(ChosenDecoders == null)
            {
                return;
            }

            if(ContainsDecoder(SelectedDecoder.Codec))
            {
                return;
            }

            var entry = CodecEntry.Copy(SelectedDecoder);
            var codecViewModel = new CodecViewModel(entry);
            ChosenDecoders.Add(codecViewModel);

            entry.Guid = Guid.NewGuid();
            codecViewModel.RemoveBehaviour += RemoveDecoder;
        }

        private bool ContainsDecoder(Codec codec)
        {
            foreach (var alreadySetCodec in ChosenDecoders)
            {
                if (codec == alreadySetCodec.Codec)
                {
                    return true;
                }
            }

            return false;
        }

        private void SetSelectedEncoder()
        {
            if (ChosenEncoders == null)
            {
                return;
            }

            if (ContainsEncoder(SelectedEncoder.Codec))
            {
                return;
            }

            var entry = CodecEntry.Copy(SelectedEncoder);
            var codecViewModel = new CodecViewModel(entry);
            ChosenEncoders.Add(codecViewModel);

            entry.Guid = Guid.NewGuid();
            codecViewModel.RemoveBehaviour += RemoveDecoder;
        }

        private bool ContainsEncoder(Codec codec)
        {
            foreach (var alreadySetCodec in ChosenEncoders)
            {
                if (codec == alreadySetCodec.Codec)
                {
                    return true;
                }
            }

            return false;
        }

        private void RemoveBitstreamFilter(Filter filter) // TODO : pass viewmodel instead of just the filter
        {
            FilterViewModel viewModel = null;

            foreach (var entry in ChosenBitstreamFilters)
            {
                if(filter == entry.Filter)
                {
                    viewModel = entry;
                }
            }

            if (viewModel == null)
            {
                return;
            }

            ChosenBitstreamFilters.Remove(viewModel);
        }

        private void RemoveFilter(Filter filter) // TODO : pass viewmodel instead of just the filter
        {
            FilterViewModel viewModel = null;

            foreach (var entry in ChosenFilters)
            {
                if (filter == entry.Filter)
                {
                    viewModel = entry;
                }
            }

            if (viewModel == null)
            {
                return;
            }

            ChosenFilters.Remove(viewModel);
        }

        private void RemoveDecoder(Codec codec) // TODO : pass viewmodel instead of just the filter
        {
            CodecViewModel viewModel = null;

            foreach (var entry in ChosenDecoders)
            {
                if (codec == entry.Codec)
                {
                    viewModel = entry;
                }
            }

            if (viewModel == null)
            {
                return;
            }

            ChosenDecoders.Remove(viewModel);
        }

        private void RemoveEncoder(Codec codec) // TODO : pass viewmodel instead of just the filter
        {
            CodecViewModel viewModel = null;

            foreach (var entry in ChosenEncoders)
            {
                if (codec == entry.Codec)
                {
                    viewModel = entry;
                }
            }

            if (viewModel == null)
            {
                return;
            }

            ChosenEncoders.Remove(viewModel);
        }

        private void Finish()
        {
            if (Data.Guid == Guid.Empty)
            {
                Data.Guid = Guid.NewGuid();
            }

            Data.Name = Name;
            Data.Source = SelectedSourceFormat;
            Data.Target = SelectedTargetFormat;
            
            Data.Filters.Clear();
            if (ChosenBitstreamFilters?.Count > 0)
            {
                MediaFileFilterEntry[] bitstreamFilterEntries = new MediaFileFilterEntry[ChosenBitstreamFilters.Count];
                for(int i=0; i<ChosenBitstreamFilters.Count; i++)
                {
                    bitstreamFilterEntries[i] = ChosenBitstreamFilters[i].Entry;
                }

                Data.Filters.AddRange(bitstreamFilterEntries);
            }
            if (ChosenFilters?.Count > 0)
            {
                MediaFileFilterEntry[] filterEntries = new MediaFileFilterEntry[ChosenFilters.Count];
                for (int i = 0; i < ChosenFilters.Count; i++)
                {
                    filterEntries[i] = ChosenFilters[i].Entry;
                }

                Data.Filters.AddRange(filterEntries);
            }

            Data.Codecs.Clear();
            if (ChosenDecoders?.Count > 0)
            {
                CodecEntry[] codecEntries = new CodecEntry[ChosenDecoders.Count];
                for (int i = 0; i < ChosenDecoders.Count; i++)
                {
                    codecEntries[i] = ChosenDecoders[i].Entry;
                }

                Data.Codecs.AddRange(codecEntries);
            }
            if (ChosenEncoders?.Count > 0) 
            {
                CodecEntry[] codecEntries = new CodecEntry[ChosenEncoders.Count];
                for (int i = 0; i < ChosenEncoders.Count; i++)
                {
                    codecEntries[i] = ChosenEncoders[i].Entry;
                }

                Data.Codecs.AddRange(codecEntries);
            }

            var validationResult = _validationService.ValidateConversionProfile(Data, _isEditing);

            if(validationResult.Result == ValidationResultEnum.Failure) 
            {
                _ = _dialogService.ShowDialog("Conversion Profile Error", $"Cannot process conversion profile.\n{validationResult.ErrorMessage}");
                return;
            }

            Result.Decision = ConversionProfileDialogDecision.Finish;
            Result.Data = Data;

            CloseWindow?.Invoke();
        }

        private void Cancel()
        {
            Result.Decision = ConversionProfileDialogDecision.Cancel;
            Result.Data = Data;

            CloseWindow?.Invoke();
        }
    }
}
