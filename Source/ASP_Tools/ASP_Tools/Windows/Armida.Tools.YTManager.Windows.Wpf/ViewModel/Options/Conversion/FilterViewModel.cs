﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion
{
    public class FilterViewModel : CoreViewModel
    {
        public event Action<Filter> RemoveBehaviour;

        private readonly MediaFileFilterEntry _entry;

        public Filter Filter => _entry.Filter;
        public FilterType Type => _entry.Type;
        public MediaFileFilterEntry Entry => _entry;
        public string Name => _entry.Name;
        public string Description => _entry.Description;
        public string Parameter
        {
            get => _entry.Parameter;
            set
            {
                _entry.Parameter = value;
                RaisePropertyChanged(nameof(Parameter));
            }
        }

        public ICommand RemoveCommand { get; }

        public FilterViewModel(MediaFileFilterEntry entry) 
        {
            _entry = entry;
            RemoveCommand = new RelayCommand(RemoveFilter);
            
            RaisePropertiesChanged();
        }

        private void RaisePropertiesChanged()
        {
            RaisePropertyChanged(nameof(Filter));
            RaisePropertyChanged(nameof(Type));
            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Description));
            RaisePropertyChanged(nameof(Parameter));
            RaisePropertyChanged(nameof(RemoveCommand));
        }

        private void RemoveFilter()
        {
            RemoveBehaviour?.Invoke(Filter);
        }
    }
}
