﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Utils;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options.Conversion;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion
{
    public class ConversionProfileListViewModel : CoreViewModel
    {
        public event Action OptionsChangedBehaviour;

        private readonly IApplicationSettings _applicationSettings;
        private readonly IDialogService _dialogService;
        private readonly IConversionProfileService _conversionProfileService;
        private readonly IConversionContextProvider _conversionContextProvider;
        private readonly IMediaFileDataProvider _mediaFileDataProvider;
        private readonly IOptionsProvider _optionsProvider;
        private bool _updateConversionProfileSettings;

        public ObservableCollection<ConversionProfile> ConversionProfiles { get; private set; }

        private ConversionProfile _selectedConversionProfile;
        public ConversionProfile SelectedConversionProfile
        {
            get => _selectedConversionProfile;
            set
            {
                _selectedConversionProfile = value;
                RaisePropertyChanged(nameof(SelectedConversionProfile));
                RaisePropertyChanged(nameof(IsAnyProfileSelected));
            }
        }

        private bool _isConversionProfileEditingVisible;
        public bool IsConversionProfileEditingVisible
        {
            get => _isConversionProfileEditingVisible;
            set
            {
                _isConversionProfileEditingVisible = value;
                RaisePropertyChanged(nameof(IsConversionProfileEditingVisible));
            }
        }

        public bool IsAnyProfileSelected => SelectedConversionProfile != null;

        public ICommand AddCommand { get; private set; }
        public ICommand EditCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }

        private List<ConversionProfileData> _conversionProfileSettings;
        public List<ConversionProfileData> ConversionProfileSettings
        {
            get => _conversionProfileSettings;
            set
            {
                _conversionProfileSettings = value;

                _updateConversionProfileSettings = false;
                UpdateConversionProfilesFromSettings();
                _updateConversionProfileSettings = true;
            }
        }

        public List<ConversionProfile> ConversionProfilesAsList { get; private set; }

        public ConversionProfileListViewModel(IApplicationSettings applicationSettings, IDialogService dialogService, IConversionProfileService conversionProfileService, IConversionContextProvider conversionContextProvider, IMediaFileDataProvider mediaFileDataProvider, IOptionsProvider optionsProvider) 
        {
            _applicationSettings = applicationSettings;
            _dialogService = dialogService;
            _conversionProfileService = conversionProfileService;
            _conversionContextProvider = conversionContextProvider;
            _mediaFileDataProvider = mediaFileDataProvider;
            _optionsProvider = optionsProvider;

            ConversionProfilesAsList = new List<ConversionProfile>();
            ConversionProfiles = new ObservableCollection<ConversionProfile>();
            ConversionProfiles.CollectionChanged += ConversionProfilesChanged;

            LoadFromSettings();
            _updateConversionProfileSettings = true;

            AddCommand = new RelayCommand(Add);
            EditCommand = new RelayCommand(Edit);
            RemoveCommand = new RelayCommand(Remove);
        }

        private void LoadFromSettings()
        {
            ConversionProfileSettings = _applicationSettings.ConversionProfiles;
            
            foreach(var profile in ConversionProfiles) 
            {
                if(profile.Guid == _applicationSettings.SelectedConversionProfile)
                {
                    SelectedConversionProfile = profile;
                    break;
                }
            }
        }

        private void ConversionProfilesChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            if(_updateConversionProfileSettings)
            {
                UpdateConversionProfileSettings();
            }

            ConversionProfilesAsList.Clear();
            ConversionProfilesAsList.AddRange(ConversionProfiles);
            _conversionContextProvider.UpdateConversionProfiles(ConversionProfiles);
            _optionsProvider.LoadCenversionProfileOptions();
            RaiseOptionsChanged();
        }

        private void RaiseOptionsChanged()
        {
            OptionsChangedBehaviour?.Invoke();
        }

        private void UpdateConversionProfileSettings()
        {
            ConversionProfileSettings.Clear();

            for (int i = 0; i < ConversionProfiles.Count; i++)
            {
                var filters = new List<ConversionFilterOption>();
                foreach (var filter in ConversionProfiles[i].Filters)
                {
                    filters.Add(new() { Filter = filter.Filter, Parameter = filter.Parameter, });
                }

                var codecs = new List<CodecOption>();
                foreach(var codec in ConversionProfiles[i].Codecs)
                {
                    codecs.Add(new() { Codec = codec.Codec, Parameter = codec.Parameter, });
                }

                var conversionProfileSetting = new ConversionProfileData()
                {
                    Guid = ConversionProfiles[i].Guid,
                    Name = ConversionProfiles[i].Name,
                    Filters = filters,
                    Codecs = codecs,
                    SourceFormat = ConversionProfiles[i].Source.Format,
                    TargetFormat = ConversionProfiles[i].Target.Format,
                };

                ConversionProfileSettings.Add(conversionProfileSetting);
            }
        }

        private void UpdateConversionProfilesFromSettings()
        {
            ConversionProfiles.Clear();

            foreach(var setting in ConversionProfileSettings)
            {
                var profile = new ConversionProfile()
                {
                    Guid = setting.Guid,
                    Name = setting.Name,
                    Source = _mediaFileDataProvider.ProvideFormatEntry(setting.SourceFormat),
                    Target = _mediaFileDataProvider.ProvideFormatEntry(setting.TargetFormat),
                    Filters = _mediaFileDataProvider.ProvideFilterEntries(setting.Filters),
                    Codecs = _mediaFileDataProvider.ProvideCodecEntries(setting.Codecs),
                };

                ConversionProfiles.Add(profile);
            }   
        }

        private void RemoveSelectedConversionProfile()
        {
            ConversionProfiles.Remove(SelectedConversionProfile);
            SelectedConversionProfile = null;
        }

        private void Remove()
        {
            if(!_applicationSettings.WarnAboutConversionProfileRemoval)
            {
                RemoveSelectedConversionProfile();
                return;
            }

            var neverAskAgainOption = new AdditionalOption() { Checked = false, Name = "Never ask again" };
            var result = _dialogService.ShowDecisionDialog("Conversion Profile Removal", $"The selected conversion profile ({SelectedConversionProfile}) shall be removed.\n\nDo you want to proceed?", neverAskAgainOption);

            if (result.DialogResult == Common.Dialog.Enum.DialogResult.Yes)
            {
                RemoveSelectedConversionProfile();
            }
        }

        private void Edit()
        {
            var profileEditingResult = _conversionProfileService.ShowEditDialog(SelectedConversionProfile, ConversionProfiles);

            if(profileEditingResult.Decision == ConversionProfileDialogDecision.Finish)
            {
                SelectedConversionProfile.Name = profileEditingResult.Data.Name;
                SelectedConversionProfile.Source = profileEditingResult.Data.Source;
                SelectedConversionProfile.Target = profileEditingResult.Data.Target;
                SelectedConversionProfile.Filters = profileEditingResult.Data.Filters;
                SelectedConversionProfile.DisableAudio = profileEditingResult.Data.DisableAudio;
                SelectedConversionProfile.DisableVideo = profileEditingResult.Data.DisableVideo;
                SelectedConversionProfile.Codecs = profileEditingResult.Data.Codecs;
            }

            ConversionProfilesChanged(null, null);
            RaiseConversionProfileSettingsChanged();
        }

        private void RaiseConversionProfileSettingsChanged()
        {
            RaisePropertyChanged(nameof(ConversionProfiles));

            foreach(var profile in ConversionProfileSettings)
            {
                RaisePropertyChanged(nameof(profile.Name));
                RaisePropertyChanged(nameof(profile.SourceFormat));
                RaisePropertyChanged(nameof(profile.TargetFormat));
                RaisePropertyChanged(nameof(profile.Filters));
                RaisePropertyChanged(nameof(profile.Codecs));
            }
        }

        private void Add()
        {
            var profileAdditionResult = _conversionProfileService.ShowAddDialog(SelectedConversionProfile, ConversionProfiles);

            if (profileAdditionResult.Decision == ConversionProfileDialogDecision.Finish)
            {
                SelectedConversionProfile = null;
                
                var newProfile = new ConversionProfile()
                {
                    Guid = profileAdditionResult.Data.Guid,
                    Name = profileAdditionResult.Data.Name,
                    Source = profileAdditionResult.Data.Source,
                    Target = profileAdditionResult.Data.Target,
                    Filters = profileAdditionResult.Data.Filters,
                    DisableAudio = profileAdditionResult.Data.DisableAudio,
                    DisableVideo = profileAdditionResult.Data.DisableVideo,
                    Codecs = profileAdditionResult.Data.Codecs,
                };

                ConversionProfiles.Add(newProfile);
                SelectedConversionProfile = newProfile;
            }
        }
    }
}
