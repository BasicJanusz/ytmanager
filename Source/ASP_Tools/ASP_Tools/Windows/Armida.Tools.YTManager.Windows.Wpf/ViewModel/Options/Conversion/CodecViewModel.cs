﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion
{
    public class CodecViewModel : CoreViewModel
    {
        public event Action<Codec> RemoveBehaviour;

        private readonly CodecEntry _entry;

        public Codec Codec => _entry.Codec;
        public CodecType Type => _entry.Type;
        public CodecEntry Entry => _entry;
        public string Name => _entry.Name;
        public string Description => _entry.Description;
        public string Parameter
        {
            get => _entry.Parameter;
            set
            {
                _entry.Parameter = value;
                RaisePropertyChanged(nameof(Parameter));
            }
        }

        public ICommand RemoveCommand { get; }

        public CodecViewModel(CodecEntry entry)
        {
            _entry = entry;
            RemoveCommand = new RelayCommand(RemoveCodec);

            RaisePropertiesChanged();
        }

        private void RaisePropertiesChanged()
        {
            RaisePropertyChanged(nameof(Codec));
            RaisePropertyChanged(nameof(Type));
            RaisePropertyChanged(nameof(Entry));
            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Description));
            RaisePropertyChanged(nameof(Parameter));
        }

        private void RemoveCodec()
        {
            RemoveBehaviour?.Invoke(Codec);
        }
    }
}
