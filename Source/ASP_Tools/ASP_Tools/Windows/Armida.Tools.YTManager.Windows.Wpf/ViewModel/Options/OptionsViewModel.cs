﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Utils;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options.Conversion;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options
{
    public class OptionsViewModel : CoreViewModel
    {
        public event Action OptionsChanged;

        private List<Quality> _qualityOptions = new()
        {
            Quality.Lowest,
            Quality.Low,
            Quality.Medium,
            Quality.High,
            Quality.Higher,
            Quality.Ultra,
        };

        private readonly IApplicationSettings _applicationSettings;
        private readonly IDialogService _dialogService;
        private readonly IOptionsProvider _optionsProvider;

        private bool _areOptionsChanged;
        private string _downloadDirectory;
        private string _convertDirectory;
        private string _authenticationDirectory;
        private bool _addVideosFromPreviousSession;
        private bool _dontShowSessionLoadResults;
        private RemoveOption _selectedRemoveOption;
        private bool _useWindowParametersFromPreviousSession;
        private double _windowWidth;
        private double _windowHeight;
        private WindowStateOption _selectedWindowState;
        private int _scrollSpeed;
        private bool _allowPlaylists;
        private bool _allowRadio;
        private bool _autoLoad;
        private bool _autoDownload;
        private bool _autoConvert;
        private bool _warnAboutConversionProfileRemoval;
        private Quality _selectedQualityOption;
        private ConversionProfile _selectedConversionProfile;
        private DownloadModeOption _selectedDownloadModeOption;
        private DownloadFormatOption _selectedDownloadFormatOption;
        private PlayNonExistantVideoOption _selectedPlayNonExistantVideoOption;
        private bool _warnAboutNonExistingMedia;
        private VideoParameterBehaviourOption _selectedVideoParameterBehaviourOption;

        public bool AreOptionsChanged 
        {
            get => _areOptionsChanged;
            set
            {
                _areOptionsChanged = value;
                OptionsChanged?.Invoke();
                RaisePropertyChanged(nameof(AreOptionsChanged));
            }
        }

        public string DownloadDirectory
        {
            get => _downloadDirectory;
            set
            {
                _downloadDirectory = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(DownloadDirectory));
            }
        }
        public string ConvertDirectory
        {
            get => _convertDirectory;
            set
            {
                _convertDirectory = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(ConvertDirectory));
            }
        }
        public string AuthenticationDirectory
        {
            get => _authenticationDirectory;
            set
            {
                _authenticationDirectory = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(AuthenticationDirectory));
            }
        }

        public bool AddVideosFromPreviousSession
        {
            get => _addVideosFromPreviousSession;
            set
            {
                _addVideosFromPreviousSession = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(AddVideosFromPreviousSession));
            }
        }

        public bool DontShowSessionLoadResults
        {
            get => _dontShowSessionLoadResults;
            set
            {
                _dontShowSessionLoadResults = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(DontShowSessionLoadResults));
            }
        }

        public List<RemoveOption> RemoveOptions { get; private set; }
        public RemoveOption SelectedRemoveOption
        {
            get => _selectedRemoveOption;
            set
            {
                _selectedRemoveOption = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(SelectedRemoveOption));
            }
        }

        public bool UseWindowParametersFromPreviousSession
        {
            get => _useWindowParametersFromPreviousSession;
            set
            {
                _useWindowParametersFromPreviousSession = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(UseWindowParametersFromPreviousSession));
                RaisePropertyChanged(nameof(UseStaticWindowParameters));
            }
        }
        public bool UseStaticWindowParameters => !UseWindowParametersFromPreviousSession;
        public double WindowWidth
        {
            get => _windowWidth;
            set
            {
                _windowWidth = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(WindowWidth));
            }
        }
        public double WindowHeight
        {
            get => _windowHeight;
            set
            {
                _windowHeight = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(WindowHeight));
            }
        }
        public List<WindowStateOption> WindowStateOptions { get; private set; }
        public WindowStateOption SelectedWindowState
        {
            get => _selectedWindowState;
            set
            {
                _selectedWindowState = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(SelectedWindowState));
            }
        }
        public int ScrollSpeed
        {
            get => _scrollSpeed;
            set
            {
                _scrollSpeed = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(ScrollSpeed));
            }
        }

        public bool AllowPlaylists
        {
            get => _allowPlaylists;
            set
            {
                _allowPlaylists = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(AllowPlaylists));
            }
        }
        public bool AllowRadio
        {
            get => _allowRadio;
            set
            {
                _allowRadio = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(AllowRadio));
            }
        }
        public List<PlayNonExistantVideoOption> PlayNonExistantVideoOptions { get; private set; }
        public PlayNonExistantVideoOption SelectedPlayNonExistantVideoOption
        {
            get => _selectedPlayNonExistantVideoOption;
            set
            {
                _selectedPlayNonExistantVideoOption = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(SelectedPlayNonExistantVideoOption));
            }
        }
        public bool WarnAboutNonExistingMedia
        {
            get => _warnAboutNonExistingMedia;
            set
            {
                _warnAboutNonExistingMedia = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(WarnAboutNonExistingMedia));
            }
        }
        public List<VideoParameterBehaviourOption> VideoParameterBehaviourOptions { get; private set; }
        public VideoParameterBehaviourOption SelectedVideoParameterBehaviourOption
        {
            get => _selectedVideoParameterBehaviourOption;
            set
            {
                _selectedVideoParameterBehaviourOption = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(SelectedVideoParameterBehaviourOption));
            }
        }
        public bool AutoLoad
        {
            get => _autoLoad;
            set
            {
                _autoLoad = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(AutoLoad));
            }
        }
        public bool AutoDownload
        {
            get => _autoDownload;
            set
            {
                _autoDownload = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(AutoDownload));
            }
        }
        public bool AutoConvert
        {
            get => _autoConvert;
            set
            {
                _autoConvert = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(AutoConvert));
            }
        }

        public bool WarnAboutConversionProfileRemoval
        {
            get => _warnAboutConversionProfileRemoval;
            set
            {
                _warnAboutConversionProfileRemoval = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(WarnAboutConversionProfileRemoval));
            }
        }

        public ObservableCollection<ConversionProfile> ConversionProfiles { get; private set; }
        public ConversionProfile SelectedConversionProfile
        {
            get => _selectedConversionProfile; 
            set
            {
                _selectedConversionProfile = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(SelectedConversionProfile));
            }
        }

        public List<Quality> QualityOptions => _qualityOptions;
        public Quality SelectedQualityOption
        {
            get => _selectedQualityOption;
            set
            {
                _selectedQualityOption = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(SelectedQualityOption));
            }
        }

        public ConversionProfileListViewModel ConversionProfileListViewModel { get; private set; }

        public List<DownloadModeOption> DownloadModeOptions {get; private set; }
        public DownloadModeOption SelectedDownloadModeOption
        {
            get => _selectedDownloadModeOption;
            set
            {
                _selectedDownloadModeOption = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(SelectedDownloadModeOption));
            }
        }
        public List<DownloadFormatOption> DownloadFormatOptions { get; private set; }
        public DownloadFormatOption SelectedDownloadFormatOption
        {
            get => _selectedDownloadFormatOption;
            set
            {
                _selectedDownloadFormatOption = value;
                AreOptionsChanged = true;
                RaisePropertyChanged(nameof(SelectedDownloadFormatOption));
            }
        }

        public ICommand SaveCommand { get; }
        public ICommand UndoCommand { get; }
        public ICommand DefaultCommand { get; }
        public ICommand BrowseDownloadDirectoryCommand { get; }
        public ICommand BrowseConvertDirectoryCommand { get; }
        public ICommand BrowseAuthenticationDirectoryCommand { get; }
        public ICommand ConversionProfileEditCommand { get; }

        public OptionsViewModel(IApplicationSettings applicationSettings, IDialogService dialogService, IOptionsProvider optionsProvider, ConversionProfileListViewModel conversionProfileListViewModel)
        {
            _applicationSettings = applicationSettings;
            _dialogService = dialogService;
            _optionsProvider = optionsProvider;

            SaveCommand = new RelayCommand(Save);
            UndoCommand = new RelayCommand(Undo);
            DefaultCommand = new RelayCommand(Default);
            BrowseDownloadDirectoryCommand = new RelayCommand(BrowseDownloadDirectory);
            BrowseConvertDirectoryCommand = new RelayCommand(BrowseConvertDirectory);
            BrowseAuthenticationDirectoryCommand = new RelayCommand(BrowseAuthenticationDirectory);
            ConversionProfileEditCommand = new RelayCommand(ConversionProfileEdit);

            ConversionProfileListViewModel = conversionProfileListViewModel;
            ConversionProfileListViewModel.OptionsChangedBehaviour += () => AreOptionsChanged = true;
            ConversionProfiles = ConversionProfileListViewModel.ConversionProfiles;
            ConversionProfiles.CollectionChanged += ConversionProfilesChanged;

            LoadAvailableOptions();
            LoadSelectedOptionsFromSettings();
        }

        private void ConversionProfilesChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            AreOptionsChanged = true;
            RaisePropertyChanged(nameof(ConversionProfiles));
        }

        public void Save()
        {
            if(!AreSettingsValid(out var errorMessage))
            {
                _dialogService.ShowDialog("Settings validation error", errorMessage);
                return;
            }

            _applicationSettings.DownloadDirectory = DownloadDirectory;
            _applicationSettings.ConvertDirectory = ConvertDirectory;
            _applicationSettings.AuthenticationDirectory = AuthenticationDirectory;
            _applicationSettings.AddVideosFromPreviousSession = AddVideosFromPreviousSession;
            _applicationSettings.DontShowSessionLoadResults = DontShowSessionLoadResults;
            //_applicationSettings.AddVideosFromDownloadFolder = AddVideosFromDownloadFolder;
            //_applicationSettings.AddVideosFromConvertFolder = AddVideosFromConvertFolder;
            _applicationSettings.SelectedRemoveOption = SelectedRemoveOption.Type;
            _applicationSettings.SelectedPlayNonExistantVideoBehaviour = SelectedPlayNonExistantVideoOption.Behaviour;

            _applicationSettings.UseWindowParametersFromPreviousSession = UseWindowParametersFromPreviousSession;
            _applicationSettings.WindowWidth = WindowWidth;
            _applicationSettings.WindowHeight = WindowHeight;
            _applicationSettings.WindowState = SelectedWindowState.State;
            _applicationSettings.ScrollSpeed = ScrollSpeed;

            _applicationSettings.WarnAboutConversionProfileRemoval = WarnAboutConversionProfileRemoval;
            _applicationSettings.Quality = SelectedQualityOption;
            _applicationSettings.ConversionProfiles = ConversionProfileListViewModel.ConversionProfileSettings;
            _applicationSettings.SelectedConversionProfile = SelectedConversionProfile != null ? SelectedConversionProfile.Guid : Guid.Empty;

            _applicationSettings.AllowPlaylists = AllowPlaylists;
            _applicationSettings.AllowRadio = AllowRadio;
            _applicationSettings.WarnAboutNonExistingMedia = WarnAboutNonExistingMedia;
            _applicationSettings.SelectedVideoParameterBehaviour = SelectedVideoParameterBehaviourOption.Behaviour;
            _applicationSettings.AutoLoad = AutoLoad;
            _applicationSettings.AutoDownload = AutoDownload;
            _applicationSettings.AutoConvert = AutoConvert;

            _applicationSettings.DownloadMode = SelectedDownloadModeOption.Mode;
            _applicationSettings.DownloadFormat = SelectedDownloadFormatOption.Format;

            _applicationSettings.WriteToJson();

            AreOptionsChanged = false;
        }

        public void Undo()
        {
            LoadSelectedOptionsFromSettings();
        }

        public void Default()
        {
            _applicationSettings.SetDefaultSettings();
            LoadSelectedOptionsFromSettings();
        }

        public void BrowseDownloadDirectory()
        {
            var result = _dialogService.ShowDirectoryDialog("Select Download Directory", "", DownloadDirectory);
            if (result.DialogResult != DialogResult.SelectedDirectory)
            {
                return;
            }

            DownloadDirectory = result.Data;
        }

        public void BrowseConvertDirectory()
        {
            var result = _dialogService.ShowDirectoryDialog("Select Convert Directory", "", ConvertDirectory);
            if (result.DialogResult != DialogResult.SelectedDirectory)
            {
                return;
            }

            ConvertDirectory = result.Data;
        }

        public void BrowseAuthenticationDirectory()
        {
            var result = _dialogService.ShowDirectoryDialog("Select Authentication Directory", "", AuthenticationDirectory);
            if (result.DialogResult != DialogResult.SelectedDirectory)
            {
                return;
            }

            AuthenticationDirectory = result.Data;
        }

        public void LoadSelectedOptionsFromSettings()
        {
            DownloadDirectory = _applicationSettings.DownloadDirectory;
            ConvertDirectory = _applicationSettings.ConvertDirectory;
            AuthenticationDirectory = _applicationSettings.AuthenticationDirectory;
            AddVideosFromPreviousSession = _applicationSettings.AddVideosFromPreviousSession;
            DontShowSessionLoadResults = _applicationSettings.DontShowSessionLoadResults;
            //_applicationSettings.AddVideosFromDownloadFolder = AddVideosFromDownloadFolder;
            //_applicationSettings.AddVideosFromConvertFolder = AddVideosFromConvertFolder;
            SelectedRemoveOption = RemoveOptions[(int)_applicationSettings.SelectedRemoveOption];
            SelectedPlayNonExistantVideoOption = PlayNonExistantVideoOptions[(int)_applicationSettings.SelectedPlayNonExistantVideoBehaviour];

            UseWindowParametersFromPreviousSession = _applicationSettings.UseWindowParametersFromPreviousSession;
            WindowWidth = _applicationSettings.WindowWidth;
            WindowHeight = _applicationSettings.WindowHeight;
            SelectedWindowState = WindowStateOptions[(int)_applicationSettings.WindowState];
            ScrollSpeed = _applicationSettings.ScrollSpeed;

            WarnAboutConversionProfileRemoval = _applicationSettings.WarnAboutConversionProfileRemoval;
            SelectedQualityOption = _applicationSettings.Quality;
            ConversionProfileListViewModel.ConversionProfileSettings = _applicationSettings.ConversionProfiles;
            if(ConversionProfiles != null)
            {
                foreach (var profile in ConversionProfiles)
                {
                    if (_applicationSettings.SelectedConversionProfile == profile.Guid)
                    {
                        SelectedConversionProfile = profile;
                        break;
                    }
                }
            }

            AllowPlaylists = _applicationSettings.AllowPlaylists;
            AllowRadio = _applicationSettings.AllowRadio;
            WarnAboutNonExistingMedia = _applicationSettings.WarnAboutNonExistingMedia;
            SelectedVideoParameterBehaviourOption = VideoParameterBehaviourOptions[(int)_applicationSettings.SelectedVideoParameterBehaviour];
            AutoLoad = _applicationSettings.AutoLoad;
            AutoDownload = _applicationSettings.AutoDownload;
            AutoConvert = _applicationSettings.AutoConvert;

            SelectedDownloadModeOption = DownloadModeOptions[(int)_applicationSettings.DownloadMode];
            SelectedDownloadFormatOption = DownloadFormatOptions[(int)_applicationSettings.DownloadFormat];

            RefreshProperties();
            AreOptionsChanged = false;
        }

        public void ConversionProfileEdit()
        {
            ConversionProfileListViewModel.IsConversionProfileEditingVisible = !ConversionProfileListViewModel.IsConversionProfileEditingVisible;
        }

        public void RefreshProperties()
        {
            RaisePropertyChanged(nameof(ConvertDirectory));
            RaisePropertyChanged(nameof(DownloadDirectory));
            RaisePropertyChanged(nameof(AuthenticationDirectory));
            RaisePropertyChanged(nameof(AddVideosFromPreviousSession));
            RaisePropertyChanged(nameof(DontShowSessionLoadResults));
            //RaisePropertyChanged(nameof(AddVideosFromDownloadFolder));
            //RaisePropertyChanged(nameof(AddVideosFromConvertFolder));
            RaisePropertyChanged(nameof(RemoveOptions));
            RaisePropertyChanged(nameof(SelectedRemoveOption));
            RaisePropertyChanged(nameof(SelectedPlayNonExistantVideoOption));

            RaisePropertyChanged(nameof(UseWindowParametersFromPreviousSession));
            RaisePropertyChanged(nameof(WindowWidth));
            RaisePropertyChanged(nameof(WindowHeight));
            RaisePropertyChanged(nameof(SelectedWindowState));
            RaisePropertyChanged(nameof(ScrollSpeed));

            RaisePropertyChanged(nameof(WarnAboutConversionProfileRemoval));
            RaisePropertyChanged(nameof(ConversionProfiles));
            RaisePropertyChanged(nameof(SelectedConversionProfile));
            RaisePropertyChanged(nameof(SelectedQualityOption));
            
            RaisePropertyChanged(nameof(AllowPlaylists));
            RaisePropertyChanged(nameof(AllowRadio));
            RaisePropertyChanged(nameof(WarnAboutNonExistingMedia));
            RaisePropertyChanged(nameof(SelectedVideoParameterBehaviourOption));
            RaisePropertyChanged(nameof(AutoLoad));
            RaisePropertyChanged(nameof(AutoDownload));
            RaisePropertyChanged(nameof(AutoConvert));

            RaisePropertyChanged(nameof(SelectedDownloadModeOption));
            RaisePropertyChanged(nameof(SelectedDownloadFormatOption));
        }

        private void LoadAvailableOptions()
        {
            RemoveOptions = _optionsProvider.RemoveOptions;
            WindowStateOptions = _optionsProvider.WindowStateOptions;
            DownloadFormatOptions = _optionsProvider.DownloadFormatOptions;
            DownloadModeOptions = _optionsProvider.DownloadModeOptions;
            PlayNonExistantVideoOptions = _optionsProvider.PlayNonExistantVideoOptions;
            VideoParameterBehaviourOptions = _optionsProvider.VideoParameterBehaviourOptions;
        }

        private bool AreSettingsValid(out string errorMessage)
        {
            var messageBuilder = new StringBuilder();
            var result = true;

            if(string.IsNullOrWhiteSpace(DownloadDirectory))
            {
                messageBuilder.AppendLine("DownloadDirectory cannot be empty.");
                result = false;
            }

            if (string.IsNullOrWhiteSpace(ConvertDirectory))
            {
                messageBuilder.AppendLine("ConvertDirectory cannot be empty.");
                result = false;
            }

            if (string.IsNullOrWhiteSpace(AuthenticationDirectory))
            {
                messageBuilder.AppendLine("AuthenticationDirectory cannot be empty.");
                result = false;
            }

            if (string.Equals(ConvertDirectory, DownloadDirectory) || 
                string.Equals(ConvertDirectory, AuthenticationDirectory) || 
                string.Equals(AuthenticationDirectory, DownloadDirectory))
            {
                messageBuilder.AppendLine("ConvertDirectory, DownloadDirectory and AuthenticationDirectory cannot be the same.");
                result = false;
            }

            if(WindowHeight <= 0)
            {
                messageBuilder.AppendLine("WindowHeight has to be greater than 0.");
                result = false;
            }

            if (WindowWidth <= 0)
            {
                messageBuilder.AppendLine("WindowWidth has to be greater than 0.");
                result = false;
            }

            errorMessage = messageBuilder.ToString();
            return result;
        }
    }
}
