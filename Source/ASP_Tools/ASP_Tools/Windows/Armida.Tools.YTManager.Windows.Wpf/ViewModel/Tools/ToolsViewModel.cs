﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Converter;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Downloader;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Model;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools
{
    public class ToolsViewModel : CoreViewModel
    {
        private readonly IApplicationSettings _applicationSettings;
        private readonly IDialogService _dialogService;
        private readonly IOptionsProvider _optionsProvider;

        private bool _areToolsVisible;
        public bool AreToolsVisible
        {
            get => _areToolsVisible;
            set
            {
                _areToolsVisible = value;
                RaisePropertyChanged(nameof(AreToolsVisible));
            }
        }

        public DownloaderViewModel Downloader { get; }
        public ConverterViewModel Converter { get; }
        public OptionsViewModel Options { get; }

        public ToolsViewModel(DownloaderViewModel downloaderViewModel, ConverterViewModel converterViewModel, OptionsViewModel optionsViewModel, IApplicationSettings applicationSettings, IDialogService dialogService, IOptionsProvider optionsProvider) 
        {
            _applicationSettings = applicationSettings;
            _dialogService = dialogService;
            _optionsProvider = optionsProvider;

            Downloader = downloaderViewModel;
            Converter = converterViewModel;
            Options = optionsViewModel;

            AreToolsVisible = true;
        }

        public void HandleYoutubePage(YTPage page)
        {
            if(page.PageType == PageType.Video)
            {
                ConversionProfileOption conversionProfile = ConversionProfileOption.Empty;
                foreach (var profile in _optionsProvider.ConversionProfileOptions)
                {
                    if (_applicationSettings.SelectedConversionProfile == profile.Guid)
                    {
                        conversionProfile = profile;
                    }
                }

                var video = new YTVideo()
                {
                    Url = page.Url,
                    Id = page.Id,
                    State = VideoState.Created,
                    AutoLoad = _applicationSettings.AutoLoad,
                    AutoDownload = _applicationSettings.AutoDownload,
                    AutoConvert = _applicationSettings.AutoConvert,
                    Format = _applicationSettings.DownloadFormat,
                    DownloadMode = _applicationSettings.DownloadMode,
                    Quality = _applicationSettings.Quality,
                    ConversionProfile = conversionProfile,
                };

                var viewmodel = Downloader.CreateYoutubeVideo(video);

                if (viewmodel.AutoLoad)
                {
                    viewmodel.Load();
                }
            }

            if(page.PageType == PageType.Playlist)
            {
                if (_applicationSettings != null &&
                    !_applicationSettings.AllowPlaylists)
                {
                    _dialogService.ShowDialog($"Ignoring Playlist", $"Entered url seems to be a YouTube Playlist. It shall be ignored due to set option.\n{page.Url}\n\nIf you want to unset this go to Options->Video List.");
                    return;
                }

                var playlist = new YTPlaylist()
                {
                    Url = page.Url,
                    Id = page.Id,
                };

                _ = Downloader.CreateYoutubePLaylist(playlist);
            }

            if(page.PageType == PageType.Radio)
            {
                if (_applicationSettings != null &&
                    !_applicationSettings.AllowRadio)
                {
                    _dialogService.ShowDialog($"Ignoring Radio", $"Entered url seems to be a YouTube Radio. It shall be ignored due to set option.\n{page.Url}\n\nIf you want to unset this go to Options->Video List.");
                    return;
                }

                var radio = new YTRadio()
                {
                    Url = page.Url,
                    Id = page.Id,
                };

                Downloader.CreateYoutubeRadio(radio);
            }
        }

        public void HandleFile(string filePath)
        {
            Converter.CreateFile(filePath);
        }
    }
}
