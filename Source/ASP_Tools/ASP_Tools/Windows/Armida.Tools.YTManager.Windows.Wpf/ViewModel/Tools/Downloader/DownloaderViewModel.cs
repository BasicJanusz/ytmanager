﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Model;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Service;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Downloader
{
    public class DownloaderViewModel : CoreViewModel
    {
        private readonly IApplicationSettings _applicationSettings;
        private readonly IDialogService _dialogService;
        private readonly IYoutubeService _youtubeService;
        private readonly IConversionService _conversionService;
        private readonly IOptionsProvider _optionsProvider;

        public VideoListViewModel VideoListViewModel { get; }
        public bool IsAnyVideoLoaded => VideoListViewModel.Videos.Count > 0;

        public DownloaderViewModel(VideoListViewModel videoLisViewModel, IYoutubeService youtubeService, IConversionService conversionService, IApplicationSettings applicationSettings, IDialogService dialogService, IOptionsProvider optionsProvider)
        {
            _applicationSettings = applicationSettings;
            VideoListViewModel = videoLisViewModel;
            _youtubeService = youtubeService;
            _conversionService = conversionService;
            _dialogService = dialogService;
            _optionsProvider = optionsProvider;

            VideoListViewModel.Videos.CollectionChanged += (a, b) => RaisePropertyChanged(nameof(IsAnyVideoLoaded));

            LoadDataFromPreviousSession();
            RaisePropertyChanged(nameof(IsAnyVideoLoaded));
        }

        private void LoadDataFromPreviousSession()
        {
            if (_applicationSettings == null ||
                !_applicationSettings.AddVideosFromPreviousSession ||
                _applicationSettings.Videos == null)
                return;

            var loadMessages = new StringBuilder();

            foreach (var video in _applicationSettings.Videos)
            {
                var viewModel = CreateYoutubeVideo(video, false);

                if (string.IsNullOrWhiteSpace(video.Url))
                {
                    // not created
                    VideoListViewModel?.Videos.Remove(viewModel);
                    loadMessages.AppendLine(" * Missing url, the video has not been added to the list");
                    loadMessages.AppendLine();

                    continue;
                }

                if (string.IsNullOrWhiteSpace(video.Id) ||
                    string.IsNullOrWhiteSpace(video.Title) ||
                    string.IsNullOrWhiteSpace(video.FullName))
                {
                    // not loaded
                    loadMessages.AppendLine($" * {video.Url}");
                    loadMessages.AppendLine($"   Created, can be Loaded");
                    loadMessages.AppendLine();

                    video.State = VideoState.Created;
                    viewModel.StateChanged();
                    continue;
                }

                if (string.IsNullOrWhiteSpace(video.DownloadedFilePath))
                {
                    // not downloaded
                    loadMessages.AppendLine($" * [{video.Id}] {video.Title}");
                    loadMessages.AppendLine($"   {video.Url}");
                    loadMessages.AppendLine($"   Loaded, can be Downloaded");
                    loadMessages.AppendLine();

                    video.State = VideoState.Loaded;
                    viewModel.StateChanged();
                    continue;
                }

                if (!File.Exists(video.DownloadedFilePath))
                {
                    // downloaded but file deleted / moved
                    loadMessages.AppendLine($" * [{video.Id}] {video.Title}");
                    loadMessages.AppendLine($"   {video.Url}");
                    loadMessages.AppendLine($"   Missing Downloaded file");
                    loadMessages.AppendLine($"   Loaded, can be Downloaded");
                    loadMessages.AppendLine();

                    video.State = VideoState.Loaded;
                    viewModel.StateChanged();
                    continue;
                }

                if (string.IsNullOrWhiteSpace(video.ConvertedFilePath))
                {
                    // not converted
                    loadMessages.AppendLine($" * [{video.Id}] {video.Title}");
                    loadMessages.AppendLine($"   {video.Url}");
                    loadMessages.AppendLine($"   {video.DownloadedFilePath}");
                    loadMessages.AppendLine($"   Downloaded, can be Converted");
                    loadMessages.AppendLine();

                    video.State = VideoState.Downloaded;
                    viewModel.StateChanged();
                    continue;
                }

                if (!File.Exists(video.ConvertedFilePath))
                {
                    // converted but file deleted / moved
                    loadMessages.AppendLine($" * [{video.Id}] {video.Title}");
                    loadMessages.AppendLine($"   {video.Url}");
                    loadMessages.AppendLine($"   {video.DownloadedFilePath}");
                    loadMessages.AppendLine($"   Missing Converted file");
                    loadMessages.AppendLine($"   Downloaded, can be Converted");
                    loadMessages.AppendLine();

                    video.State = VideoState.Downloaded;
                    viewModel.StateChanged();
                    continue;
                }

                loadMessages.AppendLine($" * [{video.Id}] {video.Title}");
                loadMessages.AppendLine($"   {video.Url}");
                loadMessages.AppendLine($"   {video.DownloadedFilePath}");
                loadMessages.AppendLine($"   {video.ConvertedFilePath}");
                loadMessages.AppendLine();
                video.State = VideoState.Converted;
                viewModel.StateChanged();
            }

            var messages = loadMessages.ToString();

            if (!_applicationSettings.DontShowSessionLoadResults &&
                !string.IsNullOrWhiteSpace(messages))
            {
                var additionalOptions = new AdditionalOption[]
                {
                    new()
                    {
                        Name = "Don't show this again",
                        Checked = _applicationSettings.DontShowSessionLoadResults
                    }
                };

                var result = _dialogService.ShowDialog("Videos from previous session", $"Videos from previous session have been processed with following results:\n{messages}", additionalOptions);

                if (result.AdditionalOptions.Count > 0 && _applicationSettings.DontShowSessionLoadResults != result.AdditionalOptions[0].Checked)
                {
                    _applicationSettings.DontShowSessionLoadResults = additionalOptions[0].Checked;
                }
            }
        }

        internal VideoItemViewModel CreateYoutubeVideo(YTVideo video, bool changeStateOnCreation = true)
        {
            var viewModel = new VideoItemViewModel(video, _youtubeService, _conversionService, _applicationSettings, _dialogService, _optionsProvider);
            viewModel.RaiseVideoRemoved += (viewModel) => VideoListViewModel.Videos.Remove(viewModel);

            if(changeStateOnCreation)
            {
                viewModel.Data.State = VideoState.Created;
            }

            VideoListViewModel.Videos.Add(viewModel);
            return viewModel;
        }

        internal async Task CreateYoutubePLaylist(YTPlaylist playlist)
        {
            var videos = await _youtubeService.GetVideosFromPLaylistAsync(playlist);

            foreach (var video in videos)
            {
                var viewmodel = CreateYoutubeVideo(video);

                if (viewmodel.AutoLoad)
                {
                    viewmodel.Load();
                }
            }
        }

        public void CreateYoutubeRadio(YTRadio radio)
        {
            var videos = _youtubeService.GetVideosFromRadio(radio);

            foreach (var video in videos)
            {
                var viewmodel = CreateYoutubeVideo(video);

                if (_applicationSettings.AutoLoad)
                {
                    viewmodel.Load();
                }
            }
        }

        public void SaveVideosToSettings()
        {
            var videos = new List<YTVideo>();

            foreach (var viewModel in VideoListViewModel.Videos)
            {
                videos.Add(viewModel.Data);
            }

            _applicationSettings.Videos = videos;
        }
    }
}
