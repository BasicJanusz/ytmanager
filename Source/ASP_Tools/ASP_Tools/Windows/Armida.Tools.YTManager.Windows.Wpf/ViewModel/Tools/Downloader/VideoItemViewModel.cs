﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Model;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Model;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Service;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Downloader
{
    public class VideoItemViewModel : CoreViewModel
    {
        public event Action<VideoItemViewModel> RaiseVideoRemoved;

        private readonly IYoutubeService _youtubeService;
        private readonly IConversionService _conversionService;
        private readonly IApplicationSettings _applicationSettings;
        private readonly IDialogService _dialogService;
        private readonly IOptionsProvider _optionsProvider;
        private bool _isSelected;
        private bool _isBusy;
        private bool _isDownloadable;
        private bool _isConvertable;
        private bool _isLoadable;
        private bool _isRemovable;
        private bool _isPlayableForDownloaded;
        private bool _isPlayableForConverted;

        public YTVideo Data { get; }

        public string Id => Data.Id;
        public string Title => Data.Title;
        public string Url => Data.Url;

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                RaisePropertyChanged(nameof(IsSelected));
            }
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                RaisePropertyChanged(nameof(IsBusy));
            }
        }

        public bool IsDownloadable
        {
            get => _isDownloadable;
            set
            {
                _isDownloadable = value;
                RaisePropertyChanged(nameof(IsDownloadable));
            }
        }

        public bool IsConvertable
        {
            get => _isConvertable;
            set
            {
                _isConvertable = value;
                RaisePropertyChanged(nameof(IsConvertable));
            }
        }

        public bool IsLoadable
        {
            get => _isLoadable;
            set
            {
                _isLoadable = value;
                RaisePropertyChanged(nameof(IsLoadable));
            }
        }

        public bool IsRemovable
        {
            get => _isRemovable;
            set
            {
                _isRemovable = value;
                RaisePropertyChanged(nameof(IsRemovable));
            }
        }

        public bool IsAnyConversionOptionSelected
        {
            get => _conversionService.IsAnyConversionOptionSelected;
        }

        public bool AutoLoad
        {
            get => Data.AutoLoad;
            set
            {
                Data.AutoLoad = value;
                RaisePropertyChanged(nameof(AutoLoad));
            }
        }
        public bool AutoDownload
        {
            get => Data.AutoDownload;
            set
            {
                Data.AutoDownload = value;
                RaisePropertyChanged(nameof(AutoDownload));
            }
        }
        public bool AutoConvert
        {
            get => Data.AutoConvert;
            set
            {
                Data.AutoConvert = value;
                RaisePropertyChanged(nameof(AutoConvert));
            }
        }
        
        public List<ConversionProfileOption> ConversionProfileOptions => _optionsProvider.ConversionProfileOptions;

        public ConversionProfileOption SelectedConversionProfileOption
        {
            get => Data.ConversionProfile;
            set
            {
                Data.ConversionProfile = value;
                RaisePropertyChanged(nameof(SelectedConversionProfileOption));
            }
        }

        private List<Quality> _qualityOptions = new()
            {
                Quality.Lowest,
                Quality.Low,
                Quality.Medium,
                Quality.High,
                Quality.Higher,
                Quality.Ultra,
            };
        public List<Quality> QualityOptions => _qualityOptions;
        public Quality SelectedQualityOption
        {
            get => Data.Quality;
            set
            {
                Data.Quality = value;
                RaisePropertyChanged(nameof(SelectedQualityOption));
            }
        }

        private List<DownloadMode> _downloadModes = new()
        {
            DownloadMode.MuxedHighestVideoQuality,
            DownloadMode.MuxedHighestBitrateQuality,
            DownloadMode.HighestPossibleQuality,
        };
        public List<DownloadMode> DownloadModes => _downloadModes;
        public DownloadMode SelectedDownloadMode
        {
            get => Data.DownloadMode;
            set
            {
                Data.DownloadMode = value;
                RaisePropertyChanged(nameof(SelectedDownloadMode));
            }
        }

        private List<DownloadFormat> _downloadFormats = new()
        {
            DownloadFormat.WebMediaFile,
            DownloadFormat.MPEG4,
            DownloadFormat.ThirdGenerationPartnershipProject,
        };
        public List<DownloadFormat> DownloadFormats => _downloadFormats;
        public DownloadFormat SelectedDownloadFormat
        {
            get => Data.Format;
            set
            {
                Data.Format = value;
                RaisePropertyChanged(nameof(SelectedDownloadFormat));
            }
        }

        public bool IsPlayableForDownloaded
        {
            get => _isPlayableForDownloaded;
            set
            {
                _isPlayableForDownloaded = value;
                RaisePropertyChanged(nameof(IsPlayableForDownloaded));
            }
        }

        public bool IsPlayableForConverted
        {
            get => _isPlayableForConverted;
            set
            {
                _isPlayableForConverted = value;
                RaisePropertyChanged(nameof(IsPlayableForConverted));
            }
        }

        public ICommand DownloadCommand { get; }
        public ICommand ConvertCommand { get; }
        public ICommand LoadCommand { get; }
        public ICommand RemoveCommand { get; }
        public ICommand PlayDownloadableCommand { get; }
        public ICommand PlayConvertedCommand { get; }


        public VideoItemViewModel(YTVideo data, IYoutubeService youtubeService, IConversionService conversionService, IApplicationSettings applicationSettings, IDialogService dialogService, IOptionsProvider optionsProvider) 
        {
            Data = data;
            Data.PropertyChanged += (obj, arg) => RaisePropertyChanged(arg.PropertyName);
            Data.StateChanged += StateChanged;

            _youtubeService = youtubeService;
            _conversionService = conversionService;
            _applicationSettings = applicationSettings;
            _dialogService = dialogService;
            _optionsProvider = optionsProvider;

            DownloadCommand = new RelayCommand(Download);
            ConvertCommand = new RelayCommand(Convert);
            LoadCommand = new RelayCommand(Load);
            RemoveCommand = new RelayCommand(Remove);
            PlayDownloadableCommand = new RelayCommand(PlayDownloaded);
            PlayConvertedCommand = new RelayCommand(PlayConverted);

            IsSelected = false;
            IsBusy = false;
            IsDownloadable = false;
            IsConvertable = false;
            IsLoadable = false;
            IsRemovable = false;
            IsPlayableForDownloaded = false;
            IsPlayableForConverted = false;

            // TODO : Unnecessary assignment, just raise property changed instead
            SelectedDownloadFormat = data.Format;
            SelectedDownloadMode = data.DownloadMode;
            SelectedQualityOption = data.Quality;
            SelectedConversionProfileOption = data.ConversionProfile;
        }

        public void Load()
        {
            IsBusy = true;
            _youtubeService.EnqueueProcess(ProcessType.Load, Data);
        }

        private void Download()
        {
            IsBusy = true;
            _youtubeService.EnqueueProcess(ProcessType.Download, Data);
        }

        private void Convert()
        {

            if (!File.Exists(Data.DownloadedFilePath))
            {
                var option = _applicationSettings.SelectedPlayNonExistantVideoBehaviour;
                switch (option)
                {
                    case PlayNonExistantVideoBehaviour.Ask:

                        var askDialogResult = _dialogService.ShowDecisionDialog(
                            "Downloaded File Missing",
                            $"The downloaded file {Data.FullName} seems to be missing at '{Data.DownloadedFilePath}'." +
                            $"\n\nDo you want to clear the file reference and revert it to the Loaded State?",
                            new AdditionalOption()
                            {
                                Name = "Don't show this again",
                                Checked = false,
                            });

                        if (askDialogResult.AdditionalOptions.Count > 0 && askDialogResult.AdditionalOptions[0].Checked)
                        {
                            _applicationSettings.SelectedPlayNonExistantVideoBehaviour = askDialogResult.DialogResult == DialogResult.Yes
                                ? PlayNonExistantVideoBehaviour.Clear
                                : PlayNonExistantVideoBehaviour.DoNothing;
                        }

                        switch (askDialogResult.DialogResult)
                        {
                            case DialogResult.Yes:
                                ClearDownloadedVideo();
                                break;

                            default:
                                break;
                        }

                        break;

                    case PlayNonExistantVideoBehaviour.Clear:

                        if (_applicationSettings.WarnAboutNonExistingMedia)
                        {
                            var clearDialogResult = _dialogService.ShowDialog(
                                "Downloaded File Missing",
                                $"The downloaded file {Data.FullName} seems to be missing at '{Data.DownloadedFilePath}'." +
                                $"\n\nThe media file shall be reverted to the Loaded State and have its file reference cleared.",
                                new AdditionalOption()
                                {
                                    Name = "Don't show this again",
                                    Checked = false,
                                });

                            if (clearDialogResult.AdditionalOptions.Count > 0 && clearDialogResult.AdditionalOptions[0].Checked)
                            {
                                _applicationSettings.WarnAboutNonExistingMedia = false;
                            }
                        }

                        ClearDownloadedVideo();
                        break;

                    case PlayNonExistantVideoBehaviour.DoNothing:

                        if (_applicationSettings.WarnAboutNonExistingMedia)
                        {
                            var clearDialogResult = _dialogService.ShowDialog(
                                "Downloaded File Missing",
                                $"The downloaded file {Data.FullName} seems to be missing at '{Data.DownloadedFilePath}'.",
                                new AdditionalOption()
                                {
                                    Name = "Don't show this again",
                                    Checked = false,
                                });

                            if (clearDialogResult.AdditionalOptions.Count > 0 && clearDialogResult.AdditionalOptions[0].Checked)
                            {
                                _applicationSettings.WarnAboutNonExistingMedia = false;
                            }
                        }

                        break;
                }

                return;
            }

            IsBusy = true;
            _youtubeService.EnqueueProcess(ProcessType.Convert, Data);
        }

        private void Remove()
        {
            IsBusy = true;
            _youtubeService.EnqueueProcess(ProcessType.Remove, Data);
            RaiseVideoRemoved?.Invoke(this);
        }

        public void PlayDownloaded()
        {
            if(!File.Exists(Data.DownloadedFilePath))
            {
                var option = _applicationSettings.SelectedPlayNonExistantVideoBehaviour;
                switch(option)
                {
                    case PlayNonExistantVideoBehaviour.Ask:
                        
                        var askDialogResult = _dialogService.ShowDecisionDialog(
                            "Downloaded File Missing", 
                            $"The downloaded file {Data.FullName} seems to be missing at '{Data.DownloadedFilePath}'." +
                            $"\n\nDo you want to clear the file reference and revert it to the Loaded State?", 
                            new AdditionalOption()
                            {
                                Name = "Don't show this again",
                                Checked = false,
                            });

                        if (askDialogResult.AdditionalOptions.Count > 0 && askDialogResult.AdditionalOptions[0].Checked)
                        {
                            _applicationSettings.SelectedPlayNonExistantVideoBehaviour = askDialogResult.DialogResult == DialogResult.Yes 
                                ? PlayNonExistantVideoBehaviour.Clear 
                                : PlayNonExistantVideoBehaviour.DoNothing;
                        }

                        switch (askDialogResult.DialogResult)
                        {
                            case DialogResult.Yes:
                                ClearDownloadedVideo();
                                break;

                            default:
                                break;
                        }

                        break;

                    case PlayNonExistantVideoBehaviour.Clear:

                        if(_applicationSettings.WarnAboutNonExistingMedia)
                        {
                            var clearDialogResult = _dialogService.ShowDialog(
                                "Downloaded File Missing", 
                                $"The downloaded file {Data.FullName} seems to be missing at '{Data.DownloadedFilePath}'." +
                                $"\n\nThe media file shall be reverted to the Loaded State and have its file reference cleared.",
                                new AdditionalOption()
                                {
                                    Name = "Don't show this again",
                                    Checked = false,
                                });

                            if (clearDialogResult.AdditionalOptions.Count > 0 && clearDialogResult.AdditionalOptions[0].Checked)
                            {
                                _applicationSettings.WarnAboutNonExistingMedia = false;
                            }
                        }

                        ClearDownloadedVideo();
                        break;

                    case PlayNonExistantVideoBehaviour.DoNothing:

                        if (_applicationSettings.WarnAboutNonExistingMedia)
                        {
                            var clearDialogResult = _dialogService.ShowDialog(
                                "Downloaded File Missing",
                                $"The downloaded file {Data.FullName} seems to be missing at '{Data.DownloadedFilePath}'.",
                                new AdditionalOption()
                                {
                                    Name = "Don't show this again",
                                    Checked = false,
                                });

                            if (clearDialogResult.AdditionalOptions.Count > 0 && clearDialogResult.AdditionalOptions[0].Checked)
                            {
                                _applicationSettings.WarnAboutNonExistingMedia = false;
                            }
                        }

                        break;
                }

                return;
            }

            using (var process = new Process())
            {
                process.StartInfo.FileName = "explorer";
                process.StartInfo.Arguments = "\"" + Data.DownloadedFilePath + "\"";
                process.Start();
            }
        }

        public void PlayConverted()
        {
            if (!File.Exists(Data.ConvertedFilePath))
            {
                var option = _applicationSettings.SelectedPlayNonExistantVideoBehaviour;
                switch (option)
                {
                    case PlayNonExistantVideoBehaviour.Ask:

                        var askDialogResult = _dialogService.ShowDecisionDialog(
                            "Converted File Missing",
                            $"The converted file {Data.FullName} seems to be missing at '{Data.ConvertedFilePath}'." +
                            $"\n\nDo you want to clear the file reference and revert it to the Loaded State?",
                            new AdditionalOption()
                            {
                                Name = "Don't show this again",
                                Checked = false,
                            });

                        if (askDialogResult.AdditionalOptions.Count > 0 && askDialogResult.AdditionalOptions[0].Checked)
                        {
                            _applicationSettings.SelectedPlayNonExistantVideoBehaviour = askDialogResult.DialogResult == DialogResult.Yes
                                ? PlayNonExistantVideoBehaviour.Clear
                                : PlayNonExistantVideoBehaviour.DoNothing;
                        }

                        switch (askDialogResult.DialogResult)
                        {
                            case DialogResult.Yes:
                                ClearConvertedVideo();
                                break;

                            default:
                                break;
                        }

                        break;

                    case PlayNonExistantVideoBehaviour.Clear:

                        if (_applicationSettings.WarnAboutNonExistingMedia)
                        {
                            var clearDialogResult = _dialogService.ShowDialog(
                                "Converted File Missing",
                                $"The converted file {Data.FullName} seems to be missing at '{Data.ConvertedFilePath}'." +
                                $"\n\nThe media file shall be reverted to the Loaded State and have its file reference cleared.",
                                new AdditionalOption()
                                {
                                    Name = "Don't show this again",
                                    Checked = false,
                                });

                            if (clearDialogResult.AdditionalOptions.Count > 0 && clearDialogResult.AdditionalOptions[0].Checked)
                            {
                                _applicationSettings.WarnAboutNonExistingMedia = false;
                            }
                        }

                        ClearConvertedVideo();
                        break;

                    case PlayNonExistantVideoBehaviour.DoNothing:

                        if (_applicationSettings.WarnAboutNonExistingMedia)
                        {
                            var clearDialogResult = _dialogService.ShowDialog(
                                "Converted File Missing",
                                $"The converted file {Data.FullName} seems to be missing at '{Data.ConvertedFilePath}'.",
                                new AdditionalOption()
                                {
                                    Name = "Don't show this again",
                                    Checked = false,
                                });

                            if (clearDialogResult.AdditionalOptions.Count > 0 && clearDialogResult.AdditionalOptions[0].Checked)
                            {
                                _applicationSettings.WarnAboutNonExistingMedia = false;
                            }
                        }

                        break;
                }

                return;
            }

            using (var process = new Process())
            {
                process.StartInfo.FileName = "explorer";
                process.StartInfo.Arguments = "\"" + Data.ConvertedFilePath + "\"";
                process.Start();
            }
        }

        public void StateChanged()
        {
            switch(Data.State)
            {
                case VideoState.Created:
                    {
                        IsDownloadable = false;
                        IsConvertable = false;
                        IsLoadable = true;
                        IsRemovable = true;
                        IsPlayableForDownloaded = false;
                        IsPlayableForConverted = false;

                        if (AutoLoad)
                        {
                            Load();
                            return;
                        }

                        break;
                    }

                case VideoState.Loaded:
                    {
                        IsDownloadable = true;
                        IsConvertable = false;
                        IsLoadable = false;
                        IsRemovable = true;
                        IsPlayableForDownloaded = false;
                        IsPlayableForConverted = false;

                        if (AutoDownload)
                        {
                            Download();
                            return;
                        }

                        break;
                    }

                case VideoState.Downloaded:
                    {
                        IsDownloadable = false;
                        IsConvertable = true;
                        IsLoadable = false;
                        IsRemovable = true;
                        IsPlayableForDownloaded = true;
                        IsPlayableForConverted = false;

                        if(AutoConvert)
                        {
                            Convert();
                            return;
                        }

                        break;
                    }

                case VideoState.Converted:
                    IsDownloadable = false;
                    IsConvertable = false;
                    IsLoadable = false;
                    IsRemovable = true;
                    IsPlayableForDownloaded = true;
                    IsPlayableForConverted = true;
                    break;
            }

            IsBusy = false;
        }

        private void ClearDownloadedVideo()
        {
            Data.DownloadedFilePath = string.Empty;
            Data.State = VideoState.Loaded;
        }

        private void ClearConvertedVideo()
        {
            Data.ConvertedFilePath = string.Empty;
            Data.State = VideoState.Downloaded;
        }
    }
}
