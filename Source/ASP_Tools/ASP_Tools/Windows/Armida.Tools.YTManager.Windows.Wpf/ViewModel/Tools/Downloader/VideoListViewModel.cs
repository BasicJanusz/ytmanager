﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Common.Dialog.Service;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using Armida.Tools.YTManager.Windows.Wpf.Utils.Options;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options.Conversion;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Model;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Downloader
{
    public class VideoListViewModel : CoreViewModel
    {
        private readonly IConversionService _conversionService;
        private readonly IApplicationSettings _applicationSettings;
        private readonly IDialogService _dialogService;
        private readonly IOptionsProvider _optionProvider;
        private readonly ConversionProfileListViewModel _conversionProfileListViewModel;
        public event Action<YTPage> HandleYoutubePage;

        private DownloadFormatOption _selectedDownloadFormat;
        private ConversionProfile _selectedConversionProfile;
        private RemoveOption _selectedRemoveOption;

        public ObservableCollection<VideoItemViewModel> Videos { get; }
        public bool IsAnyVideoLoaded => Videos.Count > 0;
        public bool AreOptionsVisible { get; set; }
        public ObservableCollection<string> ConversionOptions => _conversionService.ConversionOptions;
        public bool IsAnyConversionOptionSelected => !string.IsNullOrWhiteSpace(SelectedConversionOption);
        public string SelectedConversionOption
        {
            get => _conversionService.SelectedConversionOption;
            set
            {
                _conversionService.SelectedConversionOption = value;
                RefreshVideoConversionButtons();
            }
        }

        private void RefreshVideoConversionButtons()
        {
            foreach(var video in Videos)
            {
                video.RaisePropertyChanged(nameof(IsAnyConversionOptionSelected));
            }
        }

        public bool AddVideosFromPreviousSession
        {
            get => _applicationSettings.AddVideosFromPreviousSession;
            set
            {
                _applicationSettings.AddVideosFromPreviousSession = value;
                RaisePropertyChanged(nameof(AddVideosFromPreviousSession));
            }
        }

        public bool UseWindowParametersFromPreviousSession
        {
            get => _applicationSettings.UseWindowParametersFromPreviousSession;
            set
            {
                _applicationSettings.UseWindowParametersFromPreviousSession = value;
                RaisePropertyChanged(nameof(UseWindowParametersFromPreviousSession));
            }
        }

        public bool AllowPlaylists
        {
            get => _applicationSettings.AllowPlaylists;
            set
            {
                _applicationSettings.AllowPlaylists = value;
                RaisePropertyChanged(nameof(AllowPlaylists));
            }
        }
        public bool AllowRadio
        {
            get => _applicationSettings.AllowRadio;
            set
            {
                _applicationSettings.AllowRadio = value;
                RaisePropertyChanged(nameof(AllowRadio));
            }
        }
        public bool AutoLoad
        {
            get => _applicationSettings.AutoLoad;
            set
            {
                _applicationSettings.AutoLoad = value;
                RaisePropertyChanged(nameof(AutoLoad));

                RaiseAutoConvertChanged();
            }
        }
        public bool AutoDownload
        {
            get => _applicationSettings.AutoDownload;
            set
            {
                _applicationSettings.AutoDownload = value;
                RaisePropertyChanged(nameof(AutoDownload));

                RaiseAutoConvertChanged();
            }
        }
        public bool AutoConvert
        {
            get => _applicationSettings.AutoConvert;
            set
            {
                _applicationSettings.AutoConvert = value;
                RaisePropertyChanged(nameof(AutoConvert));

                RaiseAutoConvertChanged();
            }
        }

        public List<RemoveOption> RemoveOptions => _optionProvider.RemoveOptions;
        public RemoveOption SelectedRemoveOption
        {
            get => _selectedRemoveOption;
            set
            {
                _selectedRemoveOption = value;
                RaiseRemoveOptionChanged();
                
                UpdateRemoveOptionSetting();
            }
        }

        public int ScrollSpeed => _applicationSettings.ScrollSpeed;

        private List<Quality> _qualityOptions = new()
            {
                Quality.Lowest,
                Quality.Low,
                Quality.Medium,
                Quality.High,
                Quality.Higher,
                Quality.Ultra,
            };
        public List<Quality> QualityOptions => _qualityOptions;
        public Quality SelectedQualityOption
        {
            get => _applicationSettings.Quality;
            set
            {
                _applicationSettings.Quality = value;
                RaisePropertyChanged(nameof(SelectedQualityOption));
                
                RaiseQualityOptionChanged();
            }
        }

        public List<ConversionProfile> ConversionProfiles => _conversionProfileListViewModel.ConversionProfilesAsList;
        public ConversionProfile SelectedConversionProfile
        {
            get => _selectedConversionProfile; 
            set
            {
                _selectedConversionProfile = value;
                RaiseConversionProfileChanged();
                
                UpdateConversionProfileSetting();
            }
        }

        public List<DownloadFormatOption> DownloadFormats => _optionProvider.DownloadFormatOptions;
        public DownloadFormatOption SelectedDownloadFormat
        {
            get => _selectedDownloadFormat;
            set
            {
                _selectedDownloadFormat = value;
                RaiseDownloadFormatChanged();
                
                UpdateDownloadFormatSetting();
                UpdateDownloadFormatInVideoList();
            }
        }

        private List<DownloadMode> _downloadModes = new()
            {
                DownloadMode.MuxedHighestVideoQuality,
                DownloadMode.MuxedHighestBitrateQuality,
                DownloadMode.HighestPossibleQuality,
            };
        public List<DownloadMode> DownloadModes => _downloadModes;
        public DownloadMode SelectedDownloadMode
        {
            get => _applicationSettings.DownloadMode;
            set
            {
                _applicationSettings.DownloadMode = value;
                RaisePropertyChanged(nameof(SelectedDownloadMode));

                RaiseDownloadModeChanged();
            }
        }

        private string _currentUrl;
        public string CurrentUrl
        {
            get => _currentUrl;
            set
            {
                _currentUrl = value;
                RaisePropertyChanged(nameof(CurrentUrl));
                
                RaisePropertyChanged(nameof(CanCurrentUrlBeProcessed));
            }
        }
        public bool CanCurrentUrlBeProcessed => !string.IsNullOrWhiteSpace(CurrentUrl);
        public bool IsCurrentUrlProcessed { get; set; }

        public ICommand RemoveAllCommand { get; }
        public ICommand RemoveSelectedCommand { get; }
        public ICommand SelectAllCommand {  get; }
        public ICommand DeselectAllCommand {  get; }
        public ICommand DownloadAllCommand {  get; }
        public ICommand DownloadSelectedCommand {  get; }
        public ICommand ConvertAllCommand {  get; }
        public ICommand ConvertSelectedCommand {  get; }
        public ICommand ClearCommand {  get; }
        public ICommand ToggleOptionVisibilityCommand {  get; }
        public ICommand HandleCurrentUrlCommand { get; }
        public ICommand OpenConvertDirectoryCommand { get; }
        public ICommand BrowseConvertwDirectoryCommand { get; }
        public ICommand OpenDownloadDirectoryCommand { get; }
        public ICommand BrowseDownloadDirectoryCommand { get; }
        public ICommand OpenAuthenticationDirectoryCommand { get; }
        public ICommand BrowseAuthenticationDirectoryCommand { get; }

        public VideoListViewModel(IConversionService conversionService, IApplicationSettings applicationSettings, IDialogService dialogService, IOptionsProvider optionsProvider, ConversionProfileListViewModel conversionProfileListViewModel)
        {
            Videos = new ObservableCollection<VideoItemViewModel>();
            Videos.CollectionChanged += VideoCollectionChanged;
            
            RemoveAllCommand = new RelayCommand(RemoveAll);
            RemoveSelectedCommand = new RelayCommand(RemoveSelected);
            SelectAllCommand = new RelayCommand(SelectAll);
            DeselectAllCommand = new RelayCommand(DeselectAll);
            DownloadAllCommand = new RelayCommand(DownloadAll);
            DownloadSelectedCommand = new RelayCommand(DownloadSelected);
            ConvertAllCommand = new RelayCommand(ConvertAll);
            ConvertSelectedCommand = new RelayCommand(ConvertSelected);
            ClearCommand = new RelayCommand(Clear);
            ToggleOptionVisibilityCommand = new RelayCommand(ToggleOptionVisibility);
            HandleCurrentUrlCommand = new RelayCommand(HandleCurrentUrl);
            OpenConvertDirectoryCommand = new RelayCommand(OpenConvertDirectory);
            BrowseConvertwDirectoryCommand = new RelayCommand(BrowseConvertDirectory);
            OpenDownloadDirectoryCommand = new RelayCommand(OpenDownloadDirectory);
            BrowseDownloadDirectoryCommand = new RelayCommand(BrowseDownloadDirectory);
            OpenAuthenticationDirectoryCommand = new RelayCommand(OpenAuthenticationDirectory);
            BrowseAuthenticationDirectoryCommand = new RelayCommand(BrowseAuthenticationDirectory);

            _conversionService = conversionService;
            _applicationSettings = applicationSettings;
            _dialogService = dialogService;
            _optionProvider = optionsProvider;
            _conversionProfileListViewModel = conversionProfileListViewModel;

            _applicationSettings.DownloadFormatChanged += LoadDownloadFormatSetting;
            _applicationSettings.SelectedRemoveOptionChanged += LoadRemoveOptionSetting; // () => RaisePropertyChanged(nameof(SelectedRemoveOption));
            _applicationSettings.SelectedConversionFormatChanged += LoadConversionProfileSetting; // () => RaisePropertyChanged(nameof(SelectedConversionOption));
            _applicationSettings.QualityChanged += () => RaisePropertyChanged(nameof(SelectedQualityOption));

            LoadInitialOptionValues();

            CurrentUrl = string.Empty;
            IsCurrentUrlProcessed = true;
        }

        private void LoadInitialOptionValues()
        {
            LoadRemoveOptionSetting(); 
            LoadConversionProfileSetting();
            LoadDownloadFormatSetting();
        }

        private void VideoCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(IsAnyVideoLoaded));
        }

        public void RefreshOptions()
        {
            RaisePropertyChanged(nameof(AddVideosFromPreviousSession));
            RaisePropertyChanged(nameof(UseWindowParametersFromPreviousSession));
            RaisePropertyChanged(nameof(AllowPlaylists));
            RaisePropertyChanged(nameof(AllowRadio));
            RaisePropertyChanged(nameof(AutoLoad));
            RaisePropertyChanged(nameof(AutoDownload));
            RaisePropertyChanged(nameof(AutoConvert));
            RaisePropertyChanged(nameof(SelectedRemoveOption));
            RaisePropertyChanged(nameof(ScrollSpeed));
            RaisePropertyChanged(nameof(SelectedConversionProfile));
            RaisePropertyChanged(nameof(SelectedQualityOption));
            RaisePropertyChanged(nameof(SelectedDownloadFormat));
            RaisePropertyChanged(nameof(SelectedDownloadMode));
        }

        private void RemoveAll()
        {
            var count = Videos.Count;
            var baseCount = count;
            var baseId = 0;
            for(int i=0; i<count; i++)
            {
                RemoveVideo(Videos[baseId]);

                if (baseCount == Videos.Count)
                {
                    baseId++;
                }
                else
                {
                    baseCount = Videos.Count;
                }
            }
        }

        private void RemoveSelected()
        {
            var count = Videos.Count;
            var baseCount = count;
            var baseId = 0;
            for (int i = 0; i < count; i++)
            {
                var video = Videos[baseId];
                if (!video.IsSelected)
                {
                    baseId++;
                    continue;
                }

                RemoveVideo(video);

                if (baseCount == Videos.Count)
                {
                    
                }
                else
                {
                    baseCount = Videos.Count;
                }
            }
        }

        private void SelectAll()
        {
            foreach (var video in Videos)
            {
                SelectVideo(video);
            }
        }

        private void DeselectAll()
        {
            foreach (var video in Videos)
            {
                DeselectVideo(video);
            }
        }

        private void DownloadAll()
        {
            foreach (var video in Videos)
            {
                DownloadVideo(video);
            }
        }

        private void DownloadSelected()
        {
            foreach (var video in Videos)
            {
                if (!video.IsSelected)
                    continue;

                DownloadVideo(video);
            }
        }

        public void DeselectVideo(VideoItemViewModel video)
        {
            video.IsSelected = false;
        }

        public void SelectVideo(VideoItemViewModel video)
        {
            video.IsSelected = true;
        }

        public void DownloadVideo(VideoItemViewModel video)
        {
            if (!video.IsBusy)
                video.DownloadCommand.Execute(null);
        }

        public void ConvertAll()
        {
            foreach (var video in Videos)
            {
                ConvertVideo(video);
            }
        }

        public void ConvertSelected()
        {
            foreach (var video in Videos)
            {
                if (!video.IsSelected)
                    continue;

                ConvertVideo(video);
            }
        }

        public void ConvertVideo(VideoItemViewModel video)
        {
            if (!video.IsBusy && video.IsConvertable)
                video.ConvertCommand.Execute(null);
        }

        public void RemoveVideo(VideoItemViewModel video)
        {
            if (!video.IsBusy)
                video.RemoveCommand.Execute(null);
        }

        public void Clear()
        {
            Videos?.Clear();
        }

        public void ToggleOptionVisibility()
        {
            AreOptionsVisible = !AreOptionsVisible;
            RaisePropertyChanged(nameof(AreOptionsVisible));
            RefreshOptions();
        }

        public void OnGotFocus(string clipboardText)
        {
            if (IsCurrentUrlProcessed && 
                YTPage.IsYtPage(clipboardText) &&
                !UrlAlreadyProcessed(clipboardText))
            {
                CurrentUrl = clipboardText;
                IsCurrentUrlProcessed = false;
            }
        }

        public void HandleCurrentUrl()
        {
            var page = YTPage.Parse(CurrentUrl);

            if (page != null)
            {
                HandleYoutubePage?.Invoke(page);
                IsCurrentUrlProcessed = true;
                CurrentUrl = string.Empty;
            }
        }

        private void OpenConvertDirectory()
        {
            Process.Start(Environment.GetEnvironmentVariable("WINDIR") + @"\explorer.exe", _applicationSettings.ConvertDirectory);
        }

        private void BrowseConvertDirectory()
        {
            var result = _dialogService.ShowDirectoryDialog("Select Convert Directory", "", _applicationSettings.ConvertDirectory);
            if (result.DialogResult != DialogResult.SelectedDirectory)
            {
                return;
            }

            _applicationSettings.ConvertDirectory = result.Data;
        }

        private void OpenDownloadDirectory()
        {
            Process.Start(Environment.GetEnvironmentVariable("WINDIR") + @"\explorer.exe", _applicationSettings.DownloadDirectory);
        }

        private void BrowseDownloadDirectory()
        {
            var result = _dialogService.ShowDirectoryDialog("Select Convert Directory", "", _applicationSettings.DownloadDirectory);
            if (result.DialogResult != DialogResult.SelectedDirectory)
            {
                return;
            }

            _applicationSettings.DownloadDirectory = result.Data;
        }

        private void OpenAuthenticationDirectory()
        {
            Process.Start(Environment.GetEnvironmentVariable("WINDIR") + @"\explorer.exe", _applicationSettings.AuthenticationDirectory);
        }

        private void BrowseAuthenticationDirectory()
        {
            var result = _dialogService.ShowDirectoryDialog("Select Authentication Directory", "", _applicationSettings.AuthenticationDirectory);
            if (result.DialogResult != DialogResult.SelectedDirectory)
            {
                return;
            }

            _applicationSettings.AuthenticationDirectory = result.Data;
        }

        private bool UrlAlreadyProcessed(string url)
        {
            foreach(var video in Videos)
            {
                if (video.Url == url)
                    return true;
            }

            return false;
        }

        private void RaiseAutoConvertChanged()
        {
            if(_applicationSettings.SelectedVideoParameterBehaviour == VideoParameterBehaviour.ChangeForSelectedAndAdded)
            {
                foreach(var video in Videos)
                {
                    if(video.IsSelected)
                    {
                        video.AutoLoad = AutoLoad;
                        video.AutoDownload = AutoDownload;
                        video.AutoConvert = AutoConvert;
                    }
                }
            }

            if (_applicationSettings.SelectedVideoParameterBehaviour == VideoParameterBehaviour.ChangeForAll)
            {
                foreach (var video in Videos)
                {
                    video.AutoLoad = AutoLoad;
                    video.AutoDownload = AutoDownload;
                    video.AutoConvert = AutoConvert;
                }
            }
        }

        private void RaiseQualityOptionChanged()
        {
            if (_applicationSettings.SelectedVideoParameterBehaviour == VideoParameterBehaviour.ChangeForSelectedAndAdded)
            {
                foreach (var video in Videos)
                {
                    if (video.IsSelected)
                    {
                        video.SelectedQualityOption = SelectedQualityOption;
                    }
                }
            }

            if (_applicationSettings.SelectedVideoParameterBehaviour == VideoParameterBehaviour.ChangeForAll)
            {
                foreach (var video in Videos)
                {
                    video.SelectedQualityOption = SelectedQualityOption;
                }
            }
        }

        private void RaiseRemoveOptionChanged()
        {
            RaisePropertyChanged(nameof(SelectedRemoveOption));
        }

        private void RaiseConversionProfileChanged()
        {
            RaisePropertyChanged(nameof(SelectedConversionProfile));
        }

        private void RaiseDownloadFormatChanged()
        {
            RaisePropertyChanged(nameof(SelectedDownloadFormat));
        }

        private void LoadRemoveOptionSetting()
        {
            foreach(var option in RemoveOptions)
            {
                if(option.Type == _applicationSettings.SelectedRemoveOption)
                {
                    _selectedRemoveOption = option;
                    RaiseRemoveOptionChanged();
                    break;
                }
            }    
        }

        private void LoadConversionProfileSetting()
        {
            foreach(var profile in ConversionProfiles)
            {
                if(profile.Guid == _applicationSettings.SelectedConversionProfile)
                {
                    _selectedConversionProfile = profile;
                    RaiseConversionProfileChanged();
                    break;
                }
            }
        }

        private void LoadDownloadFormatSetting()
        {
            foreach(var option in DownloadFormats)
            {
                if(option.Format == _applicationSettings.DownloadFormat)
                {
                    _selectedDownloadFormat = option;
                    RaiseDownloadFormatChanged();
                    break;
                }
            }
        }

        private void UpdateRemoveOptionSetting()
        {
            _applicationSettings.SelectedRemoveOption = SelectedRemoveOption.Type;
        }

        private void UpdateDownloadFormatSetting()
        {
            _applicationSettings.DownloadFormat = SelectedDownloadFormat.Format;
        }

        private void UpdateConversionProfileSetting()
        {
            _applicationSettings.SelectedConversionProfile = SelectedConversionProfile.Guid;
        }

        private void UpdateDownloadFormatInVideoList()
        {
            if (_applicationSettings.SelectedVideoParameterBehaviour == VideoParameterBehaviour.ChangeForSelectedAndAdded)
            {
                foreach (var video in Videos)
                {
                    if (video.IsSelected)
                    {
                        video.SelectedDownloadFormat = SelectedDownloadFormat.Format;
                    }
                }
            }

            if (_applicationSettings.SelectedVideoParameterBehaviour == VideoParameterBehaviour.ChangeForAll)
            {
                foreach (var video in Videos)
                {
                    video.SelectedDownloadFormat = SelectedDownloadFormat.Format;
                }
            }
        }

        private void RaiseDownloadModeChanged()
        {
            if (_applicationSettings.SelectedVideoParameterBehaviour == VideoParameterBehaviour.ChangeForSelectedAndAdded)
            {
                foreach (var video in Videos)
                {
                    if (video.IsSelected)
                    {
                        video.SelectedDownloadMode = SelectedDownloadMode;
                    }
                }
            }

            if (_applicationSettings.SelectedVideoParameterBehaviour == VideoParameterBehaviour.ChangeForAll)
            {
                foreach (var video in Videos)
                {
                    video.SelectedDownloadMode = SelectedDownloadMode;
                }
            }
        }
    }
}
