﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using CommunityToolkit.Mvvm.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Converter
{
    public class ConverterViewModel : CoreViewModel
    {
        private readonly IConversionService _conversionService;
        private readonly IApplicationSettings _applicationSettings;
        private readonly Dispatcher _dispatcher;

        public ObservableCollection<MediaFileViewModel> MediaFiles { get; }
        public bool IsAnyVideoLoaded => MediaFiles.Count > 0;
        public bool AreOptionsVisible { get; set; }
        public ObservableCollection<string> ConversionOptions => _conversionService.ConversionOptions;
        public bool IsAnyConversionOptionSelected => !string.IsNullOrWhiteSpace(SelectedConversionOption);
        public string SelectedConversionOption
        {
            get => _conversionService.SelectedConversionOption;
            set
            {
                _conversionService.SelectedConversionOption = value;
                RefreshVideoConversionButtons();
            }
        }

        private void RefreshVideoConversionButtons()
        {
            foreach (var file in MediaFiles)
            {
                file.RaisePropertyChanged(nameof(IsAnyConversionOptionSelected));
            }
        }

        private readonly List<RemoveOption> _removeOptions = new()
            {
                new() { Type = RemoveOptionType.Nothing, Description = "Don't delete anything" },
                new() { Type = RemoveOptionType.DownloadedFile, Description = "Delete the downloaded file" },
                new() { Type = RemoveOptionType.ConvertedFile, Description = "Delete the converted file" },
                new() { Type = RemoveOptionType.All, Description = "Delete both files" },
            };
        public List<RemoveOption> RemoveOptions => _removeOptions;
        private RemoveOption _selectedRemoveOption;
        public RemoveOption SelectedRemoveOption
        {
            get => _selectedRemoveOption;
            set
            {
                _selectedRemoveOption = value;
                RaisePropertyChanged(nameof(SelectedRemoveOption));
            }
        }

        public ICommand RemoveAllCommand { get; }
        public ICommand RemoveSelectedCommand { get; }
        public ICommand SelectAllCommand { get; }
        public ICommand DeselectAllCommand { get; }
        public ICommand ConvertAllCommand { get; }
        public ICommand ConvertSelectedCommand { get; }
        public ICommand ClearCommand { get; }
        public ICommand ToggleOptionVisibilityCommand { get; }

        public ConverterViewModel(IConversionService conversionService, Dispatcher dispatcher, IApplicationSettings applicationSettings) 
        {
            _conversionService = conversionService;
            _dispatcher = dispatcher;
            _applicationSettings = applicationSettings;
            MediaFiles = new ObservableCollection<MediaFileViewModel>();

            RemoveAllCommand = new RelayCommand(RemoveAll);
            RemoveSelectedCommand = new RelayCommand(RemoveSelected);
            SelectAllCommand = new RelayCommand(SelectAll);
            DeselectAllCommand = new RelayCommand(DeselectAll);
            ConvertAllCommand = new RelayCommand(ConvertAll);
            ConvertSelectedCommand = new RelayCommand(ConvertSelected);
            ClearCommand = new RelayCommand(Clear);
            ToggleOptionVisibilityCommand = new RelayCommand(ToggleOptionVisibility);

            _applicationSettings.SelectedRemoveOptionChanged += () => RaisePropertyChanged(nameof(SelectedRemoveOption));
            _applicationSettings.SelectedConversionFormatChanged += () => RaisePropertyChanged(nameof(SelectedConversionOption));
        }

        private void RemoveAll()
        {
            var count = MediaFiles.Count;
            var baseCount = count;
            var baseId = 0;
            for (int i = 0; i < count; i++)
            {
                RemoveMediaFile(MediaFiles[baseId]);

                if (baseCount == MediaFiles.Count)
                {
                    baseId++;
                }
                else
                {
                    baseCount = MediaFiles.Count;
                }
            }
        }

        private void RemoveSelected()
        {
            var count = MediaFiles.Count;
            var baseCount = count;
            var baseId = 0;
            for (int i = 0; i < count; i++)
            {
                var mediafile = MediaFiles[baseId];
                if (!mediafile.IsSelected)
                {
                    baseId++;
                    continue;
                }

                RemoveMediaFile(mediafile);

                if (baseCount == MediaFiles.Count)
                {
                }
                else
                {
                    baseCount = MediaFiles.Count;
                }
            }
        }

        public void CreateFile(string filePath)
        {
            // confirmed that the file exists
            var mediaFile = new MediaFile(filePath);
            var viewModel = new MediaFileViewModel(mediaFile, _conversionService);
            viewModel.RaiseMediaFileRemoved += (viewModel) => MediaFiles.Remove(viewModel);

            _dispatcher.Invoke(() =>
            {
                MediaFiles.Add(viewModel);
            });

            viewModel.Data.State = MediaFileState.Loaded;
        }

        public void DeselectFile(MediaFileViewModel file)
        {
            file.IsSelected = false;
        }

        public void SelectFile(MediaFileViewModel file)
        {
            file.IsSelected = true;
        }

        private void SelectAll()
        {
            foreach (var file in MediaFiles)
            {
                SelectFile(file);
            }
        }

        private void DeselectAll()
        {
            foreach (var file in MediaFiles)
            {
                DeselectFile(file);
            }
        }

        public void ConvertAll()
        {
            if (!IsAnyConversionOptionSelected)
            {
                MessageBox.Show("Choose a conversion format first.");
                return;
            }

            foreach (var file in MediaFiles)
            {
                ConvertFile(file);
            }
        }

        public void ConvertSelected()
        {
            if (!IsAnyConversionOptionSelected)
            {
                MessageBox.Show("Choose a conversion format first.");
                return;
            }

            foreach (var file in MediaFiles)
            {
                if (!file.IsSelected)
                    continue;

                ConvertFile(file);
            }
        }

        public void ConvertFile(MediaFileViewModel file)
        {
            if (!file.IsBusy && file.IsConvertable)
                file.ConvertCommand.Execute(null);
        }

        public void RemoveMediaFile(MediaFileViewModel mediaFile)
        {
            if (!mediaFile.IsBusy)
                mediaFile.RemoveCommand.Execute(null);
        }

        public void Clear()
        {
            MediaFiles?.Clear();
        }

        public void ToggleOptionVisibility()
        {
            AreOptionsVisible = !AreOptionsVisible;
            RaisePropertyChanged(nameof(AreOptionsVisible));
        }
    }
}
