﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Service;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Windows;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Converter
{
    public class MediaFileViewModel : CoreViewModel
    {
        public event Action<MediaFileViewModel> RaiseMediaFileRemoved;

        private readonly IConversionService _conversionService;
        private bool _isSelected;
        private bool _isBusy;
        private bool _isConvertable;
        private bool _isRemovable;

        public MediaFile Data { get; }
        public string Name => Data.Name;
        public string Path => Data.Path;

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                RaisePropertyChanged(nameof(IsSelected));
            }
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                RaisePropertyChanged(nameof(IsBusy));
            }
        }

        public bool IsConvertable
        {
            get => _isConvertable;
            set
            {
                _isConvertable = value;
                RaisePropertyChanged(nameof(IsConvertable));
            }
        }

        public bool IsRemovable
        {
            get => _isRemovable;
            set
            {
                _isRemovable = value;
                RaisePropertyChanged(nameof(IsRemovable));
            }
        }

        public bool IsAnyConversionOptionSelected
        {
            get => _conversionService.IsAnyConversionOptionSelected;
        }

        public ICommand ConvertCommand { get; }
        public ICommand RemoveCommand { get; }

        public MediaFileViewModel(MediaFile mediaFile, IConversionService conversionService) 
        {
            Data = mediaFile;
            Data.PropertyChanged += (obj, arg) => RaisePropertyChanged(arg.PropertyName);
            Data.StateChanged += StateChanged;

            _conversionService = conversionService;

            ConvertCommand = new RelayCommand(Convert);
            RemoveCommand = new RelayCommand(Remove);

            IsSelected = false;
            IsBusy = false;
            IsConvertable = false;
            IsRemovable = false;
        }

        private void Convert()
        {
            if (!IsAnyConversionOptionSelected)
            {
                MessageBox.Show("Choose a conversion format first.");
                return;
            }

            IsBusy = true;
            _conversionService.EnqueueProcess(ProcessType.Convert, Data);
        }

        private void Remove()
        {
            IsBusy = true;
            _conversionService.EnqueueProcess(ProcessType.Remove, Data);
            RaiseMediaFileRemoved?.Invoke(this);
        }

        private void StateChanged()
        {
            switch (Data.State)
            {
                case MediaFileState.Loaded:
                    IsRemovable = true;
                    IsConvertable = true;
                    break;

                case MediaFileState.Converted:
                    IsConvertable = false;
                    break;
            }

            IsBusy = false;
        }
    }
}
