﻿using Armida.Tools.YTManager.Windows.Wpf.Common.Core.ViewModel;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Tools.Downloader;
using Armida.Tools.YTManager.Windows.Wpf.ViewModel.Options;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Model;
using CommunityToolkit.Mvvm.Input;
using System.IO;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.ViewModel.Main
{
    public class MainViewModel : CoreViewModel
    {
        private bool _areOptionsVisible;
        public bool AreOptionsVisible 
        {
            get => _areOptionsVisible;
            private set
            {
                _areOptionsVisible = value;
                RaisePropertyChanged(nameof(AreOptionsVisible));
            }
        }

        public OptionsViewModel OptionsViewModel { get; }
        public ToolsViewModel ToolsViewModel { get; }
        public VideoListViewModel VideoListViewModel { get; }

        public ICommand OptionsCommand { get; }
        public ICommand ReturnCommand { get; }

        public MainViewModel(ToolsViewModel toolsViewModel, VideoListViewModel videoListViewModel, OptionsViewModel optionsViewModel) 
        {
            ToolsViewModel = toolsViewModel;
            VideoListViewModel = videoListViewModel;
            OptionsViewModel = optionsViewModel;

            OptionsCommand = new RelayCommand(Options);
            ReturnCommand = new RelayCommand(Return);
        }

        private void Options()
        {
            ToolsViewModel.AreToolsVisible = false;
            AreOptionsVisible = true;

            OptionsViewModel.LoadSelectedOptionsFromSettings();
        }

        private void Return()
        {
            AreOptionsVisible = false;
            ToolsViewModel.AreToolsVisible = true;

            VideoListViewModel.RefreshOptions();
        }

        public void HandleText(string text)
        {
            var page = YTPage.Parse(text);

            if (page != null)
                ToolsViewModel.HandleYoutubePage(page);

            if(File.Exists(text))
            {
                ToolsViewModel.HandleFile(text);
            }
        }
    }
}
