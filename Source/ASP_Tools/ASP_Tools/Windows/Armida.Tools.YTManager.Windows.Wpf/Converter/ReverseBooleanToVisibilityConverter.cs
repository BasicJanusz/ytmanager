﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter
{
    public class ReverseBooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null && 
               value is bool valueAsBool && 
               valueAsBool) 
            {
                return Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
