﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Enum
{
    public enum MediaFileFormat
    {
        // Video
        MPEG4, // Part 14 (.mp4)
        Matroska,// (.mkv)
        AudioVideoInterleave, // (.avi)
        FlashVideo, // (.flv)
        MPEG2_ts, // Transport Stream (.ts)
        MPEG2_ps, // Program Stream (.ps)
        QuickTime, // QuickTime File Format (.mov, .qt)
        WebM, // (.webm)
        WindowsMediaVideo, // (.wmv)
        RealMedia, // (.rm, .rmvb)
        Ogg, // (.ogg, .ogv)
        ThirdGenerationProject, // 3GP (.3gp)
        DigitalVideo, // (.dv)
        MotionJPEG, // (.mjpeg, .mjpg)
        ASF, // (.asf)
        MXF, // (.mxf)
        H_264_Video, // (.h264)
        H_265_Video, // (.hevc)
        VP8, // (.vp8)
        VP9, // (.vp9)
        AV1, // (.av1)

        // Audio
        MP3, // (.mp3)
        AdvancedAudioCoding, // (.aac, .m4a)
        WaveformAudioFileFormat, // (.wav)
        OggVorbis, // (.ogg)
        FreeLosslessAudioCodec, // (.flac)
        AudioInterchangeFileFormat, // (.aiff)
        WindowsMediaAudio, // (.wma)
        Opus, // (.opus)
        MonkeysAudio, // (.ape)
        TrueAudio, // (.tta)
        MPEGAudioLayerIII, // (.mp3)
        DolbyDigital, // (.ac3)
        DigitalTheaterSystems, // (.dts)
        AppleLosslessAudioCodec, // (.alac)
        AdaptiveMultiRate, // (.amr)

        None,
        Any,
    }

    public enum MediaType
    {
        AudioVideo,
        AudioOnly,
        VideoOnly,
        None,
    }

    /// <summary>
    /// Stores data displayed to the user. If used, the object is to be stored as <b>MediaFileFormat</b> enum as a reference.<br></br>
    /// It is like that because the user does not edit any Property.
    /// </summary>
    public class MediaFileFormatEntry
    {
        public MediaFileFormat Format { get; private set; }
        public MediaType MediaType { get; private set; }
        public string FullName { get; private set; }
        public string Description { get; private set; }
        public string Extension { get; private set; }

        public MediaFileFormatEntry(MediaFileFormat format, MediaType mediaType, string fullName, string description, string extension)
        {
            Format = format;
            MediaType = mediaType;
            FullName = fullName;
            Description = description;
            Extension = extension;
        }
    }

    /// <summary>
    /// Stores data displayed to the user. User may edit the Parameter property. That is why it is being copied.<br></br>
    /// If used, the object is to be stored as <b>ConversionFilterOption</b>.<br></br><br></br>
    /// 
    /// When used/edited/stored the entry is being recognized by its <b>Guid</b>.<br></br> 
    /// Only when the user sets up the filter the <b>Guid</b> is assigned.
    /// </summary>
    public class MediaFileFilterEntry
    {
        private event Action<Filter> Remove;

        public Guid Guid { get; set; }
        public Filter Filter { get; private set; }
        public FilterType Type { get; private set; }
        public string Name { get; private set; }
        public string FullName => $"{Name} [{Parameter}] [{GetMediaTypeAsString()}]";
        public string Description { get; private set; }
        public string Parameter { get; set; }

        public ICommand RemoveCommand { get; }

        private MediaFileFilterEntry(Filter filter, FilterType type, string name, string description, string parameter)
        {
            Guid = Guid.Empty;
            Filter = filter;
            Type = type; 
            Name = name;
            Description = description;
            Parameter = parameter;

            RemoveCommand = new RelayCommand<Filter>((f) => Remove?.Invoke(f), (_) => true);
        }

        public void SetRemoveBehaviour(Action<Filter> behaviour)
        {
            Remove += behaviour;
        }

        public static MediaFileFilterEntry Copy(MediaFileFilterEntry e)
        {
            Span<char> name = new(new char[e.Name.Length]);
            Span<char> description = new(new char[e.Description.Length]);
            Span<char> parameter = new(new char[e.Parameter.Length]);

            e.Name.CopyTo(name);
            e.Description.CopyTo(description);
            e.Parameter.CopyTo(parameter);

            return new(e.Filter, e.Type, name.ToString(), description.ToString(), parameter.ToString());
        }

        public static MediaFileFilterEntry CreateFromData(MediaFileFilterData d)
        {
            Span<char> parameter = new(new char[d.Parameter.Length]);
            d.Parameter.CopyTo(parameter);

            return new(d.Filter, d.Type, d.FullName, d.Description, parameter.ToString());
        }

        public override string ToString() => $"{Filter:g}:{GetMediaTypeAsString()}";

        private string GetMediaTypeAsString() =>
            Type switch
            {
                FilterType.Audio => "F:A",
                FilterType.Video => "F:V",
                FilterType.SpecialAudio => "F:SA",
                FilterType.SpecialVideo=> "F:SV",
                FilterType.BitstreamSubtitle => "BTS:S",
                FilterType.BitstreamVideo => "BTS:V",
                FilterType.BitstreamAudio => "BTS:A",
                _ => "X",
            };
    }

    public interface IMediaFileDataProvider
    {
        public void LoadData();
        public MediaFileFormatEntry ProvideFormatEntry(MediaFileFormat format);
        public ObservableCollection<MediaFileFormatEntry> ProvideAvailableFormats();
        public MediaFileFormatEntry ProvideDefaultMediaFileFormatEntry();

        public MediaFileFilterData ProvideFilterData(Filter filter);
        public List<MediaFileFilterEntry> ProvideFilterEntries(List<ConversionFilterOption> filter);

        public ObservableCollection<MediaFileFilterEntry> ProvideAvailableFilters();
        public MediaFileFilterEntry ProvideDefaultMediaFileFilterEntry();
        public ObservableCollection<MediaFileFilterEntry> ProvideAvailableBitstreamFilters();
        public MediaFileFilterEntry ProvideDefaultMediaFileBitstreamFilterEntry();

        public ObservableCollection<CodecEntry> ProvideAvailableDecoders();
        public ObservableCollection<CodecEntry> ProvideAvailableEncoders();
        public List<CodecEntry> ProvideCodecEntries(List<CodecOption> codecs);
    }

    /// <summary>
    /// Used to provide outside information about a format, filter or codec. 
    /// </summary>
    public class MediaFileDataProvider : IMediaFileDataProvider
    {
        private Dictionary<MediaFileFormat, MediaFileFormatData> _availableFormats;
        private ObservableCollection<MediaFileFormatEntry> _formatEntries;
        private Dictionary<Filter, MediaFileFilterData> _availableBitstreamFilters;
        private ObservableCollection<MediaFileFilterEntry> _bitstreamFilterEntries;
        private Dictionary<Filter, MediaFileFilterData> _availableFilters;
        private ObservableCollection<MediaFileFilterEntry> _filterEntries;
        private Dictionary<Codec, CodecData> _availableCodecs;
        private ObservableCollection<CodecEntry> _decoderEntries;
        private ObservableCollection<CodecEntry> _encoderEntries;

        public MediaFileDataProvider()
        {
            _availableFormats = new();
            _formatEntries = new();
            _availableBitstreamFilters = new();
            _bitstreamFilterEntries = new();
            _availableFilters = new();
            _filterEntries = new();
            _availableCodecs = new();
            _decoderEntries = new();
            _encoderEntries = new();
        }

        public void LoadData()
        {
            // ### LOAD  DATA ###
            LoadFormatdata();
            LoadFilterData();
            LoadBitstreamFilterData();
            LoadDecoderData();
            LoadEncoderData();
        }

        private void LoadFormatdata()
        {
            // ### FORMAT DATA ###
            // ### VIDEO ###                
            // # mp4
            _availableFormats.Add(
                MediaFileFormat.MPEG4, new()
                {
                    FullName = "MPEG4 Part 14",
                    Extension = "mp4",
                    MediaType = MediaType.AudioVideo,
                    Description = "Also known as Mp4. Employs advanced compression techniques to reduce the size of audiovisual data while maintaining relatively high quality, making it suitable for streaming and storing multimedia content.",
                });

            // ### AUDIO ###
            // # Mp3
            _availableFormats.Add(
                MediaFileFormat.MP3, new()
                {
                    FullName = "MPEG Audio Layer III",
                    Extension = "mp3",
                    MediaType = MediaType.AudioOnly,
                    Description = "Employs lossy compression techniques to significantly reduce the size of audio files while retaining perceptually similar sound quality.",
                });

            // # Map Data to Entries
            foreach (var d in _availableFormats)
            {
                var newEntry = new MediaFileFormatEntry(d.Key, d.Value.MediaType, d.Value.FullName, d.Value.Description, d.Value.Extension);
                _formatEntries.Add(newEntry);
            }
        }

        private void LoadFilterData()
        {
            //_availableFilters.Add(Filter.aac_adtstoasc, new()
            //{
            //    IsBitstream = true,
            //    IsAudio = true,
            //    IsVideo = false,
            //    Filter = Filter.aac_adtstoasc,
            //    Type = FilterType.BitstreamAudio,
            //    FullName = "ADTS to ASC",
            //    Description =
            //    "Convert MPEG-2/4 AAC ADTS to an MPEG-4 Audio Specific Configuration bitstream.\r\nThis filter creates an MPEG-4 AudioSpecificConfig from an MPEG-2/4 ADTS header and removes the ADTS header.\r\nThis filter is required for example when copying an AAC stream from a raw ADTS AAC or an MPEG-TS container to MP4A-LATM, to an FLV file, or to MOV/MP4 files and related formats such as 3GP or M4A. Please note that it is auto-inserted for MP4A-LATM and MOV/MP4 and related formats. "
            //});

            _availableFilters.Add(Filter.scale, new()
            {
                IsBitstream = false,
                IsAudio = false, 
                IsVideo = true,
                Filter = Filter.scale,
                Type = FilterType.Video,
                FullName = "Scale",
                Description = "Changes video dimension to a specific size.",
                Parameter = "scale=320:240",
            });

            _availableFilters.Add(Filter.loudnorm, new()
            {
                IsBitstream = false,
                IsAudio = true,
                IsVideo = false,
                Filter = Filter.loudnorm,
                Type = FilterType.Audio,
                FullName = "Loudness Normalization",
                Description = "Normalizes the loudness of the audio via EBU R128 algorithm.",
                Parameter = string.Empty,
            });

            _availableFilters.Add(Filter.volume, new()
            {
                IsBitstream = false,
                IsAudio = true,
                IsVideo = false,
                Filter = Filter.volume,
                Type = FilterType.Audio,
                FullName = "Volume",
                Description = "Changes the volume of the audio.",
                Parameter = "volume=10dB",
            });

            _availableFilters.Add(Filter.overlay, new()
            {
                IsBitstream = false,
                IsAudio = false,
                IsVideo = true,
                Filter = Filter.overlay,
                Type = FilterType.Video,
                FullName = "Overlay",
                Description = "Overlays one video on top of another.",
                Parameter = "overlay=10:main_h-overlay_h-10",
            });

            _availableFilters.Add(Filter.firequalizer, new()
            {
                IsBitstream = false,
                IsAudio = true,
                IsVideo = false,
                Filter = Filter.firequalizer,
                Type = FilterType.Audio,
                FullName = "FIR Eequalizer",
                Description = "Apply FIR Equalization using arbitrary frequency response.",
                Parameter = "=gain_entry='entry(0,10);entry(250,5)'",
            });

            _availableFilters.Add(Filter.atempo, new()
            {
                IsBitstream = false,
                IsAudio = true,
                IsVideo = false,
                Filter = Filter.atempo,
                Type = FilterType.Audio,
                FullName = "Atempo",
                Description = "Adjust audio tempo.",
                Parameter = "=1.25",
            });

            _availableFilters.Add(Filter.rubberband, new()
            {
                IsBitstream = false,
                IsAudio = true,
                IsVideo = false,
                Filter = Filter.rubberband,
                Type = FilterType.Audio,
                FullName = "Rubberband",
                Description = "Apply time-stretching and pitch-shifting.",
                Parameter = "=pitch=1.25",
            });

            // # Map Data to Entries
            foreach (var d in _availableFilters)
            {
                var newEntry = MediaFileFilterEntry.CreateFromData(d.Value);
                _filterEntries.Add(newEntry);
            }
        }

        private void LoadBitstreamFilterData()
        {
            _availableBitstreamFilters.Add(Filter.aac_adtstoasc, new() { 
                IsBitstream = true, IsAudio = true, IsVideo = false, 
                Filter = Filter.aac_adtstoasc, Type = FilterType.BitstreamAudio, FullName = "ADTS to ASC", 
                Description = "Convert MPEG-2/4 AAC ADTS to an MPEG-4 Audio Specific Configuration bitstream.\r\nThis filter creates an MPEG-4 AudioSpecificConfig from an MPEG-2/4 ADTS header and removes the ADTS header.\r\nThis filter is required for example when copying an AAC stream from a raw ADTS AAC or an MPEG-TS container to MP4A-LATM, to an FLV file, or to MOV/MP4 files and related formats such as 3GP or M4A. Please note that it is auto-inserted for MP4A-LATM and MOV/MP4 and related formats. ", 
                Parameter = string.Empty, });

            _availableBitstreamFilters.Add(Filter.chomp, new() { 
                IsBitstream = true, IsAudio = false, IsVideo = true, 
                Filter = Filter.chomp, Type = FilterType.BitstreamVideo, FullName = "Chomp", 
                Description = "Remove zero padding at the end of a packet.",
                Parameter = string.Empty, });

            _availableBitstreamFilters.Add(Filter.dca_core, new() { 
                IsBitstream = true, IsAudio = true, IsVideo = false, 
                Filter = Filter.dca_core, Type = FilterType.BitstreamAudio, FullName = "Core from DCA", 
                Description = "Extract the core from a DCA/DTS stream, dropping extensions such as DTS-HD.",
                Parameter = string.Empty, });

            _availableBitstreamFilters.Add(Filter.dvbsubs, new() { 
                IsBitstream = true, IsAudio = false, IsVideo = false, 
                Filter = Filter.dvbsubs, Type = FilterType.BitstreamSubtitle, FullName = "DVB Subtitles", 
                Description = "Extract the DVB subtitle stream.",
                Parameter = string.Empty, });


            // # Map Data to Entries
            foreach (var d in _availableBitstreamFilters)
            {
                var newEntry = MediaFileFilterEntry.CreateFromData(d.Value);
                _bitstreamFilterEntries.Add(newEntry);
            }
        }

        private void LoadDecoderData()
        {
            _availableCodecs.Add(Codec.mpeg4_cuvid, new()
            {
                IsAudio = false,
                IsVideo = true,
                IsDecodingSupported = true,
                IsEncodingSupported = false,
                Name = "Nvidia CUVID MPEG4",
                Description = "Nvidia CUVID MPEG4 decoder (codec mpeg4)",
                Codec = Codec.mpeg4_cuvid,
                Parameter = string.Empty,
            });

            foreach (var d in _availableCodecs)
            {
                var codecMediaType = d.Value.IsAudio
                    ? d.Value.IsVideo
                        ? CodecMediaType.AudioVideo
                        : CodecMediaType.Audio
                    : d.Value.IsVideo
                        ? CodecMediaType.Video
                        : CodecMediaType.None;

                var codecType = d.Value.IsDecodingSupported
                    ? d.Value.IsEncodingSupported
                        ? CodecType.DecoderEncoder
                        : CodecType.Decoder
                    : d.Value.IsEncodingSupported
                        ? CodecType.Encoder
                        : CodecType.None;

                var newEntry = CodecEntry.CreateFromData(d.Value, codecMediaType, codecType);
                _decoderEntries.Add(newEntry);
            }
        }

        private void LoadEncoderData()
        {
            _availableCodecs.Add(Codec.libmp3lame, new() { 
                IsAudio = true, IsVideo = false, IsDecodingSupported = false, IsEncodingSupported = true, 
                Name = "Libmp3lame MP3", Description = "MPEG audio layer 3 (MP3) codec",
                Codec = Codec.libmp3lame, Parameter = "-b:a 320k", });

            _availableCodecs.Add(Codec.mp3_mf, new() { 
                IsAudio = true, IsVideo = false, IsDecodingSupported = false, IsEncodingSupported = true, 
                Name = "MediaFoundation MP3", Description = "MPEG audio layer 3 (MP3) via MediaFoundation codec",
                Codec = Codec.mp3_mf, Parameter = string.Empty, });

            foreach(var d in _availableCodecs)
            {
                var codecMediaType = d.Value.IsAudio
                    ? d.Value.IsVideo 
                        ? CodecMediaType.AudioVideo 
                        : CodecMediaType.Audio
                    : d.Value.IsVideo
                        ? CodecMediaType.Video
                        : CodecMediaType.None;

                var codecType = d.Value.IsDecodingSupported
                    ? d.Value.IsEncodingSupported
                        ? CodecType.DecoderEncoder
                        : CodecType.Decoder
                    : d.Value.IsEncodingSupported
                        ? CodecType.Encoder
                        : CodecType.None;

                var newEntry = CodecEntry.CreateFromData(d.Value, codecMediaType, codecType);
                _encoderEntries.Add(newEntry);
            }
        }

        public MediaFileFormatEntry ProvideFormatEntry(MediaFileFormat format)
        {
            if(_formatEntries == null)
            {
                return null;
            }

            for (int i = 0; i < _formatEntries.Count; i++)
            {
                if (_formatEntries[i].Format == format)
                {
                    return _formatEntries[i];
                }
            }

            return null;
        }

        public MediaFileFilterData ProvideBitstreamFilterData(Filter filter)
        {
            if (!_availableBitstreamFilters.ContainsKey(filter))
            {
                return null;
            }

            return _availableBitstreamFilters[filter];
        }

        public MediaFileFilterData ProvideFilterData(Filter filter)
        {
            if (!_availableFilters.ContainsKey(filter))
            {
                return null;
            }

            return _availableFilters[filter];
        }

        public List<MediaFileFilterEntry> ProvideFilterEntries(List<ConversionFilterOption> filterOptions)
        {
            var filterEntries = new List<MediaFileFilterEntry>();

            foreach(var option in filterOptions)
            {
                var entry = ProvideFilterEntry(option);
                if(entry == null)
                {
                    entry = ProvideBitstreamFilterEntry(option);
                }

                if (entry == null)
                {
                    continue;
                }

                filterEntries.Add(entry);
            }

            return filterEntries;
        }

        private MediaFileFilterEntry ProvideFilterEntry(ConversionFilterOption filterOption)
        {
            foreach (var entry in _filterEntries)
            {
                if (filterOption.Filter == entry.Filter)
                {
                    var newEntry = MediaFileFilterEntry.Copy(entry);
                    newEntry.Parameter = filterOption.Parameter;

                    return newEntry;
                }
            }

            return null;
        }

        private MediaFileFilterEntry ProvideBitstreamFilterEntry(ConversionFilterOption filterOption)
        {
            foreach(var entry in _bitstreamFilterEntries)
            {
                if (filterOption.Filter == entry.Filter)
                {
                    var newEntry = MediaFileFilterEntry.Copy(entry);
                    newEntry.Parameter = filterOption.Parameter;

                    return newEntry;
                }
            }

            return null;
        }

        public ObservableCollection<MediaFileFormatEntry> ProvideAvailableFormats() => _formatEntries;
        public MediaFileFormatEntry ProvideDefaultMediaFileFormatEntry() => _formatEntries[0];
        public ObservableCollection<MediaFileFilterEntry> ProvideAvailableFilters() => _filterEntries;
        public MediaFileFilterEntry ProvideDefaultMediaFileFilterEntry() => _filterEntries[0];
        public ObservableCollection<MediaFileFilterEntry> ProvideAvailableBitstreamFilters() => _bitstreamFilterEntries;
        public MediaFileFilterEntry ProvideDefaultMediaFileBitstreamFilterEntry() => _bitstreamFilterEntries[0];

        public ObservableCollection<CodecEntry> ProvideAvailableDecoders() => _decoderEntries;
        public ObservableCollection<CodecEntry> ProvideAvailableEncoders() => _encoderEntries;

        public List<CodecEntry> ProvideCodecEntries(List<CodecOption> codecs)
        {
            var codecEntries = new List<CodecEntry>();

            foreach (var codec in codecs)
            {
                var entry = ProvideCodecEntry(codec);
                if (entry == null)
                {
                    continue;
                }

                codecEntries.Add(entry);
            }

            return codecEntries;
        }

        private CodecEntry ProvideCodecEntry(CodecOption codecOption)
        {
            foreach (var entry in _decoderEntries)
            {
                if (codecOption.Codec == entry.Codec)
                {
                    var newEntry = CodecEntry.Copy(entry);
                    newEntry.Parameter = codecOption.Parameter;

                    return newEntry;
                }
            }
            foreach (var entry in _encoderEntries)
            {
                if (codecOption.Codec == entry.Codec)
                {
                    var newEntry = CodecEntry.Copy(entry);
                    newEntry.Parameter = codecOption.Parameter;

                    return newEntry;
                }
            }

            return null;
        }
    }

    /// <summary>
    /// Raw information about a format. Loaded and and translated from an external source
    /// </summary>
    public class MediaFileFormatData
    {
        public string FullName { get; set; }
        public string Description { get; set; }
        public string Extension { get; set; }
        public MediaType MediaType { get; set; }
    }

    public enum FilterType
    {
        BitstreamVideo,
        BitstreamAudio,
        BitstreamSubtitle,
        Video,
        Audio,
        SpecialAudio,
        SpecialVideo,
    }

    /*
ffmpeg -i input.mp4 -c copy -bsf:a aac_adtstoasc output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v chomp output.mp4
ffmpeg -i input.mp4 -c copy -bsf:a dca_core output.mp4
ffmpeg -i input.mp4 -c copy -bsf:s dvbsubs output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v extract_extradata output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v filter_units output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v h264_metadata output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v hevc_metadata output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v imx_dump_header output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v mjpeg2jpeg output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v mjpega_dump_header output.mp4
ffmpeg -i input.mp4 -c copy -bsf:a mp3_header_decompress output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v mpeg2_metadata output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v mpeg4_unpack_bframes output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v noise output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v remove_extra output.mp4
ffmpeg -i input.mp4 -c copy -bsf:s text2movsub output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v trace_headers output.mp4
ffmpeg -i input.mp4 -c copy -bsf:v vp9_raw_reorder output.mp4
     */

    public enum Filter
    {
        // Bitstream Filters
        aac_adtstoasc,
        chomp,
        dca_core,
        dvbsubs,
        extract_extradata,
        filter_units,
        h264_metadata,
        hevc_metadata,
        imx_dump_header,
        mjpeg2jpeg,
        mjpega_dump_header,
        mp3_header_decompress,
        mpeg2_metadata,
        mpeg4_unpack_bframes,
        noise,
        remove_extra,
        text2movsub,
        trace_headers,
        vp9_raw_reorder,

        // Video Filters
        scale,

        // Audio Filters
        atempo,
        loudnorm,
        volume,
        firequalizer,
        rubberband,
            
        // General Purpose Filters
        overlay, 
    }

    /// <summary>
    /// Raw information about a filter. Loaded and and translated from an external source
    /// </summary>
    public class MediaFileFilterData
    {
        public Filter Filter { get; set; }
        public FilterType Type { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public string Parameter { get; set; }

        public bool IsBitstream { get; set; }
        public bool IsAudio { get; set; }
        public bool IsVideo { get; set; }
    }


    /// <summary>
    /// Stores data displayed to the user. User may edit the Parameter property. That is why it is being copied.<br></br>
    /// If used, the object is to be stored as <b>CodecOption</b>.<br></br><br></br>
    /// 
    /// When used/edited/stored the entry is being recognized by its <b>Guid</b>.<br></br> 
    /// Only when the user sets up the codec the <b>Guid</b> is assigned.
    /// </summary>
    public class CodecEntry
    {
        public Guid Guid { get; set; }
        public Codec Codec { get; private set; }
        public CodecMediaType MediaType { get; private set; }
        public CodecType Type { get; private set; }
        public string Name { get; private set; }
        public string FullName => $"{Name} [{Parameter}] [{GetTypeAsString()}] [{GetTypeAsString()}]";
        public string Description { get; private set; }
        public string Parameter { get; set; }

        private CodecEntry(Codec codec, CodecMediaType mediaType, CodecType type, string name, string description, string parameter)
        {
            Codec = codec;
            MediaType = mediaType;
            Type = type;
            Name = name;
            Description = description;
            Parameter = parameter;
        }

        public static CodecEntry Copy(CodecEntry e)
        {
            Span<char> name = new(new char[e.Name.Length]);
            Span<char> description = new(new char[e.Description.Length]);
            Span<char> parameter = new(new char[e.Parameter.Length]);

            e.Name.CopyTo(name);
            e.Description.CopyTo(description);
            e.Parameter.CopyTo(parameter);

            return new(e.Codec, e.MediaType, e.Type, name.ToString(), description.ToString(), parameter.ToString());
        }

        public static CodecEntry CreateFromData(CodecData d, CodecMediaType mediaType, CodecType type)
        {
            Span<char> parameter = new(new char[d.Parameter.Length]);
            d.Parameter.CopyTo(parameter);

            return new(d.Codec, mediaType, type, d.Name, d.Description, parameter.ToString());
        }

        public override string ToString() => $"{Codec:g}:{GetMediaTypeAsString()}:{GetTypeAsString()}";

        private string GetMediaTypeAsString() =>
            MediaType switch
            {
                CodecMediaType.Audio => "A",
                CodecMediaType.Video => "V",
                CodecMediaType.AudioVideo => "AV",
                _ => "X",
            };

        private string GetTypeAsString() =>
            Type switch
            {
                CodecType.Encoder => "E",
                CodecType.Decoder => "D",
                CodecType.DecoderEncoder => "DE",
                _ => "X",
            };
    }

    /// <summary>
    /// Raw information about a codec. Loaded and and translated from an external source
    /// </summary>
    public class CodecData
    {
        public Codec Codec { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Parameter { get; set; }

        public bool IsDecodingSupported { get; set; }
        public bool IsEncodingSupported { get; set; }
        public bool IsAudio { get; set; }
        public bool IsVideo { get; set; }
    }

    public enum CodecType
    {
        Decoder,
        Encoder,
        DecoderEncoder,
        None,
    }

    public enum CodecMediaType
    {
        Audio,
        Video,
        AudioVideo,
        None,
    }

    public enum Codec
    {
        // Decoders & Encoders
        alias_pix,
        amv,
        apng,
        asv1,
        asv2,
        libaom_av1,
        av1_qsv,
        avrp,
        avui,
        bitpacked,
        bmp,
        cfhd,
        cinepak,
        cljr,
        dnxhd,
        dpx,
        dvvideo,
        dxv,
        exr,
        ffv1,
        ffvhuff,
        fits,
        flashsv,
        flashsv2,
        flv,
        gif,
        h261,
        h263,
        h263p,
        h264_qsv,
        hap,
        hdr,
        hevc_qsv,
        huffyuv,
        jpeg2000,
        jpegls,
        libjxl,
        magicyuv,
        mjpeg,
        mjpeg_qsv,
        mpeg1video,
        mpeg2video,
        mpeg2_qsv,
        mpeg4,
        msmpeg4v2,
        msmpeg4,
        msrle,
        msvideo1,
        pam,
        pbm,
        pcx,
        pfm,
        pgm,
        pgmyuv,
        phm,
        png,
        ppm,
        prores,
        qoi,
        qtrle,
        r10k,
        r210,
        rawvideo,
        roqvideo,
        rpza,
        rv10,
        rv20,
        sgi,
        smc,
        snow,
        speedhq,
        sunrast,
        svq1,
        targa,
        tiff,
        utvideo,
        v210,
        v308,
        v408,
        v410,
        vbn,
        vnull,
        libvpx,
        libvpx_vp9,
        vp9_qsv,
        wbmp,
        wmv1,
        wmv2,
        wrapped_avframe,
        xbm,
        xface,
        xwd,
        y41p,
        yuv4,
        zlib,
        zmbv,
        aac,
        ac3,
        ac3_fixed,
        adpcm_adx,
        adpcm_argo,
        g722,
        g726,
        g726le,
        adpcm_ima_alp,
        adpcm_ima_amv,
        adpcm_ima_apm,
        adpcm_ima_qt,
        adpcm_ima_ssi,
        adpcm_ima_wav,
        adpcm_ima_ws,
        adpcm_ms,
        adpcm_swf,
        adpcm_yamaha,
        alac,
        libopencore_amrnb,
        anull,
        aptx,
        aptx_hd,
        libcodec2,
        comfortnoise,
        dfpwm,
        dca,
        eac3,
        flac,
        g723_1,
        libgsm,
        libgsm_ms,
        libilbc,
        mlp,
        mp2,
        nellymoser,
        opus,
        libopus,
        pcm_alaw,
        pcm_bluray,
        pcm_dvd,
        pcm_f32be,
        pcm_f32le,
        pcm_f64be,
        pcm_f64le,
        pcm_mulaw,
        pcm_s16be,
        pcm_s16be_planar,
        pcm_s16le,
        pcm_s16le_planar,
        pcm_s24be,
        pcm_s24daud,
        pcm_s24le,
        pcm_s24le_planar,
        pcm_s32be,
        pcm_s32le,
        pcm_s32le_planar,
        pcm_s64be,
        pcm_s64le,
        pcm_s8,
        pcm_s8_planar,
        pcm_u16be,
        pcm_u16le,
        pcm_u24be,
        pcm_u24le,
        pcm_u32be,
        pcm_u32le,
        pcm_u8,
        pcm_vidc,
        real_144,
        roq_dpcm,
        s302m,
        sbc,
        sonic,
        libspeex,
        truehd,
        tta,
        vorbis,
        libvorbis,
        wavpack,
        wmav1,
        wmav2,
        ssa,
        ass,
        dvbsub,
        dvdsub,
        mov_text,
        srt,
        subrip,
        text,
        webvtt,
        xsub,

        // Decoders
        _012v,
        _4xm,
        _8bps,
        aasc,
        agm,
        aic,
        anm,
        ansi,
        arbc,
        argo,
        aura,
        aura2,
        libdav1d,
        av1,
        av1_cuvid,
        avrn,
        avs,
        libdavs2,
        libuavs3d,
        bethsoftvid,
        bfi,
        binkvideo,
        bintext,
        bmv_video,
        brender_pix,
        c93,
        cavs,
        cdgraphics,
        cdtoons,
        cdxl,
        clearvideo,
        cllc,
        eacmv,
        cpia,
        cri,
        camstudio,
        cyuv,
        dds,
        dfa,
        dirac,
        dsicinvideo,
        dxa,
        dxtory,
        escape124,
        escape130,
        fic,
        flic,
        fmvc,
        fraps,
        frwu,
        g2m,
        gdv,
        gem,
        h263i,
        h264,
        h264_cuvid,
        hevc,
        hevc_cuvid,
        hnm4video,
        hq_hqa,
        hqx,
        hymt,
        idcinvideo,
        idf,
        iff,
        imm4,
        imm5,
        indeo2,
        indeo3,
        indeo4,
        indeo5,
        interplayvideo,
        ipu,
        jv,
        kgv1,
        kmvc,
        lagarith,
        lead,
        loco,
        lscr,
        m101,
        eamad,
        mdec,
        media100,
        mimic,
        mjpeg_cuvid,
        mjpegb,
        mmvideo,
        mobiclip,
        motionpixels,
        mpeg1_cuvid,
        mpegvideo,
        mpeg2_cuvid,
        mpeg4_cuvid,
        msa1,
        mscc,
        msmpeg4v1,
        msp2,
        mss1,
        mss2,
        mszh,
        mts2,
        mv30,
        mvc1,
        mvc2,
        mvdv,
        mvha,
        mwsc,
        mxpeg,
        notchlc,
        nuv,
        paf_video,
        pdv,
        pgx,
        photocd,
        pictor,
        pixlet,
        prosumer,
        psd,
        ptx,
        qdraw,
        qpeg,
        rasc,
        rl2,
        rscc,
        rtv1,
        rv30,
        rv40,
        sanm,
        scpr,
        screenpresso,
        sga,
        sgirle,
        sheervideo,
        simbiosis_imx,
        smackvid,
        smvjpeg,
        sp5x,
        srgc,
        svq3,
        targa_y216,
        tdsc,
        eatgq,
        eatgv,
        theora,
        thp,
        tiertexseqvideo,
        tmv,
        eatqi,
        truemotion1,
        truemotion2,
        truemotion2rt,
        camtasia,
        tscc2,
        txd,
        ultimotion,
        v210x,
        vb,
        vble,
        vc1,
        vc1_qsv,
        vc1_cuvid,
        vc1image,
        vcr1,
        xl,
        vmdvideo,
        vmix,
        vmnc,
        vp3,
        vp4,
        vp5,
        vp6,
        vp6a,
        vp6f,
        vp7,
        vp8,
        vp8_cuvid,
        vp8_qsv,
        vp9,
        vp9_cuvid,
        vqc,
        vvc,
        wcmv,
        webp,
        wmv3,
        wmv3image,
        wnv1,
        vqavideo,
        xan_wc3,
        xan_wc4,
        xbin,
        xpm,
        ylc,
        yop,
        zerocodec,
        _8svx_exp,
        _8svx_fib,
        aac_fixed,
        aac_latm,
        acelp_kelvin,
        adpcm_4xm,
        adpcm_afc,
        adpcm_agm,
        adpcm_aica,
        adpcm_ct,
        adpcm_dtk,
        adpcm_ea,
        adpcm_ea_maxis_xa,
        adpcm_ea_r1,
        adpcm_ea_r2,
        adpcm_ea_r3,
        adpcm_ea_xas,
        adpcm_ima_acorn,
        adpcm_ima_apc,
        adpcm_ima_cunning,
        adpcm_ima_dat4,
        adpcm_ima_dk3,
        adpcm_ima_dk4,
        adpcm_ima_ea_eacs,
        adpcm_ima_ea_sead,
        adpcm_ima_iss,
        adpcm_ima_moflex,
        adpcm_ima_mtf,
        adpcm_ima_oki,
        adpcm_ima_rad,
        adpcm_ima_smjpeg,
        adpcm_mtaf,
        adpcm_psx,
        adpcm_sbpro_2,
        adpcm_sbpro_3,
        adpcm_sbpro_4,
        adpcm_thp,
        adpcm_thp_le,
        adpcm_vima,
        adpcm_xa,
        adpcm_xmd,
        adpcm_zork,
        amrnb,
        amrwb,
        libopencore_amrwb,
        apac,
        ape,
        atrac1,
        atrac3,
        atrac3al,
        atrac3plus,
        atrac3plusal,
        atrac9,
        on2avc,
        binkaudio_dct,
        binkaudio_rdft,
        bmv_audio,
        bonk,
        cbd2_dpcm,
        cook,
        derf_dpcm,
        dolby_e,
        dsd_lsbf,
        dsd_lsbf_planar,
        dsd_msbf,
        dsd_msbf_planar,
        dsicinaudio,
        dss_sp,
        dst,
        dvaudio,
        evrc,
        fastaudio,
        ftr,
        g729,
        gremlin_dpcm,
        gsm,
        gsm_ms,
        hca,
        hcom,
        iac,
        ilbc,
        imc,
        interplay_dpcm,
        interplayacm,
        mace3,
        mace6,
        metasound,
        misc4,
        mp1,
        mp1float,
        mp2float,
        mp3float,
        mp3,
        mp3adufloat,
        mp3adu,
        mp3on4float,
        mp3on4,
        als,
        msnsiren,
        mpc7,
        mpc8,
        osq,
        paf_audio,
        pcm_f16le,
        pcm_f24le,
        pcm_lxf,
        pcm_sga,
        qcelp,
        qdm2,
        qdmc,
        qoa,
        real_288,
        ralf,
        rka,
        sdx2_dpcm,
        shorten,
        sipr,
        siren,
        smackaud,
        sol_dpcm,
        speex,
        tak,
        truespeech,
        twinvq,
        vmdaudio,
        wady_dpcm,
        wavarc,
        wavesynth,
        ws_snd1,
        wmalossless,
        wmapro,
        wmavoice,
        xan_dpcm,
        xma1,
        xma2,
        libaribcaption,
        libaribb24,
        libzvbi_teletextdec,
        cc_dec,
        pgssub,
        jacosub,
        microdvd,
        mpl2,
        pjs,
        realtext,
        stl,
        subviewer,
        subviewer1,
        vplayer,
        
        // Encoders
        a64multi,
        a64multi5,
        librav1e,
        libsvtav1,
        av1_nvenc,
        av1_amf,
        libxavs2,
        vc2,
        libx264,
        libx264rgb,
        h264_amf,
        h264_mf,
        h264_nvenc,
        libx265,
        hevc_amf,
        hevc_mf,
        hevc_nvenc,
        libopenjpeg,
        ljpeg,
        libxvid,
        prores_aw,
        prores_ks,
        libtheora,
        libwebp_anim,
        libwebp,
        aac_mf,
        ac3_mf,
        libvo_amrwbenc,
        mp2fixed,
        libtwolame,
        libmp3lame,
        libshine,
        mp3_mf,
        sonicls,
        ttml,
    }
}
