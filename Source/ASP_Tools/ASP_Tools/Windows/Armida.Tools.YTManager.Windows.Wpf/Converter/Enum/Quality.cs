﻿namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Enum
{
    public enum Quality
    {
        Lowest,
        Low,
        Medium,
        High,
        Higher,
        Ultra,
    }
}
