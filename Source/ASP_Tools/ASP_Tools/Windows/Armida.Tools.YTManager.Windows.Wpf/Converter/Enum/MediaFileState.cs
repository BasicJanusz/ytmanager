﻿namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Enum
{
    public enum MediaFileState
    {
        Created,
        Loaded,
        Converted,
    }
}
