﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Utils
{
    public interface IConversionContextProvider
    {
        public List<ConversionProfile> ConversionProfiles { get; }
        public List<string> ExistingProfileNames { get; }

        public void UpdateConversionProfiles(ObservableCollection<ConversionProfile> conversionProfiles);
        public ConversionProfile GetConversionProfileByOption(ConversionProfileOption option);
    }

    public class ConversionContextProvider : IConversionContextProvider
    {
        public List<ConversionProfile> ConversionProfiles { get; private set; }
        public List<string> ExistingProfileNames { get; private set; }
        
        public ConversionContextProvider()
        {
            ConversionProfiles = new List<ConversionProfile>();
            ExistingProfileNames = new List<string>();
        }

        private void UpdateCurrentConversionProfileNames(string[] profileNames)
        {
            if (profileNames == null)
            {
                ExistingProfileNames = new List<string>();
                return;
            }

            ExistingProfileNames.Clear();
            ExistingProfileNames.AddRange(profileNames);
        }

        public void UpdateConversionProfiles(ObservableCollection<ConversionProfile> conversionProfiles)
        {
            if(conversionProfiles == null)
            {
                ConversionProfiles = new List<ConversionProfile>();
                return;
            }

            ConversionProfiles.Clear();
            ConversionProfiles.AddRange(conversionProfiles);

            var profileNames = new string[ConversionProfiles.Count];

            for (int i = 0; i < ConversionProfiles.Count; i++)
            {
                profileNames[i] = ConversionProfiles[i].Name;
            }

            UpdateCurrentConversionProfileNames(profileNames);
        }

        public ConversionProfile GetConversionProfileByOption(ConversionProfileOption option)
        {
            foreach (var profile in ConversionProfiles)
            {
                if (option.Name == profile.Name)
                {
                    return profile;
                }
            }

            return null;
        }
    }
}
