﻿using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Model;
using System;
using System.IO;
using System.Text;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Utils
{
    public interface IFFmpegQueryFactory
    {
        public string ConversionDirectory { get; }
        public YTVideo Video { get; }
        public ConversionProfile ConversionProfile { get; }
        public FFmpegQuery Build();
        public void SetVideo(YTVideo video);
        public void SetConversionProfile(ConversionProfile conversionProfile);
        public void SetMainApplicationPath(string mainApplicationPath);
        public void SetConversionDirectory(string conversionDirectory);
    }

    public class FFmpegQueryFactory : IFFmpegQueryFactory
    {
        private readonly IMediaFileDataProvider _mediaFileDataProvider;
        private string _ffmpegPath;

        public string ConversionDirectory { get; private set; }
        public YTVideo Video { get; private set; }
        public ConversionProfile ConversionProfile { get; private set; }

        public FFmpegQueryFactory(IMediaFileDataProvider mediaFileDataProvider)
        {
            _mediaFileDataProvider = mediaFileDataProvider;
            _ffmpegPath = string.Empty;
        }

        public FFmpegQuery Build()
        {
            Validate();

            var fileName = Path.GetFileNameWithoutExtension(Video.DownloadedFilePath);
            var targetFormat = _mediaFileDataProvider.ProvideFormatEntry(ConversionProfile.Target.Format);
            var convertedFilePath = $"{Path.Combine(ConversionDirectory, fileName)}.{targetFormat.Extension}";
            var inputFile = $"-i \"{Video.DownloadedFilePath}\" ";
            var inputArguments = GetInputArguments();

            FFmpegQuery query = new FFmpegQuery();
            query.Query = $"{inputFile}{inputArguments}\"{convertedFilePath}\"";
            query.FFmpegPath = _ffmpegPath;
            query.ConvertedFilePath = convertedFilePath;

            return query;
        }

        public void SetVideo(YTVideo video)
        {
            Video = video;
        }

        public void SetConversionProfile(ConversionProfile conversionProfile)
        {
            ConversionProfile = conversionProfile;
        }
        public void SetConversionDirectory(string conversionDirectory)
        {
            ConversionDirectory = conversionDirectory;
        }

        public void SetMainApplicationPath(string mainApplicationPath)
        {
            _ffmpegPath = Path.Combine(mainApplicationPath, "Ffmpeg\\ffmpeg.exe");
        }

        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(ConversionDirectory))
            {
                throw new ArgumentException("The Conversion Directory has to be set properly in order to create the FFMPEG Query");
            }

            if (Video == null)
            {
                throw new ArgumentException("The converted Video has to be set properly in order to create the FFMPEG Query");
            }

            if (ConversionProfile == null)
            {
                throw new ArgumentException("The Conversion Profile has to be set properly in order to create the FFMPEG Query");
            }

            if (string.IsNullOrWhiteSpace(_ffmpegPath))
            {
                throw new ArgumentException("The Main Application Path has to be set properly in order to create the FFMPEG Query");
            }
        }

        private string GetInputArguments()
        {
            var builder = new StringBuilder();
            var alreadyHasAudioFilter = false;
            var alreadyHasVideoFilter = false;

            // Disable Tracks
            if(ConversionProfile.DisableVideo)
            {
                builder.Append("-vn ");
            }
            if (ConversionProfile.DisableAudio)
            {
                builder.Append("-an ");
            }

            //Decoders
            if (!ConversionProfile.DisableVideo)
            {
                foreach (var codec in ConversionProfile.Codecs)
                {
                    if (codec.MediaType == CodecMediaType.Video && codec.Type == CodecType.Decoder)
                    {
                        builder.Append($"-c:v {codec.Codec} {codec.Parameter} ");
                    }
                }
            }
            if(!ConversionProfile.DisableAudio)
            {
                foreach (var codec in ConversionProfile.Codecs)
                {
                    if (codec.MediaType == CodecMediaType.Audio && codec.Type == CodecType.Decoder)
                    {
                        builder.Append($"-c:a {codec.Codec} {codec.Parameter} ");
                    }
                }
            }

            // Bitstream Filters
            if (!ConversionProfile.DisableVideo)
            {
                foreach (var filter in ConversionProfile.Filters)
                {
                    if (filter.Type == FilterType.BitstreamVideo)
                    {
                        builder.Append($"-bsf:v {filter.Filter}{filter.Parameter} ");
                    }
                }
            }
            if (!ConversionProfile.DisableAudio)
            {
                foreach (var filter in ConversionProfile.Filters)
                {
                    if (filter.Type == FilterType.BitstreamAudio)
                    {
                        builder.Append($"-bsf:a {filter.Filter}{filter.Parameter} ");
                    }
                }
            }

            // Filters
            if (!ConversionProfile.DisableVideo)
            {
                var videoFilters = new StringBuilder();
                foreach (var filter in ConversionProfile.Filters)
                {
                    if(filter.Type == FilterType.Video)
                    {
                        if (alreadyHasVideoFilter)
                        {
                            videoFilters.Append($" {filter.Filter}{filter.Parameter},");
                            continue;
                        }

                        builder.Append($"-vf \"{filter.Filter}{filter.Parameter},");
                        alreadyHasVideoFilter = true;
                    }
                }

                if(alreadyHasVideoFilter)
                {
                    videoFilters.Remove(videoFilters.Length - 1, 1);
                    videoFilters.Append("\" ");
                    builder.Append(videoFilters.ToString());
                }
            }
            if (!ConversionProfile.DisableAudio)
            {
                var audioFilters = new StringBuilder();
                foreach (var filter in ConversionProfile.Filters)
                {
                    if (filter.Type == FilterType.Audio)
                    {
                        if (alreadyHasAudioFilter)
                        {
                            audioFilters.Append($" {filter.Filter}{filter.Parameter},");
                            continue;
                        }

                        builder.Append($"-af \"{filter.Filter}{filter.Parameter},");
                        alreadyHasAudioFilter = true;
                    }
                }

                if(alreadyHasAudioFilter)
                {
                    audioFilters.Remove(audioFilters.Length - 1, 1);
                    audioFilters.Append("\" ");
                    builder.Append(audioFilters.ToString());
                }
            }

            //Encoder
            if (!ConversionProfile.DisableVideo)
            {
                foreach (var codec in ConversionProfile.Codecs)
                {
                    if (codec.MediaType == CodecMediaType.Video && codec.Type == CodecType.Encoder)
                    {
                        builder.Append($"-vcodec {codec.Codec} {codec.Parameter} ");
                    }
                }
            }
            if (!ConversionProfile.DisableAudio)
            {
                foreach (var codec in ConversionProfile.Codecs)
                {
                    if (codec.MediaType == CodecMediaType.Audio && codec.Type == CodecType.Encoder)
                    {
                        builder.Append($"-acodec {codec.Codec} {codec.Parameter} ");
                    }
                }
            }

            return builder.ToString();
        }
    }

    public class FFmpegQuery
    {
        public string ConvertedFilePath { get; set; }
        public string FFmpegPath { get; set; }
        public string Query { get; set; }

        public FFmpegQuery() 
        {
            ConvertedFilePath = string.Empty;
            FFmpegPath = string.Empty;
            Query = string.Empty;
        }
    }
}
