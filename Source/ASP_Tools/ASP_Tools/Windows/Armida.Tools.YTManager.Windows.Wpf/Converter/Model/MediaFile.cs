﻿using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using System;
using System.ComponentModel;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Model
{
    public class MediaFile : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        public event Action StateChanged;

        private MediaFileState _state;
        private string _format;
        private string _name;
        private string _path;

        public MediaFileState State
        {
            get => _state;
            set
            {
                _state = value;
                StateChanged?.Invoke();
            }
        }

        public string Path
        {
            get => _path;
            set
            {
                _path = value;
                RaisePropertyChanged(nameof(Path));
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }

        public string Format
        {
            get => _format;
            set
            {
                _format = value;
                RaisePropertyChanged(nameof(Format));
            }
        }

        public MediaFile(string path) 
        {
            Path = path;
            Name = System.IO.Path.GetFileNameWithoutExtension(path);
            Format = System.IO.Path.GetExtension(path);
            State = MediaFileState.Created;
        }

        private void RaisePropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
