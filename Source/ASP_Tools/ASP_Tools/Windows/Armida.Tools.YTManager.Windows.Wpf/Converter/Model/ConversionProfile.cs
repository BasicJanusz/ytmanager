﻿using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using System;
using System.Collections.Generic;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Model
{
    public class ConversionProfile
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public MediaFileFormatEntry Source { get; set; }
        public MediaFileFormatEntry Target { get; set; } 
        public List<MediaFileFilterEntry> Filters { get; set; }
        public List<CodecEntry> Codecs { get; set; }
        public bool DisableAudio { get; set; }
        public bool DisableVideo { get; set; }

        public ConversionProfile() 
        {
            Guid = Guid.Empty;
            Name = string.Empty;
            Source = null;
            Target = null;
            Filters = new List<MediaFileFilterEntry>();
            Codecs = new List<CodecEntry>();
        }

        public override string ToString()
        {
            var sourceAsString = "NULL";
            if(Source != null)
            {
                sourceAsString = Source.Extension;
            }
            
            var targetAsString = "NULL";
            if (Target != null)
            {
                targetAsString = Target.Extension;
            }

            return $"{Name} ({sourceAsString} => {targetAsString}; [{string.Join("; ", Filters)}]; [{string.Join("; ", Codecs)}])";
        }
    }
}
