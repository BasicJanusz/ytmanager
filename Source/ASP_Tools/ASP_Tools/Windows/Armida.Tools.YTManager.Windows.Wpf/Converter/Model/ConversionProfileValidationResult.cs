﻿using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Model
{
    public class ConversionProfileValidationResult
    {
        public ValidationResultEnum Result { get; set; }
        public string ErrorMessage { get; set; }
    }
}
