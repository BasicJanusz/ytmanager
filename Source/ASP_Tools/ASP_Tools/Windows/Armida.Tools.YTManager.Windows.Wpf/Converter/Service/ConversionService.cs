﻿using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Settings;
using Armida.Tools.YTManager.Windows.Wpf.ApplicationComponents.Workspace;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Youtube.Enum;
using MediaToolkit;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Service
{
    public class ConversionService : IConversionService
    {
        private object _processLock = new object();
        private object _queueLock = new object();
        private bool _mainLoop;
        private Queue<(ProcessType process, MediaFile file)> _processQueue;
        private Thread _thread;
        private IApplicationSettings _applicationSettings;
        private readonly IWorkspaceManager _workspaceManager;

        public event Action<string> FileConverted;
        public ObservableCollection<string> ConversionOptions { get; }
        public string SelectedConversionOption 
        {
            get => _applicationSettings.SelectedConversionFormat;
            set
            {
                _applicationSettings.SelectedConversionFormat = value;
            }
        }

        public bool IsAnyConversionOptionSelected => !string.IsNullOrWhiteSpace(SelectedConversionOption);

        public bool DeleteConvertedFileWhenRemovingVideo => 
            _applicationSettings.SelectedRemoveOption == RemoveOptionType.ConvertedFile ||
            _applicationSettings.SelectedRemoveOption == RemoveOptionType.All;

        public ConversionService(IWorkspaceManager workspaceManager, IApplicationSettings applicationSettings)
        {
            _applicationSettings = applicationSettings;
            _processQueue = new Queue<(ProcessType process, MediaFile file)>();
            _thread = new Thread(new ThreadStart(ProcessLoop));

            // TODO : Conversion profiles
            ConversionOptions = new ObservableCollection<string>()
            {
                "mp3",
                "mp4",
            };

            _workspaceManager = workspaceManager;
        }

        public void StartProcessThread()
        {
            if (_thread == null)
                return;

            _mainLoop = true;
            _thread?.Start();
        }

        public void EnqueueProcess(ProcessType process, MediaFile file)
        {
            lock (_queueLock)
            {
                _processQueue.Enqueue((process, file));
            }
        }

        private async void ProcessLoop()
        {
            MediaFile file = null;
            ProcessType process = ProcessType.Empty;
            bool dequeued = false;

            while (_mainLoop)
            {
                lock (_queueLock)
                {
                    if (_processQueue.Count > 0)
                    {
                        var currentProcess = _processQueue.Dequeue();
                        process = currentProcess.process;
                        file = currentProcess.file;
                        dequeued = true;
                    }
                }

                if (dequeued)
                {
                    lock (_processLock)
                    {
                        switch (process)
                        {
                            case ProcessType.Convert:
                                ConvertFileAsync(file);
                                break;

                            case ProcessType.Remove:
                                RemoveFileAsync(file);
                                break;

                            default:
                                break;
                        }
                    }

                    dequeued = false;
                    continue;
                }

                await Task.Delay(500);
            }
        }

        private void ConvertFileAsync(MediaFile file)
        {
            // TODO : Investigate audio quality
            var conversionFormat = SelectedConversionOption;
            var convertedFilePath = $"{Path.Combine(_workspaceManager.ConvertPath, file.Name)}.{conversionFormat}";

            var inputfile = new MediaToolkit.Model.MediaFile { Filename = file.Path };
            var outputfile = new MediaToolkit.Model.MediaFile { Filename = convertedFilePath };

            using (var enging = new Engine())
            {
                enging.GetMetadata(inputfile);
                enging.Convert(inputfile, outputfile);
            }

            FileConverted?.Invoke(convertedFilePath);
            file.State = MediaFileState.Converted;
        }

        private void RemoveFileAsync(MediaFile file)
        {
            if (DeleteConvertedFileWhenRemovingVideo && File.Exists(file.Path))
            {
                File.Delete(file.Path);
            }
        }
    }

    public interface IConversionService
    {
        public event Action<string> FileConverted;

        ObservableCollection<string> ConversionOptions { get; }
        public string SelectedConversionOption { get; set; }
        bool IsAnyConversionOptionSelected { get; }

        void EnqueueProcess(ProcessType load, MediaFile file);
        void StartProcessThread();
    }
}
