﻿using Armida.Tools.YTManager.Windows.Wpf.Converter.Enum;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Model;
using Armida.Tools.YTManager.Windows.Wpf.Converter.Utils;
using System.Collections.Generic;
using System.Text;

namespace Armida.Tools.YTManager.Windows.Wpf.Converter.Service
{
    public interface IConversionValidationService
    {
        ConversionProfileValidationResult ValidateConversionProfile(ConversionProfile data, bool ignoreNameValidation = false);
    }

    public class ConversionValidationService : IConversionValidationService
    {
        private readonly IConversionContextProvider _conversionContextProvider;

        private List<string> ExistingProfileNames => _conversionContextProvider.ExistingProfileNames;
        private Dictionary<MediaFileFormat, Codec[]> _requiredCodecsByFormat = new Dictionary<MediaFileFormat, Codec[]>();

        public ConversionValidationService(IConversionContextProvider conversionContextProvider)
        {
            _conversionContextProvider = conversionContextProvider;

            _requiredCodecsByFormat = new()
            {
                { MediaFileFormat.MP3, new Codec[] { Codec.libmp3lame, Codec.mp3_mf } }
            };
        }

        public ConversionProfileValidationResult ValidateConversionProfile(ConversionProfile data, bool ignoreNameValidation = false)
        {
            var result = ValidationResultEnum.Success;
            var errorMessage = new StringBuilder();
            
            if(!ignoreNameValidation)
            {
                ValidateProfileName(data.Name, ref result, ref errorMessage);
            }

            ValidateFormat(data.Source, ref result, ref errorMessage, "source");
            ValidateFormat(data.Target, ref result, ref errorMessage, "target");
            ValidateCodecs(data.Source, data.Codecs, ref result, ref errorMessage, "Source");
            ValidateCodecs(data.Target, data.Codecs, ref result, ref errorMessage, "Target");

            return new() { Result = result, ErrorMessage = errorMessage.ToString() };
        }

        private void ValidateProfileName(string name, ref ValidationResultEnum result, ref StringBuilder errorMessage)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                result = ValidationResultEnum.Failure;
                errorMessage.AppendLine(" * Name cannot be empty");
            }

            if (ExistingProfileNames.Contains(name))
            {
                result = ValidationResultEnum.Failure;
                errorMessage.AppendLine($" * The profile with a Name of '{name}' already exists");
            }
        }

        private void ValidateFormat(MediaFileFormatEntry entry, ref ValidationResultEnum result, ref StringBuilder errorMessage, string formatIntention)
        {
            if(entry == null)
            {
                result = ValidationResultEnum.Failure;
                errorMessage.AppendLine($" * Profile has to have a {formatIntention} format set");

                return;
            }

            if (entry.Format == MediaFileFormat.Any || entry.Format == MediaFileFormat.None)
            {
                result = ValidationResultEnum.Failure;
                errorMessage.AppendLine($" * The profile cannot have '{entry.FullName}' {formatIntention} format");
            }
        }

        private void ValidateCodecs(MediaFileFormatEntry format, List<CodecEntry> codecs, ref ValidationResultEnum result, ref StringBuilder errorMessage, string formatIntention)
        {
            if (_requiredCodecsByFormat.TryGetValue(format.Format, out var requiredCodecs))
            {
                foreach(var codec in codecs)
                {
                    var isAnyCodecProvided = false;

                    foreach (var requiredCodec in requiredCodecs)
                    {
                        if (requiredCodec == codec.Codec)
                        {
                            isAnyCodecProvided = true;
                        }
                    }

                    if (isAnyCodecProvided)
                    {
                        return;
                    }
                }

                result = ValidationResultEnum.Failure;
                errorMessage.AppendLine($" * For chosen {formatIntention} ({format.FullName}), at least one of the following codecs hava to be set '{string.Join(',', requiredCodecs)}'");
            }
        }
    }
}
