﻿using MediaToolkit;
using MediaToolkit.Model;
using VideoLibrary;

namespace Armida.Tools.YTManager.Common
{
    public static class Example
    {
        public static void DowloadMp3(string url, string targetDir)
        {
            var youtube = YouTube.Default;
            var video = youtube.GetVideo(url);

            var fileNameBase = Path.Combine(targetDir, video.FullName.Replace(' ', '_'));

            File.WriteAllBytes(fileNameBase, video.GetBytes());

            var inputFile = new MediaFile { Filename = fileNameBase };
            var mp3File = new MediaFile { Filename = $"{fileNameBase}.mp3" };

            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);
                engine.Convert(inputFile, mp3File);
            }
        }
    }
}
