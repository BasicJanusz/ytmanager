﻿using Armida.Tools.YTManager.Common.Model;
using VideoLibrary;

namespace Armida.Tools.YTManager.Common
{
    public interface IYTWrapper
    {
        public void Download(YTVideo video, string targetFilePath);
        public YTVideo GetVideo(string url);
        public List<YTVideo> GetVideosFromPLaylist(YTVideo playlist);

        public Task<int> DownloadAsync(YTVideo video, string targetFilePath);
        public Task<YTVideo> GetVideoAsync(string url);
    }

    public class YTWrapper : IYTWrapper
    {
        public YTVideo GetVideo(string url)
        {
            var video = new YTVideo(url);

            if (video.IsPlaylist)
                return video;

            video.LoadInitialData(YouTube.Default);
            return video;
        }

        public async Task<YTVideo> GetVideoAsync(string url)
        {
            var video = new YTVideo(url);
            await Task.Run(() => video.LoadInitialData(YouTube.Default));

            return video;
        }

        public void Download(YTVideo video, string targetFilePath)
        {
            if (!video.IsDataLoaded)
            {
                return;
            }

            var youTube = YouTube.Default;

            var youTubeVideo = youTube.GetVideo(video.Url);
            File.WriteAllBytes(targetFilePath, youTubeVideo.GetBytes());
        }

        public async Task<int> DownloadAsync(YTVideo video, string targetFilePath)
        {
            var youTube = YouTube.Default;

            await Task.Run(() =>
            {
                var youTubeVideo = youTube.GetVideo(video.Url);
                File.WriteAllBytes(targetFilePath, youTubeVideo.GetBytes());
            });

            return 1;
        }

        public List<YTVideo> GetVideosFromPLaylist(YTVideo playlist)
        {
            var youTube = YouTube.Default;
            var videos = new List<YTVideo>();
            var pathPlaylist = playlist.Url
                .Replace("https", "")
                .Replace("http", "")
                .Replace("://", "")
                .Replace("www.", "")
                .Replace("youtube.com/", "").Trim();

            var content = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://www.youtube.com");
                var result = client.GetAsync(pathPlaylist);
                content = result.Result.Content.ReadAsStringAsync().Result;
            }

            var splitContent = content.Split("/watch?v=");

            for (int i = 1; i < splitContent.Length; i++)
            {
                var subSplit = splitContent[i].Split("&list=");

                var link = Path.Combine("https://www.youtube.com", $"/watch?v={subSplit[0]}");

                var video = new YTVideo(link);
                video.LoadInitialData(youTube);

                if (video.IsDataLoaded)
                    videos.Add(video);
            }

            //var result = client.GetAsync(pathPlaylist).Result;
            //var conteudo = result.Content.ReadAsStringAsync().Result;
            //var links = ExtrairLinksPlaylist(conteudo);

            //foreach (var link in links)
            //{
            //    var video = new YTVideo(link);
            //    video.LoadInitialData(youTube);

            //    videos.Add(video);
            //}

            return videos;
        }

        private static List<string> ExtrairLinksPlaylist(string html)
        {
            var linksPlaylist = new List<string>();

            //var nodes = doc.DocumentNode.SelectNodes("a[@href]");
            //var w = "/watch?v=";
            //var padraoLink = $"<a class=\"pl-video-title-link yt-uix-tile-link yt-uix-sessionlink  spf-link \" dir=\"ltr\" href=\"{w}";
            //var idx = html.IndexOf(padraoLink, 0);

            //while (idx > -1)
            //{
            //    var idxInicioLink = idx + padraoLink.Length - w.Length;
            //    var idxFimLink = html.IndexOf("&", idxInicioLink);
            //    var pedacoLink = html.Substring(idxInicioLink, idxFimLink - idxInicioLink);
            //    linksPlaylist.Add(pedacoLink);

            //    idx = html.IndexOf(padraoLink, idxFimLink);
            //}

            return linksPlaylist;
        }

        private List<YTVideo> ParseVideos(IEnumerable<YouTubeVideo> videos)
        {
            var result = new List<YTVideo>();

            foreach(var video in videos)
            {
                var r = new YTVideo(video);
                result.Add(r);
            }

            return result;
        }
    }
}
