﻿using VideoLibrary;
using VideoLibrary.Exceptions;

namespace Armida.Tools.YTManager.Common.Model
{
    public class YTVideo
    {
        private readonly string _url;

        public string Url { get { return _url; } }
        public string Name { get; private set; }
        public string FullName { get; private set; }
        public string DataLoadedState { get; private set; }
        public string ErrorMessage { get; private set; }
        public bool IsDataLoaded { get; private set; }
        public bool IsNotDonwloading { get; set; }

        public bool IsPlaylist => Url.Contains("playlist?list");

        public YTVideo(string url)
        {
            _url = url;
            Name = string.Empty;
            FullName = string.Empty;
            DataLoadedState = string.Empty;
            ErrorMessage = string.Empty;
            IsNotDonwloading = true;
        }

        public YTVideo(YouTubeVideo video)
        {
            _url = video.Uri;
            Name = video.Title;
            FullName = video.FullName;
            DataLoadedState = "Video loaded properly";
            ErrorMessage = string.Empty;
            IsNotDonwloading = true;
            IsDataLoaded = true;
        }

        public void LoadInitialData(YouTube youTube = null)
        {
            if(youTube == null) 
            { 
                youTube = YouTube.Default; 
            }

            YouTubeVideo video = null;
            
            try
            {
                video = youTube.GetVideo(_url);
            }
            catch(UnavailableStreamException e)
            {
                ErrorMessage = e.Message;
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }

            if (video == null) 
            {
                DataLoadedState = "Video could not be loaded";
                IsDataLoaded = false;
                return;
            }

            Name = video.Title;
            FullName = video.FullName;
            DataLoadedState = "Video loaded properly";
            IsDataLoaded = true;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
