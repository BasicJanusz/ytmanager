# YTManager
A desktop WPF app for downloading and converting Youtube Media. It supports singular videos, custom playlists and Youtube Radio (dynamic Youtube playlists). 

## Installation
Build and run the standalone application.
The components for running .Net 6 applications are needed (they shall be requested by the application itsefl via dialog box on application start).

> [!WARNING]
> The application will not function without a reference to built ffmpeg.exe : <https://ffmpeg.org>.

## Usage
### On Apllication Start
Application creates three folders Download, Convert and Authentication. The downloaded videos go to the Download folder, the converted ones to the Convert folder, the authentication info is saved to Authentication folder. Videos and settings are loaded from the previous session.

### When Closing
The application saves the current session.

### Downloading
The desired videos can be added by dropping a Youtube Url. The video is added to the list from where it can be downloaded. After choosing the desired format the download process can be started. the downloaded file is available in the Download folder.

> [!WARNING]
> The video entry is not available on the list untile the download process is finished.

### Conversion
After downloading the video can be converted to a mp3 file.

> [!NOTE]
> All of the main three directories can be changed in the Options panel.

## Getting Help
Contact me at: <arkadiusz.zdziech@o2.pl>

## Project status
As of now :
* the videos can be downloaded as mp4, 3gp or a webm file
* The only conversion option is mp3
* The mp3 file quality can be set
* Youtube Authentication is to be implemented to specify the videos in the dynamic Youtube Radio playlists
* The user manual is to be expanded